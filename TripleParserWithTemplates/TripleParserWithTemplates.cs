﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using TripleParserConnector.Models;
using TripleParserConnector.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripleParserConnector.TripleParserWithTemplates
{
    public class TripleParserWithTemplates : ParseBase
    {


        public TripleParserWithTemplates(Globals globals) : base(globals)
        {
            
        }

        public async Task<ResultItem<ParseTripleOfProject>> GetTemplates(clsOntologyItem projectItem)
        {
            serviceAgent = new ServiceAgentElastic(globals);

            var templatesResult = await serviceAgent.GetTemplates(projectItem);

            return templatesResult;
        }

        public async Task<ResultItem<List<TemplateTerm>>> GetTemplateTerms(ParseTripleOfProject parsedTripleOfProject)
        {


            var taskResult = await Task.Run<ResultItem<List<TemplateTerm>>>(async () =>
            {
                var result = new ResultItem<List<TemplateTerm>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<TemplateTerm>()
                };


                foreach (var item in parsedTripleOfProject.ParseTemplates.SelectMany(templ => templ.TemplatesText))
                {
                    var templateWord = new TemplateTerm
                    {
                        TriplePart = new clsOntologyItem
                        {
                            GUID = item.ID_Object,
                            Name = item.Name_Object,
                            GUID_Parent = item.ID_Class,
                            Type = globals.Type_Object
                        }
                    };

                    var wordsResult = await FindWords(item.Val_String);
                    if (wordsResult.Result.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState = wordsResult.Result;
                        break;
                    }

                    templateWord.WordsBefore = wordsResult.FoundWords;

                    result.Result.Add(templateWord);
                }

                

                return result;
            });

            return taskResult;

        }

        public async Task<ResultFoundClasses> CreateTemplateClasses(List<TemplateTerm> resultGetTemplateTerms)
        {
            var taskResult = await Task.Run<ResultFoundClasses>(async () =>
            {
                serviceAgent = new ServiceAgentElastic(globals);
                var result = new ResultFoundClasses
                {
                    Result = globals.LState_Success.Clone(),
                    FoundClasses = new FoundClasses()
                };

                var foundWords = resultGetTemplateTerms.SelectMany(templ => templ.WordsBefore).Where(foundWord => foundWord.UpperCase).ToList();
                var foundClassesPre = foundWords.GroupBy(word => new { Word = word.Word }).Select(word => new clsOntologyItem
                {
                    Name = word.Key.Word,
                    Type = globals.Type_Class
                });

                result.FoundClasses.ClassesFound = (from foundWord in foundWords
                                                    join foundClassPre in foundClassesPre on foundWord.Word equals foundClassPre.Name
                                                    select new FoundClass
                                                    {
                                                        ClassItem = foundClassPre,
                                                        FoundWord = foundWord
                                                    }).ToList();

                var resultAgent = await serviceAgent.CheckClasses(result.FoundClasses.ClassesFound.Select(cls => cls.ClassItem).ToList());

                foreach (var classItm in result.FoundClasses.ClassesFound)
                {
                    if (string.IsNullOrEmpty(classItm.ClassItem.GUID_Parent))
                    {
                        classItm.ClassItem.GUID_Parent = serviceAgent.ClassProject.GUID;
                    }
                }

                var classes = result.FoundClasses.ClassesFound.Where(fc => fc.ClassItem.New_Item != null && fc.ClassItem.New_Item.Value == true).Select(fc => fc.ClassItem).ToList();
                if (classes.Any())
                {
                    var taskResultClasses = await serviceAgent.CreateClasses(classes, null);

                    if (taskResultClasses.GUID == globals.LState_Error.GUID)
                    {
                        result.Result = taskResultClasses;
                        return result;
                    }
                }


                return result;
            });

            return taskResult;



        }

        /// <summary>
        /// Parse Text by Templates
        /// </summary>
        /// <param name="tripleTerms">The Templates with terms and before- and after-words</param>
        /// <param name="projectItem">The project to relate the found Triple Parts to</param>
        /// <param name="textOrId">Text or GUID</param>
        /// <param name="orderId">The orderId of the Text (relation to project)</param>
        /// <param name="triplePartType">Type of Triple-Part</param>
        /// <returns></returns>
        public async Task<ResultItem<List<Models.ParsedTriplePart>>> ParseText(List<Models.TriplePart> tripleTerms,
            clsOntologyItem projectItem,
            string textOrId,
            long orderId,
            clsOntologyItem triplePartType)
        {
            var parseResult = new ResultItem<List<Models.ParsedTriplePart>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<ParsedTriplePart>()
            };

            serviceAgent = new ServiceAgentElastic(globals);

            // Create Triple Store items
            var triplePartResult = await serviceAgent.CheckTriplePart(projectItem, textOrId, triplePartType, orderId);

            if (triplePartResult.ResultState.GUID == globals.LState_Error.GUID)
            {
                parseResult.ResultState = triplePartResult.ResultState;
                return parseResult;
            }
            long orderIdTriplePart = 1;

            // Create item for recognition
            parseResult.Result = tripleTerms.Select(triplePart => new ParsedTriplePart(projectItem, triplePart, globals, triplePartResult.Result.TriplePart, triplePartResult.Result.TriplePartText, triplePartType, orderIdTriplePart++)).ToList();

            // Recognize the Terms
            parseResult.Result.ForEach(triplePart =>
            {
                triplePart.CompareWords();
            });

            // Order the parsed Templates by descending efficiency (ratio between Terms and found Terms)
            var bestResult = parseResult.Result.OrderByDescending(triplePart => triplePart.Efficiency).ThenByDescending(triplePart => triplePart.FoundTerms.Count).ThenByDescending(triplePart => triplePart.ParseSuccessPercent) .FirstOrDefault();

            if (bestResult != null)
            {
                bestResult.BestResult = true;
            }

            return parseResult;
        }
    }

    public class TemplateTerm
    {
        public string GuidTerm { get; set; }
        public clsOntologyItem TriplePart { get; set; }
        public clsObjectAtt TriplePartText { get; set; }
        public List<FoundWord> WordsBefore { get; set; }
        public List<FoundWord> WordsAfter { get; set; }
        public clsOntologyItem Class { get; set; }
        public clsOntologyItem OItemTerm { get; set; }
        public TemplateTerm Synonym { get; set; }

        public TemplateTerm()
        {
            GuidTerm = Guid.NewGuid().ToString();
        }
    }

    public class TextPart
    {
        public FoundWord Term { get; set; }
        public List<FoundWord> RecognitionMask { get; set; }
        
    }

    //public class ResultGetTemplateTerms
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public List<TemplateTerm> TemplateTerms { get; set; }

    //}

    public class TemplateItem
    {
        public clsOntologyItem Template { get; set; }
        public List<TemplateTerm> TemplateTerms { get; set; }
    }

    //public class ResultTemplateParse
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public List<Models.ParsedTriplePart> ParsedTripleParts { get; set; }
    //}

    public class TemplateParse
    {
        public TemplateItem TemplateTerm { get; set; }
        public List<Models.ParsedTriplePart> ParsedTripleParts { get; set; }
    }
}
