﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructureCreateModule.Attributes
{
    
    public class BaseOStructureAttribute : Attribute
    {
        public int Order { get; set; }
    }
}
