﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripleParserConnector.Models;

namespace TripleParserConnector.Factory
{
    public class ProjectFactory
    {
        private Globals globals;

        public async Task<ResultItem<List<clsOntologyItem>>> CreateProjectList(List<clsOntologyItem> projectList)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {

                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                result.Result = projectList;
                return result;
            });
            

            return taskResult;
        }

        public ProjectFactory(Globals globals)
        {
            this.globals = globals;
        }
    }
}
