﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripleParserConnector.Models;
using TripleParserConnector.Services;

namespace TripleParserConnector.Factory
{
    public class TicketTemplateFactory
    {
        public async Task<ResultItem<TicketTemplates>> CreateTicketTemplateList(TicketTemplatesWithClassesAndRegex templateList, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<TicketTemplates>>(() =>
            {

                var templateClasses = (from template in templateList.TicketTemplates
                                       join classItm in templateList.Classes on template.ID_Object equals classItm.ID_Object
                                       select new { template, classItm });

                var templateRegexes = (from template in templateList.TicketTemplates
                                       join regex in templateList.TicketTemplatesToRegex on template.ID_Object equals regex.ID_Object
                                       join regexAtt in templateList.RegexAttributes on regex.ID_Other equals regexAtt.ID_Object
                                       join classItm in templateList.Classes on regex.ID_Other equals classItm.ID_Object
                                       select new { template, regex, regexAtt, classItm });
                var result = new ResultItem<TicketTemplates>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new TicketTemplates
                    {
                        TicketTemplatesClasses = templateClasses.Select(tmplCls => new TicketTemplateClass(tmplCls.template, tmplCls.classItm)).ToList(),
                        TicketTemplatesRegex = templateRegexes.Select(tmplRegex => new TicketTemplateRegex(tmplRegex.template, tmplRegex.regex, tmplRegex.regexAtt, tmplRegex.classItm)).ToList()
                    }
                };

               

                
                return result;
            });

            return taskResult;
        }
    }
}
