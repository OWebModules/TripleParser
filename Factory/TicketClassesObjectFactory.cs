﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripleParserConnector.Models;

namespace TripleParserConnector.Factory
{
    public class TicketClassesObjectFactory
    {
        public async Task<ResultItem<List<TicketClassesObjects>>> CreateTicketClassesObjectsList(TicketClassesObjectsRels ticketClassesObjectsRels, Globals globals, string idClass = null)
        {
            var taskResult = await Task.Run<ResultItem<List<TicketClassesObjects>>>(() =>
            {
                var result = new ResultItem<List<TicketClassesObjects>>()
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var objectList = (from projectToTriplePart in ticketClassesObjectsRels.ProjectToTripleParts
                                 join ticketToTriplePart in ticketClassesObjectsRels.TicketsToTripleParts on projectToTriplePart.ID_Other equals ticketToTriplePart.ID_Other
                                 join triplePartToObject in ticketClassesObjectsRels.TriplePartsToObjects on ticketToTriplePart.ID_Other equals triplePartToObject.ID_Object
                                 select new
                                 {
                                     IdProject = projectToTriplePart.ID_Object,
                                     NameProject = projectToTriplePart.Name_Object,
                                     IdTicket = ticketToTriplePart.ID_Object,
                                     NameTicket = ticketToTriplePart.Name_Object,
                                     IdObject = triplePartToObject.ID_Other,
                                     NameObject = triplePartToObject.Name_Other,
                                     IdClass = triplePartToObject.ID_Parent_Other,
                                     NameClass = triplePartToObject.Name_Parent_Other
                                 }).GroupBy(ticket => new { ticket.IdProject, ticket.NameProject, ticket.IdTicket, ticket.NameTicket, ticket.IdObject, ticket.NameObject, ticket.IdClass, ticket.NameClass }).Select(group => new TicketClassesObjects
                                 {
                                     IdProject = group.Key.IdProject,
                                     NameProject = group.Key.NameProject,
                                     IdTicket = group.Key.IdTicket,
                                     NameTicket = group.Key.NameTicket,
                                     IdObject = group.Key.IdObject,
                                     NameObject = group.Key.NameObject,
                                     IdClass = group.Key.IdClass,
                                     NameClass = group.Key.NameClass
                                 });

                if (!string.IsNullOrEmpty(idClass))
                {
                    result.Result = objectList.Where(objectItm => objectItm.IdClass == idClass).ToList();
                }
                else
                {
                    result.Result = objectList.ToList();
                }

                return result;
            });

            

            return taskResult;
        }
    }
}
