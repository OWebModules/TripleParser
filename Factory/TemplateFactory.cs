﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripleParserConnector.Models;
using TripleParserConnector.TripleParserWithTemplates;

namespace TripleParserConnector.Factory
{
    public class TemplateFactory
    {
        public async Task<ResultItem<CreateTemplateList>> CreateTemplateList(ParseTripleOfProject parseTripleOfProject, Globals globals)
        {
            var projectItem = parseTripleOfProject.ProjectItem;

            var result = new ResultItem<CreateTemplateList>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new Factory.CreateTemplateList()
            };

            result.Result.TriplePartList = await Task.Run<List<TriplePart>>(() =>
            {
                var triplePartsTextList = parseTripleOfProject.ParseTemplates.SelectMany(parseTemplate =>
                {
                    

                    var triplePartsTexts = (from parseText in parseTemplate.TemplatesText.Where(templateText => templateText.ID_Object == parseTemplate.ProjectToTriplePart.ID_Other)
                                            join templateType in  parseTemplate.TemplatesToTypes on parseText.ID_Object equals templateType.ID_Object
                                            select new TriplePart
                                            {
                                                TriplePartOItem = new clsOntologyItem
                                                {
                                                    GUID = parseText.ID_Object,
                                                    Name = parseText.Name_Object,
                                                    GUID_Parent = parseText.ID_Class,
                                                    Type = globals.Type_Object,
                                                },
                                                TripleArtTextARel = parseText,
                                                TriplePartType = new clsOntologyItem
                                                {
                                                    GUID = templateType.ID_Other,
                                                    Name = templateType.Name_Other,
                                                    GUID_Parent = templateType.ID_Parent_Other,
                                                    Type = globals.Type_Object
                                                },
                                                ProjectItem = new clsOntologyItem
                                                {
                                                    GUID = parseTemplate.ProjectToTriplePart.ID_Object,
                                                    Name = parseTemplate.ProjectToTriplePart.Name_Object,
                                                    GUID_Parent = parseTemplate.ProjectToTriplePart.ID_Parent_Object,
                                                    Type = globals.Type_Object
                                                },
                                                TripleTerms = (from term in parseTemplate.TemplatesToTripleParts.Where(templ => templ.ID_Other == parseText.ID_Object)
                                                               join classItm in parseTemplate.TemplatesToClasses on term.ID_Object equals classItm.ID_Object into classItms
                                                               from classItm in classItms.DefaultIfEmpty()
                                                               select new TripleTerm
                                                               {
                                                                   TripleTermOItem = new clsOntologyItem
                                                                   {
                                                                       GUID = term.ID_Object,
                                                                       Name = term.Name_Object,
                                                                       GUID_Parent = term.ID_Parent_Object,
                                                                       Type = globals.Type_Object,
                                                                   },
                                                                   ClassItem = classItm != null ? new clsOntologyItem
                                                                   {
                                                                       GUID = classItm.ID_Other,
                                                                       Name = classItm.Name_Other,
                                                                       GUID_Parent = classItm.ID_Parent_Other,
                                                                       Type = classItm.Ontology
                                                                   } : null,
                                                                   OrderId = term.OrderID.Value

                                                               }).OrderBy(term => term.OrderId).ToList()
                                            }).ToList();

                    var templates = triplePartsTexts.SelectMany(tripleParts => tripleParts.TripleTerms).ToList();
                    templates.ForEach(tmpl =>
                     {
                         tmpl.BeforeWords = parseTemplate.TemplatesToWordsBefore.Where(wordBefore => wordBefore.ID_Object == tmpl.TripleTermOItem.GUID).Select(wordBefore => new clsOntologyItem
                         {
                             GUID = wordBefore.ID_Other,
                             Name = wordBefore.Name_Other,
                             Val_Long = wordBefore.OrderID.Value
                         }).OrderBy(word => word.Val_Long.Value).ToList();
                         tmpl.AfterWords = parseTemplate.TemplatesToWordsAfter.Where(wordAfter => wordAfter.ID_Object == tmpl.TripleTermOItem.GUID).Select(wordAfter => new clsOntologyItem
                         {
                             GUID = wordAfter.ID_Other,
                             Name = wordAfter.Name_Other,
                             Val_Long = wordAfter.OrderID.Value
                         }).OrderBy(word => word.Val_Long.Value).ToList();

                         tmpl.Synonym = (from synonymRel in parseTemplate.TemplatesSynonyms.Where(syn => syn.ID_Object == tmpl.TripleTermOItem.GUID)
                                         join templateSyn in templates on synonymRel.ID_Other equals templateSyn.TripleTermOItem.GUID
                                         select templateSyn).FirstOrDefault();

                         tmpl.TripleTermAttribute = (from attributeTermToTemplate in parseTemplate.AttributeTermToTemplates.Where(attributeTerm => attributeTerm.ID_Other == tmpl.TripleTermOItem.GUID)
                                                     join attributeTermToDataType in parseTemplate.AttributeTermToDataTypes on attributeTermToTemplate.ID_Object equals attributeTermToDataType.ID_Object
                                                     select new TripleTermAttribute
                                                     {
                                                         GUID = attributeTermToTemplate.ID_Object,
                                                         Name = attributeTermToTemplate.Name_Object,
                                                         GUID_Parent = attributeTermToTemplate.ID_Parent_Object,
                                                         Type = globals.Type_Object,
                                                         DataType = new clsOntologyItem
                                                         {
                                                             GUID = attributeTermToDataType.ID_Other,
                                                             Name = attributeTermToDataType.Name_Other,
                                                             GUID_Parent = attributeTermToDataType.ID_Parent_Other,
                                                             Type = attributeTermToDataType.Ontology
                                                         }
                                                     }).FirstOrDefault();

                         
                     });
                    return triplePartsTexts;
                }).ToList();



                
                return triplePartsTextList;
            });

            
            return result;
        }
    }

    //public class ResultCreateTemplateList
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public clsOntologyItem ProjectItem { get; set; }
    //    public List<TriplePart> TriplePartList { get; set; }
    //}
    public class CreateTemplateList
    {
        public clsOntologyItem ProjectItem { get; set; }
        public List<TriplePart> TriplePartList { get; set; }
    }
}
