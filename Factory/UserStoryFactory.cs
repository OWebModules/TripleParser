﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripleParserConnector.Models;
using TripleParserConnector.Services;

namespace TripleParserConnector.Factory
{
    public class UserStoryFactory
    {
        public async Task<ResultItem<List<UserStory>>> CreateUserStoryList(TriplePartItems sourceData, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<List<UserStory>>>(() =>
            {
                var userStoryList = (from projectToTriple in sourceData.ProjectToTripleParts
                                     join tripleText in sourceData.TriplePartText on projectToTriple.ID_Other equals tripleText.ID_Object
                                     join tripleObject in sourceData.TriplePartsToObjects on projectToTriple.ID_Other equals tripleObject.ID_Object into tripleObjects
                                     from tripleObject in tripleObjects.DefaultIfEmpty()
                                     select new UserStory
                                     {
                                         IdUserStory = projectToTriple.ID_Other,
                                         Name = tripleText.Val_String,
                                         Priority = projectToTriple.OrderID.Value,
                                         Object = tripleObject?.Name_Other,
                                         Class = tripleObject?.Name_Parent_Other,
                                         Order = tripleObject != null ? tripleObject.OrderID.Value : 1

                                     }).ToList();

                return new ResultItem<List<UserStory>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = userStoryList
                };

            });

            return taskResult;
        }

        public async Task<ResultItem<List<UserStory>>> CreateObjectListOfProject(ObjectsOfClasses sourceData, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<List<UserStory>>>(() =>
            {
                var userStoryList = sourceData.Objects.Select(obj => new UserStory
                {
                    Class = sourceData.RootClass.Name,
                    Object = obj.Name
                }).ToList();

                return new ResultItem<List<UserStory>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = userStoryList
                };

            });

            return taskResult;
        }

        public async Task<ResultItem<List<UserStory>>> GetObjectListOfClasses(TriplePartItems sourceData, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<List<UserStory>>>(() =>
            {
                var userStoryList = (from projectToTriple in sourceData.ProjectToTripleParts
                                     join tripleObject in sourceData.TriplePartsToObjects on projectToTriple.ID_Other equals tripleObject.ID_Object
                                     select tripleObject).GroupBy(obj => new { obj.ID_Other, obj.Name_Other, obj.ID_Parent_Other, obj.Name_Parent_Other }).Select(grp => new UserStory
                                     {
                                         Class = grp.Key.Name_Parent_Other,
                                         Object = grp.Key.Name_Other
                                     }).ToList();



                return new ResultItem<List<UserStory>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = userStoryList
                };

            });

            return taskResult;
        }


    }

    
}
