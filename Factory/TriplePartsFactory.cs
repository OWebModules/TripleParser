﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripleParserConnector.Models;

namespace TripleParserConnector.Factory
{
    public class TriplePartsFactory
    {
        public async Task<ResultItem<List<PrioObject>>> GetPrioObjectList(ProjectsTriplePartsAndObjectsWithPrios objectList, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<List<PrioObject>>>(() =>
            {
                var result = new ResultItem<List<PrioObject>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<PrioObject>()
                };

                var objects = objectList.TriplePartsToObjects.GroupBy(obj => new { obj.ID_Other, obj.Name_Other, obj.ID_Parent_Other, obj.Name_Parent_Other });



                result.Result = (from obj in objects
                                 join att in objectList.ObjectPrios on obj.Key.ID_Other equals att.ID_Object into atts
                                 from att in atts.DefaultIfEmpty()
                                 select new PrioObject
                                 {
                                     IdObject = obj.Key.ID_Other,
                                     NameObject = obj.Key.Name_Other,
                                     IdClass = obj.Key.ID_Parent_Other,
                                     NameClass = obj.Key.Name_Parent_Other,
                                     IdAttributePrio = att?.ID_Attribute,
                                     CountTripleParts = objectList.TriplePartsToObjects.Where(triplePart => triplePart.ID_Other == obj.Key.ID_Other).Count(),
                                     Prio = att?.Val_Lng
                                 }).OrderBy(obj => obj.Prio).ToList();

                return result;
            });

            return taskResult;
        }
    }
}
