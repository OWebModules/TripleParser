﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TripleParserConnector.Models;
using TripleParserConnector.Services;


namespace TripleParserConnector.TripleParser
{
    public class TripleParser : ParseBase
    {
        

        public TripleParser(Globals globals) : base(globals)
        {
            
        }

        

        

        

        private async Task<ResultTriple> ParseTriples(ResultClassesAndObjects classesAndObjects)
        {
            var taskResultTriple = await Task.Run<ResultTriple>(() =>
            {
                var result = new ResultTriple
                {
                    Result = globals.LState_Success.Clone()
                };

                var tripleList = new List<TripleItem>();

                for (int ix = 0; ix < classesAndObjects.FoundClasses.Count - 1; ix++)
                {
                    var classItm = classesAndObjects.FoundClasses[ix];
                    if (classItm.Pre)
                    {
                        var objItm = classesAndObjects.FoundObjects.FirstOrDefault(obj => obj.FoundWord.StartIx > classItm.FoundWord.StartIx);
                        if (objItm != null)
                        {
                            var startIx = 0;
                            var endIx = 0;

                            startIx = classItm.FoundWord.EndIx;
                            endIx = objItm.FoundWord.StartIx;

                            var wordsBetween = classesAndObjects.WordsRest.Where(word => word.StartIx >= startIx && word.EndIx <= endIx);
                            if (wordsBetween.Any())
                            {
                                tripleList.Add(new TripleItem
                                {
                                    FoundClassLeft = classItm,
                                    FoundObject = objItm
                                });
                            }

                        }
                    }
                    else
                    {
                        var objItm = classesAndObjects.FoundObjects.FirstOrDefault(obj => obj.FoundWord.StartIx < classItm.FoundWord.StartIx);

                        if (objItm != null)
                        {
                            var startIx = 0;
                            var endIx = 0;

                            if (classItm.Pre)
                            {
                                startIx = classItm.FoundWord.EndIx;
                                endIx = objItm.FoundWord.StartIx;
                            }
                            else
                            {
                                startIx = objItm.FoundWord.EndIx;
                                endIx = classItm.FoundWord.StartIx;
                            }

                            var wordsBetween = classesAndObjects.WordsRest.Where(word => word.StartIx >= startIx && word.EndIx <= endIx);

                            if (wordsBetween.Any())
                            {
                                tripleList.Add(new TripleItem
                                {
                                    FoundClassLeft = classItm,
                                    FoundObject = objItm
                                });
                            }

                        }
                    }
                    var nextClassItm = classesAndObjects.FoundClasses[ix + 1];
                    tripleList.Add(new TripleItem
                    {
                        FoundClassLeft = classItm,
                        FoundClassRight = nextClassItm
                    });

                }


                if (tripleList.Count == 1 && tripleList.First().FoundObject == null)
                {
                    var relationTypeName = string.Join(" ", classesAndObjects.WordsRest.Select(word => word.Word));
                    var regexSubClass = new Regex(ParseParts.SubClassPattern);
                    //if (!string.IsNullOrEmpty(relationTypeName))
                    //{
                    //    if (regexSubClass.Match(relationTypeName).Success)
                    //    {
                    //        tripleList.First().FoundClassRight.ClassItem.GUID_Parent = tripleList.First().FoundClassLeft.ClassItem.GUID;
                    //    }
                    //}
                }
                else if (tripleList.Count > 1)
                {
                    for (int ix = 0; ix < tripleList.Count; ix++)
                    {
                        var triple = tripleList[ix];
                        var relationTypeItems = tripleList.Select(tripleItm => tripleItm.RelationTypeItem).Where(relItm => relItm != null).ToList();
                        if (triple.FoundObject != null)
                        {
                            var startIx = 0;
                            var endIx = 0;
                            if (triple.FoundClassLeft.Pre)
                            {
                                startIx = triple.FoundClassLeft.FoundWord.EndIx;
                                endIx = triple.FoundObject.FoundWord.StartIx;
                            }
                            else
                            {
                                startIx = triple.FoundObject.FoundWord.EndIx;
                                endIx = triple.FoundClassLeft.FoundWord.StartIx;
                            }

                            var wordsForRelation = classesAndObjects.WordsRest.Where(word => word.StartIx >= startIx && word.EndIx <= endIx).ToList();
                            if (wordsForRelation.Any())
                            {
                                var relationTypeName = string.Join(" ", wordsForRelation.Select(word => word.Word));

                                var relationTypeItem = relationTypeItems.FirstOrDefault(relItm => relItm.RelationType.Name == relationTypeName);
                                if (relationTypeItem == null)
                                {
                                    relationTypeItem = new RelationTypeItem
                                    {
                                        RelationType = new clsOntologyItem
                                        {
                                            Name = relationTypeName,
                                            Type = globals.Type_RelationType
                                        },
                                        RelationWords = wordsForRelation
                                    };

                                }
                                triple.RelationTypeItem = relationTypeItem;

                            }
                        }
                        else
                        {
                            var startIx = 0;
                            var endIx = 0;

                            startIx = triple.FoundClassLeft.FoundWord.EndIx;
                            endIx = triple.FoundClassRight.FoundWord.StartIx;

                            var wordsForRelation = classesAndObjects.WordsRest.Where(word => word.StartIx >= startIx && word.EndIx <= endIx).ToList();
                            if (wordsForRelation.Any())
                            {
                                var relationTypeName = string.Join(" ", wordsForRelation.Select(word => word.Word));
                                var relationTypeItem = relationTypeItems.FirstOrDefault(relItm => relItm.RelationType.Name == relationTypeName);
                                if (relationTypeItem == null)
                                {
                                    relationTypeItem = new RelationTypeItem
                                    {
                                        RelationType = new clsOntologyItem
                                        {
                                            Name = relationTypeName,
                                            Type = globals.Type_RelationType
                                        },
                                        RelationWords = wordsForRelation
                                    };

                                }
                                triple.RelationTypeItem = relationTypeItem;
                            }
                        }
                    }
                }


                result.TripleItems = tripleList;

                return result;
            });

            return taskResultTriple;
        }

        private async Task<ResultParsePartItems> ParsePartItems(ParsePartItem basePart)
        {
            var taskResultTriple = await Task.Run<ResultParsePartItems>(async () =>
            {
                var orderId = basePart.NextOrderId;
                var result = new ResultParsePartItems
                {
                    Result = globals.LState_Success.Clone(),
                    ParseParts = new List<ParsePartItem>()
                };

                var regexPuncuationMark = new Regex(ParseParts.PunctuationMarkPattern);
                var parseMatches = regexPuncuationMark.Matches(basePart.Part);

                var parsePartTypeForFollowing = serviceAgent.ObjectNone;
                var ixStart = 0;

                for (int i = 0; i < parseMatches.Count; i++)
                {
                    orderId += i;
                    var match = parseMatches[i];
                    var ixEnd = match.Index;
                    var text = basePart.Part.Substring(ixStart, ixEnd - ixStart);
                    text = text.Trim();

                    var parsePartItm = new ParsePartItem
                    {
                        StartIx = ixStart,
                        EndIx = ixEnd,
                        BasePart = basePart,
                        Part = text,
                        Words = new List<FoundWord>(),
                        NextOrderId = orderId
                    };

                    ixStart = match.Index + match.Length;
                    var parsePartType = serviceAgent.ObjectAction;

                    if (parsePartTypeForFollowing.GUID != serviceAgent.ObjectNone.GUID)
                    {
                        parsePartType = parsePartTypeForFollowing;
                    }
                    else if (Regex.IsMatch(text, ParseParts.ReasonPattern))
                    {
                        parsePartType = serviceAgent.ObjectReason;
                    }

                    var taskResult = await serviceAgent.CreateTriplePart(basePart.PartOItem, text, true, parsePartType, orderId);
                    if (taskResult.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.Result = taskResult.ResultState;
                        return result;
                    }

                    parsePartItm.PartOItem = taskResult.Result.TriplePart;
                    result.ParseParts.Add(parsePartItm);

                    if (Regex.IsMatch(text, ParseParts.AimPattern))
                    {
                        parsePartTypeForFollowing = serviceAgent.ObjectAim;
                    }
                    else
                    {
                        parsePartTypeForFollowing = serviceAgent.ObjectNone;
                    }

                }



                return result;
            });

            return taskResultTriple;
        }

        
        

        

        private async Task<ResultClassesAndObjects> ParseClassesAndObjects(string sentense)
        {
            var taskResultClassesAndObjects = await Task.Run<ResultClassesAndObjects>(() =>
            {
                var result = new ResultClassesAndObjects
                {
                    Result = logStates.LogState_Success.Clone()
                };


                var regexGUID = new Regex(ParseParts.GUIDPattern);

                var regexIdentitiy = new Regex(ParseParts.IdentityVerbs);

                var matchesGUIDs = regexGUID.Matches(sentense);

                var words = new List<FoundWord>();

                ParseQuotationWords(sentense, words);
                ParseSeperators(sentense, words);

                words = words.OrderBy(match => match.StartIx).ToList();

                // Classes and Objects
                var classList = ParseExplicitClasses(words, matchesGUIDs);
                var matchesGUIDsForObjects = (from match in matchesGUIDs.Cast<Match>()
                                              join matchForClasses in classList.ClassesFound.Select(classFound => classFound.GuidMatch) on match equals matchForClasses into matchesForClasses
                                              from matchForClasses in matchesForClasses.DefaultIfEmpty()
                                              where matchForClasses == null
                                              select match).ToList();

                var wordsRest = (from word in words
                                 join classItm in classList.ClassesFound on word equals classItm.FoundWord into classItems
                                 from classItm in classItems.DefaultIfEmpty()
                                 where classItm == null
                                 select word).ToList();

                var objectList = ParseExplicitObjects(wordsRest, classList, matchesGUIDsForObjects);

                // -------------------------

                wordsRest = (from word in wordsRest
                             join objectItm in objectList.ObjectsFound on word equals objectItm.FoundWord into objectItems
                             from objectItm in objectItems.DefaultIfEmpty()
                             where objectItm == null
                             select word).ToList();

                var regexObjectPattern = new Regex(ParseParts.PreObjectIdentPattern);
                var regexClassPattern = new Regex(ParseParts.PreClassIdentPattern);
                wordsRest = wordsRest.Select(word =>
                {
                    if (!regexClassPattern.Match(word.Word).Success &&
                        !regexObjectPattern.Match(word.Word).Success &&
                        !regexGUID.Match(word.Word).Success)
                    {
                        return word;
                    }

                    return null;
                }).Where(word => word != null).ToList();



                var regexPrefix = new Regex(ParseParts.ClassPrefixPattern);
                var regexPrefixPattern = new Regex(ParseParts.PrefixPattern);

                //var wordsPre = words.Select(word =>
                //{
                //    if (regexPrefix.Match(word.Word).Success || regexPrefixPattern.Match(word.Word).Success)
                //    {
                //        return word;
                //    }
                //    return null;
                //}).Where(word => word != null).ToList();
                var nomen = wordsRest.Where(wrd => wrd.Nomen).ToList();
                for (int ix = 0; ix < nomen.Count; ix++)
                {
                    //var wordPre = wordsPre[ix];

                    //FoundWord wordClass = null;
                    //if (ix < wordsPre.Count - 1)
                    //{
                    //    var wordPreNext = wordsPre[ix + 1];
                    //    var wordClasses = words.ToList().Where(word => word.Nomen && word.StartIx > wordPre.EndIx && word.EndIx < wordPreNext.StartIx);
                    //    wordClass = wordClasses.Select(word =>
                    //    {
                    //        if (regexClassPattern.Match(word.Word).Success) return null;
                    //        if (regexObjectPattern.Match(word.Word).Success) return null;
                    //        if (regexGUID.Match(word.Word).Success) return null;
                    //        if (word.Quoted) return null;
                    //        return word;
                    //    }).FirstOrDefault(word => word != null);


                    //}
                    //else
                    //{
                    //    var wordClasses = words.ToList().Where(word => word.Nomen && word.StartIx >= wordPre.EndIx);
                    //    wordClass = wordClasses.Select(word =>
                    //    {
                    //        if (regexClassPattern.Match(word.Word).Success) return null;
                    //        if (regexObjectPattern.Match(word.Word).Success) return null;
                    //        if (regexGUID.Match(word.Word).Success) return null;
                    //        if (regexIdentitiy.Match(word.Word).Success) return null;
                    //        if (word.Quoted) return null;
                    //        return word;
                    //    }).FirstOrDefault(word => word != null);

                    //}

                    var isClass = true;
                    var wordClass = nomen[ix];

                    if (wordClass == null) continue;

                    var wordsBefore = words.Where(word => word.StartIx < wordClass.StartIx).OrderByDescending(word => word.StartIx);
                    var wordBefore = wordsBefore.FirstOrDefault();
                    FoundClass parentClass = null;

                    if (wordBefore != null)
                    {
                        if (wordBefore.Nomen)
                        {
                            isClass = false;
                            parentClass = classList.ClassesFound.FirstOrDefault(cls => cls.FoundWord == wordBefore);
                        }
                    }

                    var idItm = matchesGUIDs.Cast<Match>().FirstOrDefault(itm => itm.Index > wordClass.EndIx && itm.Index < wordClass.StartIx);

                    if (isClass)
                    {
                        if (classList.ClassesFound.Any(cls => cls.ClassItem.Name.ToLower() == wordClass.Word.Replace("(", "").Replace(")", "").ToLower())) continue;
                        if (idItm != null)
                        {
                            var foundClass = new FoundClass
                            {
                                GuidMatch = idItm,
                                ClassItem = new clsOntologyItem
                                {
                                    GUID = idItm.Value,
                                    Name = wordClass.Word.Replace("(", "").Replace(")", ""),
                                    Type = globals.Type_Class
                                },
                                FoundWord = wordClass,
                                Pre = true
                            };
                            classList.ClassesFound.Add(foundClass);
                        }
                        else
                        {
                            var foundClass = new FoundClass
                            {
                                ClassItem = new clsOntologyItem
                                {
                                    Name = wordClass.Word.Replace("(", "").Replace(")", ""),
                                    Type = globals.Type_Class
                                },
                                FoundWord = wordClass,
                                Pre = true
                            };
                            classList.ClassesFound.Add(foundClass);
                        }
                    }
                    else
                    {
                        if (parentClass != null)
                        {
                            objectList.ObjectsFound.Add(new FoundObject
                            {
                                ClassItem = parentClass,
                                FoundWord = wordClass,
                                GuidMatch = idItm,
                                ObjectItem = new clsOntologyItem
                                {
                                    GUID = globals.NewGUID,
                                    Name = wordClass.Word,
                                    GUID_Parent = parentClass.ClassItem.GUID,
                                    Type = globals.Type_Object
                                }
                            });
                        }
                    }



                }

                wordsRest = (from word in wordsRest
                             join classItm in classList.ClassesFound on word equals classItm.FoundWord into classItems
                             from classItm in classItems.DefaultIfEmpty()
                             where classItm == null
                             select word).ToList();


                wordsRest = (from word in wordsRest
                             join objectItm in objectList.ObjectsFound on word equals objectItm.FoundWord into objectItems
                             from objectItm in objectItems.DefaultIfEmpty()
                             where objectItm == null
                             select word).ToList();



                wordsRest = wordsRest.Select(word =>
                {
                    if (!regexPrefixPattern.Match(word.Word).Success &&
                        !regexPrefix.Match(word.Word).Success)
                    {
                        return word;
                    }
                    return null;
                }).Where(word => word != null).ToList();

                var wordsQuoted = words.Where(word => word.Quoted).ToList();
                var classesPre = (from cls in classList.ClassesFound.Where(cls => cls.Pre)
                                  join obj in objectList.ObjectsFound on cls.ClassItem equals obj.ClassItem.ClassItem into objs
                                  from obj in objs.DefaultIfEmpty()
                                  where obj == null
                                  select cls).OrderByDescending(cls => cls.FoundWord.StartIx);

                var classesPost = (from cls in classList.ClassesFound.Where(cls => !cls.Pre)
                                   join obj in objectList.ObjectsFound on cls.ClassItem equals obj.ClassItem.ClassItem into objs
                                   from obj in objs.DefaultIfEmpty()
                                   where obj == null
                                   select cls);

                foreach (var word in wordsQuoted)
                {
                    var objectItem = new FoundObject
                    {
                        FoundWord = word
                    };

                    var preClass = classesPre.FirstOrDefault(cls => cls.FoundWord.EndIx <= word.StartIx);
                    if (preClass != null)
                    {
                        objectItem.ObjectItem = new clsOntologyItem
                        {
                            GUID = globals.NewGUID,
                            Name = word.Word,
                            Type = globals.Type_Object
                        };

                        var matches = (from match in matchesGUIDsForObjects
                                       join objectItm in objectList.ObjectsFound.Where(obj => obj.GuidMatch != null) on match equals objectItem.GuidMatch into objectItms
                                       from objectItm in objectItms.DefaultIfEmpty()
                                       where objectItm == null
                                       select match).ToList();

                        if (matches.Any())
                        {
                            objectItem.ObjectItem.GUID = matches.First().Value;
                        }

                        objectItem.ClassItem = preClass;
                        objectList.ObjectsFound.Add(objectItem);
                        continue;
                    }

                    var postClass = classesPost.FirstOrDefault(cls => cls.FoundWord.StartIx > word.EndIx);
                    if (postClass != null)
                    {
                        objectItem.ObjectItem = new clsOntologyItem
                        {
                            GUID = globals.NewGUID,
                            Name = word.Word,
                            Type = globals.Type_Object
                        };

                        var matches = (from match in matchesGUIDsForObjects
                                       join objectItm in objectList.ObjectsFound.Where(obj => obj.GuidMatch != null) on match equals objectItem.GuidMatch into objectItms
                                       from objectItm in objectItms.DefaultIfEmpty()
                                       where objectItm == null
                                       select match).ToList();

                        if (matches.Any())
                        {
                            objectItem.ObjectItem.GUID = matches.First().Value;
                        }

                        objectItem.ClassItem = postClass;
                        objectList.ObjectsFound.Add(objectItem);
                    }
                }

                wordsRest = (from word in wordsRest
                             join quotedWord in wordsQuoted on word.Word equals quotedWord.Word into quotedWords
                             from quotedWord in quotedWords.DefaultIfEmpty()
                             where quotedWord == null
                             select word).ToList();
                var classListOrdered = classList.ClassesFound.OrderBy(cls => cls.FoundWord.StartIx).ToList();
                var objectListOrdered = objectList.ObjectsFound.OrderBy(obj => obj.FoundWord.StartIx).ToList();

                result.FoundClasses = classListOrdered;
                result.FoundObjects = objectListOrdered;
                result.WordsRest = wordsRest;

                return result;
            });

            return taskResultClassesAndObjects;
        }

        



        public async Task<ResultParse> ParseText(clsOntologyItem projectItem, string sentense, long orderId)
        {
            var parseResult = new ResultParse
            {
                Result = globals.LState_Success.Clone()
            };


            serviceAgent = new ServiceAgentElastic(globals);


            var triplePartResult = await serviceAgent.CreateTriplePart(projectItem, sentense, true, serviceAgent.ObjectBase, orderId);

            if (triplePartResult.ResultState.GUID == globals.LState_Error.GUID)
            {
                parseResult.Result = triplePartResult.ResultState;
                return parseResult;
            }

            var triplePart = new ParsePartItem
            {
                BasePart = null,
                StartIx = 0,
                EndIx = 0,
                Part = sentense,
                NextOrderId = 1,
                PartOItem = triplePartResult.Result.TriplePart
            };

            var taskResult = await ParsePartItems(triplePart);

            foreach (var parsePartItm in taskResult.ParseParts)
            {
                var taskResultClassesAndObjects = await ParseClassesAndObjects(parsePartItm.Part);

                taskResultClassesAndObjects.FoundObjects.ForEach(obj =>
                {
                    obj.ObjectItem.GUID_Parent = obj.ClassItem.ClassItem.GUID;
                });

                var resultAgent = await serviceAgent.CheckClasses(taskResultClassesAndObjects.FoundClasses.Select(cls => cls.ClassItem).ToList());

                foreach (var classItm in taskResultClassesAndObjects.FoundClasses)
                {
                    if (string.IsNullOrEmpty(classItm.ClassItem.GUID_Parent))
                    {
                        classItm.ClassItem.GUID_Parent = serviceAgent.ClassProject.GUID;
                    }
                }

                var classes = taskResultClassesAndObjects.FoundClasses.Select(fc => fc.ClassItem).ToList();
                var taskResultClasses = await serviceAgent.CreateClasses(classes, null);

                if (taskResultClasses.GUID == globals.LState_Error.GUID)
                {
                    parseResult.Result = taskResultClasses;
                    return parseResult;
                }

                if (taskResultClassesAndObjects.FoundObjects.Any())
                {
                    taskResultClassesAndObjects.FoundObjects.ForEach(obj =>
                    {
                        if (string.IsNullOrEmpty(obj.ObjectItem.GUID_Parent))
                        {
                            obj.ObjectItem.GUID_Parent = obj.ClassItem.ClassItem.GUID;
                        }
                    });

                    var objects = taskResultClassesAndObjects.FoundObjects.Select(cls => cls.ObjectItem).ToList();
                    var taskResultCheckObjects = await serviceAgent.CheckObjects(objects);

                    if (taskResultCheckObjects.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        parseResult.Result = taskResultCheckObjects.ResultState;
                        return parseResult;
                    }

                    var objectsToCreate = objects.Where(obj => obj.New_Item != null && obj.New_Item.Value).ToList();
                    if (objectsToCreate.Any())
                    {
                        var taskResultObjects = await serviceAgent.CreateObjects(objectsToCreate);

                        if (taskResultObjects.GUID == globals.LState_Error.GUID)
                        {
                            parseResult.Result = taskResultObjects;
                            return parseResult;
                        }
                    }

                    if (taskResultClassesAndObjects.FoundObjects.Any())
                    {
                        var taskResultRelationsObjects = await serviceAgent.CreateObjectRelations(taskResultClassesAndObjects.FoundObjects.Select(obj => obj.ObjectItem).ToList(), parsePartItm.PartOItem);
                        if (taskResultRelationsObjects.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            parseResult.Result = taskResultRelationsObjects.ResultState;
                            return parseResult;
                        }
                    }

                }


                var taskResultRelations = await serviceAgent.CreateClassRelations(classes, parsePartItm.PartOItem);
                if (taskResultRelations.ResultState.GUID == globals.LState_Error.GUID)
                {
                    parseResult.Result = taskResultRelations.ResultState;
                    return parseResult;
                }
                //var taskResultTriple = await ParseTriples(taskResultClassesAndObjects);
            }





            return parseResult;
        }

        private FoundObjects ParseExplicitObjects(List<FoundWord> words, FoundClasses foundClasses, List<Match> matchesGUIDs)
        {
            var objectList = new FoundObjects()
            {
                ObjectsFound = new List<FoundObject>()
            };

            var regexObjectPattern = new Regex(ParseParts.PreObjectIdentPattern);
            var regexClassPatternPost = new Regex(ParseParts.Brackets);
            var regexGUIDs = new Regex(ParseParts.GUIDPattern);

            for (int ix = 0; ix < words.Count; ix++)
            {
                if (regexObjectPattern.Match(words[ix].Word).Success && ix < words.Count - 1)
                {
                    var objectItem = new FoundObject
                    {
                        FoundWord = words[ix + 1]
                    };

                    var postClasses = (from postClass in foundClasses.ClassesFound
                                       join objectClass in objectList.ObjectsFound on postClass equals objectClass.ClassItem into objectClasses
                                       from objectClass in objectClasses.DefaultIfEmpty()
                                       where objectClass == null
                                       select postClass).OrderBy(postClass => postClass.FoundWord.StartIx).ToList();

                    var postClassItm = postClasses.FirstOrDefault(postClass => postClass.FoundWord.StartIx > objectItem.FoundWord.EndIx);

                    if (postClassItm != null)
                    {
                        objectItem.ClassItem = postClassItm;
                        objectList.ObjectsFound.Add(objectItem);
                    }

                }
                else if (ix < words.Count - 1 && regexClassPatternPost.Match(words[ix + 1].Word).Success && !regexGUIDs.Match(words[ix + 1].Word).Success)
                {
                    var objectItem = new FoundObject
                    {
                        FoundWord = words[ix]
                    };

                    var postClasses = (from postClass in foundClasses.ClassesFound
                                       join objectClass in objectList.ObjectsFound on postClass equals objectClass.ClassItem into objectClasses
                                       from objectClass in objectClasses.DefaultIfEmpty()
                                       where objectClass == null
                                       select postClass).OrderBy(postClass => postClass.FoundWord.StartIx).ToList();

                    var postClassItm = postClasses.FirstOrDefault(postClass => postClass.FoundWord.StartIx > objectItem.FoundWord.EndIx);

                    if (postClassItm != null)
                    {
                        objectItem.ClassItem = postClassItm;
                        objectList.ObjectsFound.Add(objectItem);
                    }

                }
            }

            for (int ix = 0; ix < objectList.ObjectsFound.Count; ix++)
            {
                var objectItm = objectList.ObjectsFound[ix];

                if (ix < objectList.ObjectsFound.Count - 1)
                {
                    var nextObjectItm = objectList.ObjectsFound[ix + 1];
                    var idItm = matchesGUIDs.Cast<Match>().FirstOrDefault(itm => itm.Index > objectItm.FoundWord.EndIx && itm.Index < nextObjectItm.FoundWord.StartIx);
                    if (idItm != null)
                    {
                        objectItm.GuidMatch = idItm;
                        objectItm.ObjectItem = new clsOntologyItem
                        {
                            GUID = idItm.Value,
                            Name = objectItm.FoundWord.Word,
                            Type = globals.Type_Class
                        };
                    }
                    else
                    {
                        objectItm.ObjectItem = new clsOntologyItem
                        {
                            GUID = globals.NewGUID,
                            Name = objectItm.FoundWord.Word,
                            Type = globals.Type_Class
                        };
                    }
                }
                else
                {
                    var idItm = matchesGUIDs.Cast<Match>().FirstOrDefault(itm => itm.Index > objectItm.FoundWord.EndIx);
                    if (idItm != null)
                    {
                        objectItm.GuidMatch = idItm;
                        objectItm.ObjectItem = new clsOntologyItem
                        {
                            GUID = idItm.Value,
                            Name = objectItm.FoundWord.Word,
                            Type = globals.Type_Class
                        };
                    }
                    else
                    {
                        objectItm.ObjectItem = new clsOntologyItem
                        {
                            GUID = globals.NewGUID,
                            Name = objectItm.FoundWord.Word,
                            Type = globals.Type_Class
                        };
                    }
                }
            }

            return objectList;
        }

        private FoundClasses ParseExplicitClasses(List<FoundWord> words, MatchCollection matchesGUIDs)
        {
            var classList = new FoundClasses
            {
                ClassesFound = new List<FoundClass>(),
            };

            var regexClassPattern = new Regex(ParseParts.PreClassIdentPattern);
            var regexClassPatternPost = new Regex(ParseParts.Brackets);
            var regexGUIDs = new Regex(ParseParts.GUIDPattern);

            for (int ix = 0; ix < words.Count; ix++)
            {
                if (regexClassPattern.Match(words[ix].Word).Success && ix < words.Count - 1)
                {
                    if (!classList.ClassesFound.Any(cls => cls.FoundWord.Word.ToLower() == words[ix + 1].Word.ToLower()))
                    {
                        classList.ClassesFound.Add(new FoundClass { FoundWord = words[ix + 1], Pre = true });
                    }

                }
                else if (regexClassPatternPost.Match(words[ix].Word).Success && !regexGUIDs.Match(words[ix].Word).Success)
                {
                    if (!classList.ClassesFound.Any(cls => cls.FoundWord.Word.ToLower() == words[ix].Word.ToLower()))
                    {
                        classList.ClassesFound.Add(new FoundClass { FoundWord = words[ix], Pre = false });
                    }

                }
            }

            for (int ix = 0; ix < classList.ClassesFound.Count; ix++)
            {
                var foundClass = classList.ClassesFound[ix];

                if (ix < classList.ClassesFound.Count - 1)
                {
                    var nextClass = classList.ClassesFound[ix + 1];

                    var nextClassItm = nextClass;
                    var idItm = matchesGUIDs.Cast<Match>().FirstOrDefault(itm => itm.Index > foundClass.FoundWord.EndIx && itm.Index < nextClassItm.FoundWord.StartIx);

                    if (idItm != null)
                    {
                        foundClass.GuidMatch = idItm;
                        foundClass.ClassItem = new clsOntologyItem
                        {
                            GUID = idItm.Value,
                            Name = classList.ClassesFound[ix].FoundWord.Word.Replace("(", "").Replace(")", ""),
                            Type = globals.Type_Class
                        };
                    }
                    else
                    {
                        foundClass.ClassItem = new clsOntologyItem
                        {
                            Name = classList.ClassesFound[ix].FoundWord.Word.Replace("(", "").Replace(")", ""),
                            Type = globals.Type_Class
                        };
                    }
                }
                else
                {
                    var idItm = matchesGUIDs.Cast<Match>().FirstOrDefault(itm => itm.Index > classList.ClassesFound[ix].FoundWord.EndIx);
                    if (idItm != null)
                    {
                        foundClass.GuidMatch = idItm;
                        foundClass.ClassItem = new clsOntologyItem
                        {
                            GUID = idItm.Value,
                            Name = classList.ClassesFound[ix].FoundWord.Word.Replace("(", "").Replace(")", ""),
                            Type = globals.Type_Class
                        };
                    }
                    else
                    {
                        foundClass.ClassItem = new clsOntologyItem
                        {
                            Name = classList.ClassesFound[ix].FoundWord.Word.Replace("(", "").Replace(")", ""),
                            Type = globals.Type_Class
                        };
                    }
                }
            }

            return classList;
        }
    }

    public class TripleItem
    {
        public FoundClass FoundClassLeft { get; set; }
        public RelationTypeItem RelationTypeItem { get; set; }
        public FoundClass FoundClassRight { get; set; }
        public FoundObject FoundObject { get; set; }
    }

    public class RelationTypeItem
    {
        public clsOntologyItem RelationType { get; set; }
        public List<FoundWord> RelationWords { get; set; }
    }

    

    

    public class ResultParsePartItems
    {
        public clsOntologyItem Result { get; set; }
        public List<ParsePartItem> ParseParts { get; set; }
    }


    

    public class FoundObject
    {
        public FoundWord FoundWord { get; set; }
        public FoundClass ClassItem { get; set; }
        public clsOntologyItem ObjectItem { get; set; }
        public Match GuidMatch { get; set; }
    }

    public class FoundObjects
    {
        public List<FoundObject> ObjectsFound { get; set; }
    }

    public class ResultClassesAndObjects
    {
        public clsOntologyItem Result;
        public List<FoundClass> FoundClasses { get; set; }
        public List<FoundObject> FoundObjects { get; set; }
        public List<FoundWord> WordsRest { get; set; }
    }

    public class ResultTriple
    {
        public clsOntologyItem Result { get; set; }
        public List<TripleItem> TripleItems { get; set; }
    }
}
