﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TripleParserConnector;
using TripleParserConnector.Factory;
using TripleParserConnector.Models;
using TripleParserConnector.Services;
using TripleParserConnector.TripleParserWithTemplates;

namespace TripleParserConnector
{
    public class TripleParserConnector
    {
        private Globals globals;
        public Globals Globals => globals;
        private clsLogStates logStates = new clsLogStates();
        private TripleParser.TripleParser tripleParser;
        private ServiceAgentElastic serviceAgent;
        private ProjectFactory factory;
        private TemplateFactory templateFactory;

        public async Task<ResultParse> ProcessSentense(string project, string sentense)
        {
            var projectResult = await tripleParser.CreateProject(project);
            var result = await tripleParser.ParseText(projectResult.Result, sentense, 1);
            return result;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetProjectList()
        {
            var result = await serviceAgent.GetProjectList();

            var projectListResult = await factory.CreateProjectList(result.Result);

            return projectListResult;
        }

        public async Task<ResultItem<TriplePartItems>> GetTripleParts(string idProject)
        {
            var result = await serviceAgent.GetTripleParts(idProject);
            return result;
        }

        public async Task<ResultItem<ClassesAndObjectsWithRootItem>> GetClassesAndObjectsOfTriplePart(string idTriplePart, string idProject)
        {
            var result = await serviceAgent.GetClassesAndObjectsOfTriplePart(idTriplePart, idProject);
            return result;
        }

        public async Task<ResultItem<ClassesAndObjectsWithRootItem>> GetClassesAndObjectsOfProjects(string idProject)
        {
            var result = await serviceAgent.GetClassesAndObjectsOfProject(idProject);
            return result;
        }

        public async Task<ResultItem<ProjectsAndUserStories>> GetProjectsAndUserStories(string idObject)
        {
            var result = await serviceAgent.GetProjectsAndUserStories(idObject);
            return result;
        }
        public async Task<ResultItem<List<TicketClassesObjects>>> GetTicketClassesObjects(string idProject, string idClass)
        {
            var resultElastic = await serviceAgent.GetTicketClassesObjects(idProject);

            var result = new ResultItem<List<TicketClassesObjects>>
            {
                ResultState = resultElastic.ResultState
            };

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            var factory = new TicketClassesObjectFactory();

            result = await factory.CreateTicketClassesObjectsList(resultElastic.Result, globals, idClass);

            return result;

        }

        public async Task<ResultItem<List<UserStory>>> GetUserStories(string idProject)
        {
            var result = await serviceAgent.GetUserStories(idProject);
            var factory = new UserStoryFactory();
            var resultFactory = await factory.CreateUserStoryList(result.Result, globals);
            return resultFactory;
        }

        public async Task<ResultItem<List<UserStory>>> GetUserStoriesWithRelatedObjects(string idProject)
        {
            var result = await serviceAgent.GetUserStoriesWithRelatedObjects(idProject);
            var factory = new UserStoryFactory();
            var resultFactory = await factory.CreateUserStoryList(result.Result, globals);
            return resultFactory;
        }

        public async Task<ResultItem<List<UserStory>>> GetRelatedObjectsOfProject(string idProject)
        {
            var result = await serviceAgent.GetObjectsOfClasses(idProject);
            var factory = new UserStoryFactory();
            var resultFactory = await factory.CreateObjectListOfProject(result.Result, globals);
            return resultFactory;
        }


        public async Task<ResultItem<List<UserStory>>> GetObjectsOfClasses(string idClass)
        {
            var result = await serviceAgent.GetObjectsOfClasses(idClass);
            var factory = new UserStoryFactory();
            var resultFactory = await factory.CreateObjectListOfProject(result.Result, globals);
            return resultFactory;
        }

        /// <summary>
        /// Parse Template-Text, get Terms, before- and after-words
        /// Create Template-Object and relate to the project-Object
        /// </summary>
        /// <param name="project">Name of the project to be related to the </param>
        /// <param name="template"></param>
        /// <returns></returns>
        public async Task<ResultTripleParts> ParseTemplate(string project, string template)
        {
            var templateParser = new TemplateParser.TemplateParser(globals);

            var result = new ResultTripleParts
            {
                Result = globals.LState_Success.Clone()
            };

            // Get Terms, before- and after-words
            var resultParse = await templateParser.ParseTemplate(template);
            if (resultParse.ResultState.GUID == globals.LState_Error.GUID)
            {
                result.Result = resultParse.ResultState;
                return result;
            }

            result = await templateParser.CreateTemplateTerms(project, resultParse.Result);
            
            return result;
        }

        public async Task<ResultTripleParts> ParseTemplate(clsOntologyItem projectItem, string template)
        {
            var templateParser = new TemplateParser.TemplateParser(globals);

            var result = new ResultTripleParts
            {
                Result = globals.LState_Success.Clone()
            };
            var resultParse = await templateParser.ParseTemplate(template);
            if (resultParse.ResultState.GUID == globals.LState_Error.GUID)
            {
                result.Result = resultParse.ResultState;
                return result;
            }

            result = await templateParser.CreateTemplateTerms(projectItem, resultParse.Result);

            return result;
        }

        public async Task<ResultItem<clsOntologyItem>> GetObjectItem(string idItem)
        {
            var result = await serviceAgent.GetOItem(idItem, globals.Type_Object);

            return result;
        }

        public async Task<ResultItem<clsOntologyItem>> GetClassItem(string idItem)
        {
            var result = await serviceAgent.GetOItem(idItem, globals.Type_Class);

            return result;
        }

        public async Task<ResultItem<clsOntologyItem>> CheckProject(string name)
        {
            var result = await serviceAgent.CheckProject(name);
            
            return result;
        }

        public async Task<ResultItem<ProjectAndUserStory>> GetProjectAndUserStoryByUserStoryId(string idUserStory)
        {
            var result = await serviceAgent.GetProjectAndUserStoryByUserStoryId(idUserStory);

            return result;

        }

        public async Task<ResultGetProjectById> GetProjectById(string idProject)
        {
            var result = await serviceAgent.GetOItem(idProject, globals.Type_Object);
            return new ResultGetProjectById { Result = result.Result, ProjectItem = result.Result };

        }

        public async Task<ResultItem<CreateTriplePart>> CheckTemplate(clsOntologyItem project, string nameOrId)
        {
            var result = await serviceAgent.CheckTriplePart(project, nameOrId, serviceAgent.ObjectTemplate, 1);
            return result;
        }

        public async Task<ResultItem<CreateTriplePart>> CheckUserStory(clsOntologyItem project, string name)
        {
            var result = await serviceAgent.CheckTriplePart(project, name, serviceAgent.ObjectBase, 1);
            return result;
        }

        public async Task<ResultItem<TriplePartsOfProjects>> GetUserStoriesOfProjects()
        {
            var result = await serviceAgent.GetUserStoriesOfProjects();

            return result;
        }

        public async Task<ResultItem<List<PrioObject>>> GetPrioObjects(string idProject, string idClass)
        {
            
            var resultDbRead = await serviceAgent.GetObjectListWithPrio(idProject, idClass);

            var factory = new TriplePartsFactory();
            var result = await factory.GetPrioObjectList(resultDbRead.Result, globals);

            return result;
            
        }

        public async Task<ResultItem<PrioObject>> ChangePrio(PrioObject prioObject, string newPrioStr)
        {
            var result = new ResultItem<PrioObject>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = prioObject
            };
                
            long newPrio;

            if (!long.TryParse(newPrioStr,out newPrio))
            {
                result.ResultState = globals.LState_Error.Clone();
                return result;
            }

            result.ResultState = await serviceAgent.ChangePrio(prioObject.IdObject, prioObject.IdAttributePrio, newPrio);
            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            result.Result.Prio = newPrio;
            result.Result.IdAttributePrio = result.ResultState.GUID_Related;

            return result;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetClassesAndObjects()
        {
            var result = await serviceAgent.GetClassesAndObjects();

            return result;
        }

        public async Task<ResultItem<List<Models.ParsedTriplePart>>> ParseText(List<Models.TriplePart> tripleParts, string templateName, string text, string idProject)
        {
            var resultProject = await serviceAgent.GetOItem(idProject, globals.Type_Object);
            var resultTemplate = await CheckTemplate(resultProject.Result, templateName);

            var tripleParser = new TripleParserWithTemplates.TripleParserWithTemplates(globals);

            var userStories = text.Split('\n');

            var resultParsed = new ResultItem<List<Models.ParsedTriplePart>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<ParsedTriplePart>()
            };

            foreach (var userStory in userStories)
            {
                var resultCheckUserStory = await CheckUserStory(resultProject.Result, userStory);
                var resultParse = await tripleParser.ParseText(tripleParts, resultProject.Result, text, 1, serviceAgent.ObjectBase);
                if (resultParse.ResultState.GUID == globals.LState_Error.GUID)
                {
                    resultParsed.Result = resultParse.Result;
                    break;
                }
                resultParsed.Result.AddRange(resultParse.Result);

            }

            return resultParsed;


        }

        public async Task<ResultItem<CreateTemplateList>> GetTemplateList(string idProject)
        {
            
            var tripleParser = new TripleParserWithTemplates.TripleParserWithTemplates(globals);



            var resultParsed = new ResultItem<CreateTemplateList>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new CreateTemplateList
                {
                    TriplePartList = new List<Models.TriplePart>()
                }
            };

            var resultProject = await serviceAgent.GetOItem(idProject, globals.Type_Object);

            if (resultProject.Result.GUID == globals.LState_Error.GUID)
            {
                resultParsed.ResultState = resultProject.Result;
                return resultParsed;
            }

            var resultTemplates = await serviceAgent.GetTemplates(resultProject.Result);

            if (resultTemplates.ResultState.GUID == globals.LState_Error.GUID)
            {
                resultParsed.ResultState = resultTemplates.ResultState;
                return resultParsed;
            }

            resultParsed = await templateFactory.CreateTemplateList(resultTemplates.Result, globals);
            resultParsed.Result.ProjectItem = resultProject.Result;
            return resultParsed;
        }

        /// <summary>
        /// Parse a text by Templates
        /// </summary>
        /// <param name="text">The text to be parsed</param>
        /// <param name="createTemplateList">The list of Templates with Terms and befor- and after-words</param>
        /// <returns>List of found Triple Parts in text</returns>
        public async Task<ResultItem<List<ParsedTriplePart>>> ParseText(string text, CreateTemplateList createTemplateList)
        {
            
            var resultParsed = new ResultItem<List<ParsedTriplePart>>()
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<ParsedTriplePart>()
            };

            if (string.IsNullOrEmpty(text)) return resultParsed;

            var tripleParseWithTemplates = new TripleParserWithTemplates.TripleParserWithTemplates(globals);
            
            //ToDo: OrderId of the text should be provided by the method-entry
            var taskResult = await tripleParseWithTemplates.ParseText(createTemplateList.TriplePartList, createTemplateList.ProjectItem, text, 1, serviceAgent.ObjectBase);
            resultParsed.Result.AddRange(taskResult.Result);
            var count = resultParsed.Result.Count;
            if (count> 9)
            {
                count = 9;
            }
            resultParsed.Result = resultParsed.Result.OrderByDescending(triplePart => triplePart.Efficiency).ThenByDescending(triplePart => triplePart.FoundTerms.Count).ToList().GetRange(0, count);
            return resultParsed;


        }
        public async Task<ResultItem<TicketTemplates>> GetTicketTemplates(clsOntologyItem project)
        {
            var resultTicketTemplates = await serviceAgent.GetTicketTemplates(project);
            var result = new ResultItem<TicketTemplates>();
            if (resultTicketTemplates.ResultState.GUID == globals.LState_Error.GUID)
            {
                result.ResultState = resultTicketTemplates.ResultState;
                return result;
            }

            var factory = new Factory.TicketTemplateFactory();
            result = await factory.CreateTicketTemplateList(resultTicketTemplates.Result, globals);

            return result;
        }

        public async Task<clsOntologyItem> ParseTicket(TicketTemplates ticketTemplates, string ticketText)
        {
            var ticketTextLower = ticketText.ToLower();
            var resultGetTicketTemplates = await serviceAgent.GetObjectsOfTicketTemplates(ticketTemplates);
            var objectItems = new List<clsOntologyItem>();

            if (resultGetTicketTemplates.GUID == globals.LState_Error.GUID)
            {
                return resultGetTicketTemplates;
            }

            foreach (var ticketTemplateClass in ticketTemplates.TicketTemplatesClasses)
            {
                ticketTemplateClass.IsContained(ticketText);
            }

            foreach (var ticketTemplateRegex in ticketTemplates.TicketTemplatesRegex)
            {
                ticketTemplateRegex.IsContained(ticketText, globals);
            }

            //foreach (var ticketTemplateClass in ticketTemplates.TicketTemplatesClasses)
            //{
            //    foreach (var objectItem in ticketTemplateClass.ObjectItems)
            //    {
            //        var index = ticketTextLower.IndexOf(objectItem.Name.ToLower());
            //        if (index >= 0)
            //        {
            //            objectItems.Add(objectItem);
            //            ticketTemplateClass.TextTags.Add(new TextTag
            //            {
            //                Reference = objectItem,
            //                Start = index,
            //                End = index + objectItem.Name.Length
            //            });
            //        }
            //    }
            //}

            //foreach (var ticketTemplateRegex in ticketTemplates.TicketTemplatesRegex)
            //{
            //    var foundObject = false;
            //    foreach (var objectItem in ticketTemplateRegex.ObjectItems)
            //    {
            //        var index = ticketTextLower.IndexOf(objectItem.Name.ToLower());
            //        if (index >= 0)
            //        {
            //            objectItems.Add(objectItem);
            //            foundObject = true;
            //            ticketTemplateRegex.TextTags.Add(new TextTag
            //            {
            //                Reference = objectItem,
            //                Start = index,
            //                End = index + objectItem.Name.Length
            //            });
            //        }
            //    }

            //    if (!foundObject)
            //    {
            //        var matches = ticketTemplateRegex.Regex.Matches(ticketText);
            //        if (matches.Count > 0)
            //        {
            //            foreach  (Match match in matches)
            //            {
            //                var objectItem = objectItems.FirstOrDefault(obj => obj.Name == match.Value);
            //                if (objectItem == null)
            //                {
            //                    objectItem = new clsOntologyItem
            //                    {
            //                        GUID = globals.NewGUID,
            //                        Name = match.Value,
            //                        GUID_Parent = ticketTemplateRegex.ToClassItem.ID_Other,
            //                        Type = globals.Type_Object,
            //                        New_Item = true
            //                    };
            //                    objectItems.Add(objectItem);
            //                    ticketTemplateRegex.ObjectItems.Add(objectItem);
            //                }

            //                ticketTemplateRegex.TextTags.Add(new TextTag
            //                {
            //                    Reference = objectItem,
            //                    Start = match.Index,
            //                    End = match.Index + match.Length
            //                });

            //            }
            //        }
            //    }
            //}
            //var tripleParser = new TripleParserWithTemplates.TripleParserWithTemplates(globals);

            //var words = tripleParser.FindWords(ticketText);



            return resultGetTicketTemplates;
        }

        public async Task<clsOntologyItem> SaveTicketTags(string idTriplePart, TicketTemplates ticketTemplates, string[] idTags)
        {
            var result = globals.LState_Success.Clone();

            var tagsToSave = (from textTag in ticketTemplates.TicketTemplatesClasses.SelectMany(classes => classes.TextTags)
                              join idTag in idTags on textTag.Id equals idTag
                              select textTag).ToList();

            tagsToSave.AddRange(from textTag in ticketTemplates.TicketTemplatesRegex.SelectMany(classes => classes.TextTags)
                                join idTag in idTags on textTag.Id equals idTag
                                select textTag);

            var objects = tagsToSave.OrderBy(textTag => textTag.Start).Select(tag => tag.Reference);

            result = await serviceAgent.SaveTicketRelations(idTriplePart, objects.ToList());

            return result;
        }

        public async Task<clsOntologyItem> SaveTemplates(List<Models.TriplePart> templates)
        {
            var result = await serviceAgent.SaveTemplates(templates);
            return result;
        }

        public async Task<clsOntologyItem> SaveParsedUserStory(ParsedTriplePart parsedTriplePart)
        {
            var result = globals.LState_Success.Clone();

            result = await serviceAgent.SaveTripleParts(new List<ParsedTriplePart> { parsedTriplePart });

            return result;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetProjectClasses(string classStartsWith)
        {
            return await serviceAgent.GetProjectClasses(classStartsWith);
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetWords(string wordStartsWith)
        {
            return await serviceAgent.GetWords(wordStartsWith);
        }

        public TripleParserConnector(string configPath)
        {
            globals = new Globals(configPath);
            tripleParser = new TripleParser.TripleParser(globals);
            serviceAgent = new ServiceAgentElastic(globals);
            factory = new ProjectFactory(globals);
            templateFactory = new TemplateFactory();
        }
    }


    public class ResultProcess
    {
        public clsOntologyItem Result { get; set; }
    }

    //public class ResultGetProjectList
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public List<clsOntologyItem> TipleProjectList { get; set; }
    //}

    public class ResultGetTriplePartsOfTriplePart
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem RootItem { get; set; }
        public List<clsObjectRel> TriplePartsToTripleParts { get; set; }
    }

    //public class ResultGetUserStories
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public List<UserStory> UserStories { get; set; }
    //}

    public class ResultTemplateParseText
    {
        public clsOntologyItem Result { get; set; }
        public List<TemplateParseItem> TemplateParseItems { get; set; }
    }

    public class TemplateParseItem
    {
        public clsOntologyItem TriplePartTemplate { get; set; }
        public List<Models.TemplateParse> TemplateTerms { get; set; }
    }

}
