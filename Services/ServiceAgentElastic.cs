﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using StructureCreateModule.Attributes;
using TripleParserConnector.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripleParserConnector.TripleParserWithTemplates;

namespace TripleParserConnector.Services
{
    public class ServiceAgentElastic
    {
        Globals globals;

        private clsRelationConfig relationConfig;


        /// <summary>
        /// Save Project, Template, Terms and before- and after-words
        /// </summary>
        /// <param name="tripleParts">The List of Triple Parts</param>
        /// <returns></returns>
        public async Task<clsOntologyItem> SaveTemplates(List<Models.TriplePart> tripleParts)
        {
            var relationConfig = new clsRelationConfig(globals);
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var dbWriter = new OntologyModDBConnector(globals);

                var triplePartsToSave = tripleParts;
                // Projects to check
                var oItemsToCheck = tripleParts.GroupBy(triplePartGroup => new { triplePartGroup.ProjectItem.Name }).Select(triplePartGroup => new clsOntologyItem
                {
                    Name = triplePartGroup.Key.Name,
                    GUID_Parent = ClassProject.GUID,
                    Type = globals.Type_Object
                }).ToList();

                // TripleParts to check
                oItemsToCheck.AddRange(tripleParts.Select(triplePart => triplePart.TriplePartOItem));
                // TripleTerms to check
                var tripleTermsToCheck = triplePartsToSave.SelectMany(triplePart => triplePart.TripleTerms.Select(tripleTerm => tripleTerm));
                oItemsToCheck.AddRange(tripleTermsToCheck.Select(tripleTerm => tripleTerm.TripleTermOItem));

                // Words to check
                oItemsToCheck.AddRange(tripleParts.SelectMany(triplePart => triplePart.TripleTerms).SelectMany(tripleTerm => tripleTerm.BeforeWords));
                oItemsToCheck.AddRange(tripleParts.SelectMany(triplePart => triplePart.TripleTerms).SelectMany(tripleTerm => tripleTerm.AfterWords));


                var result = globals.LState_Success.Clone();

                // Check objects, if the are not in the Triple Store, the will be marked as a new item.
                var resultCheck = await CheckObjects(oItemsToCheck);
                result = resultCheck.ResultState;
                if (result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                // Get the information from the check
                (from projectItem in tripleParts.Select(triplePart => triplePart.ProjectItem)
                 join projectToCheck in oItemsToCheck.Where(oItem => oItem.GUID_Parent == ClassProject.GUID) on projectItem.Name.ToLower() equals projectToCheck.Name.ToLower()
                 select new { projectItem, projectToCheck }).ToList().ForEach(project =>
                   {

                       project.projectItem.GUID = project.projectToCheck.GUID;
                       project.projectItem.New_Item = project.projectToCheck.New_Item;

                   });

                // Save only items which are not new
                var oItemsToSave = oItemsToCheck.Where(oItem => oItem.New_Item != null && oItem.New_Item.Value).ToList();


                // Relation between projects and Triple Parts (Templates)
                var projectsToTripleParts = triplePartsToSave.Select(triplePart => relationConfig.Rel_ObjectRelation(triplePart.ProjectItem, triplePart.TriplePartOItem, RelationTypeContains, orderId: triplePart.OrderId)).ToList();

                // Relation between the Triple Part and the Text, which can have more then 255 characters, for this, an attribute must be used
                var triplePartsToText = triplePartsToSave.Select(triplePart => relationConfig.Rel_ObjectAttribute(triplePart.TriplePartOItem, AttributeTypeTripleText, triplePart.TriplePartText)).ToList();

                // Relation between the Triple Part and the Type of the Triple Part (Template)
                var triplePartType = triplePartsToSave.Select(triplePart => relationConfig.Rel_ObjectRelation(triplePart.TriplePartOItem, triplePart.TriplePartType, RelationTypeIsOfType)).ToList();

                // Existence of Classes of Terms must be checked
                var classesToCheck = tripleTermsToCheck.Select(term => term.ClassItem).ToList();

                var resultClassCheck = await CheckClasses(classesToCheck);

                if (resultClassCheck.GUID == globals.LState_Error.GUID)
                {
                    result = resultClassCheck;
                    return result;
                }

                // Save only classes which are new
                var classesToSave = classesToCheck.Where(cls => cls.New_Item != null && cls.New_Item.Value).ToList();

                var templateTermsToTripleParts = new List<clsObjectRel>();
                var templateTermsToWords = new List<clsObjectRel>();
                var templateTermsToClasses = new List<clsObjectRel>();

                // Relation between the Triple Parts and the Terms
                triplePartsToSave.ToList().ForEach(triplePart =>
                {
                    long orderId = 1;
                    var rel = triplePart.TripleTerms.Select(term => relationConfig.Rel_ObjectRelation(term.TripleTermOItem, triplePart.TriplePartOItem, globals.RelationType_belongsTo, orderId: orderId++)).ToList();
                    templateTermsToTripleParts.AddRange(rel);
                });

                // Relation between the Terms and the Words
                tripleTermsToCheck.ToList().ForEach(term =>
                {
                    var rel = term.BeforeWords.Select(word => relationConfig.Rel_ObjectRelation(term.TripleTermOItem, word, RelationTypeBefore, orderId: word.Val_Long.Value));
                    templateTermsToWords.AddRange(rel);
                    var rels = term.AfterWords.Select(word => relationConfig.Rel_ObjectRelation(term.TripleTermOItem, word, RelationTypeAfter, orderId: word.Val_Long.Value));
                    templateTermsToWords.AddRange(rels);
                    templateTermsToClasses.Add(relationConfig.Rel_ObjectRelation(term.TripleTermOItem, term.ClassItem, globals.RelationType_belongingClass));

                });

                // Relation between the Terms and their Synonyms
                var termplateTermSynonyms = tripleTermsToCheck.Where(term => term.Synonym != null).Select(term => relationConfig.Rel_ObjectRelation(term.TripleTermOItem, term.Synonym.TripleTermOItem, RelationTypeIsSynonymFor)).ToList();

                // Save the Triple Store Items which are new
                if (oItemsToSave.Any())
                {
                    result = dbWriter.SaveObjects(oItemsToSave);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                // Save the Relations between projects and Triple Parts
                if (projectsToTripleParts.Any())
                {
                    result = dbWriter.SaveObjRel(projectsToTripleParts);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                // Save the name-attribute of a Triple Part, if the name is longer than 255 characters
                if (triplePartsToText.Any())
                {
                    result = dbWriter.SaveObjAtt(triplePartsToText);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }


                // Save the Relations between the Triple Parts and their Type (Template)
                if (triplePartType.Any())
                {
                    result = dbWriter.SaveObjRel(triplePartType);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }


                // Save the classes
                if (classesToSave.Any())
                {
                    result = dbWriter.SaveClass(classesToSave);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                // Save the relation between the Triple Parts and their Terms
                if (templateTermsToTripleParts.Any())
                {
                    result = dbWriter.SaveObjRel(templateTermsToTripleParts);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                // Save the relation between the Terms and their Words
                if (templateTermsToWords.Any())
                {
                    result = dbWriter.SaveObjRel(templateTermsToWords);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                // Save the relation between the Terms and the Classes
                if (templateTermsToClasses.Any())
                {
                    result = dbWriter.SaveObjRel(templateTermsToClasses);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                // Save the relation between the Terms and their Synonyms
                if (termplateTermSynonyms.Any())
                {
                    result = dbWriter.SaveObjRel(termplateTermSynonyms);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }
                return result;
            });

            return taskResult;

        }

        public async Task<clsOntologyItem> SaveTripleParts(List<Models.ParsedTriplePart> tripleParts)
        {
            var relationConfig = new clsRelationConfig(globals);
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var result = globals.LState_Success.Clone();
                var dbWriter = new OntologyModDBConnector(globals);

                var triplePartsToSave = tripleParts.Where(triplePart => triplePart.BestResult && triplePart.TriplePart.New_Item != null && triplePart.TriplePart.New_Item.Value).ToList();
                var projectsToTripleParts = triplePartsToSave.Select(triplePart => relationConfig.Rel_ObjectRelation(triplePart.ProjectItem, triplePart.TriplePart, RelationTypeContains, orderId: triplePart.OrderId)).ToList();
                var triplePartsToText = triplePartsToSave.Select(triplePart => triplePart.TriplePartText).ToList();
                var triplePartType = triplePartsToSave.Select(triplePart => relationConfig.Rel_ObjectRelation(triplePart.TriplePart, triplePart.TriplePartType, RelationTypeIsOfType)).ToList();
                var templateToTriplePart = tripleParts.Select(triplePart => relationConfig.Rel_ObjectRelation(triplePart.TriplePart, triplePart.Template.TriplePartOItem, RelationTypeContains, orderId: (long)triplePart.ParseSuccessPercent)).ToList();

                var foundTerms = tripleParts.SelectMany(triplePart => triplePart.FoundTerms).ToList();
                var foundObjects = foundTerms.Where(foundTerm => foundTerm.ObjectItem != null).Select(foundTerm => foundTerm.ObjectItem);
                var foundAttributes = foundTerms.Where(foundTerm => foundTerm.AttributeMeta != null);
                var attributeTypesToCheck = foundAttributes.Where(foundTerm => foundTerm.AttributeMeta != null).Select(foundTerm => foundTerm.AttributeMeta.AttributeType).
                    GroupBy(foundAttributeType => new { foundAttributeType.Name }).Select(foundAttributeType => new clsOntologyItem
                    {
                        Name = foundAttributeType.Key.Name
                    }).ToList();

                var resultCheck = await CheckAttributeTypes(attributeTypesToCheck);

                (from attributeTypeToCheck in attributeTypesToCheck
                 join foundAttribute in foundAttributes on attributeTypeToCheck.Name.ToLower() equals foundAttribute.AttributeMeta.AttributeType.Name.ToLower()
                 select new { attributeTypeToCheck, foundAttribute }).ToList().ForEach(attributeType =>
                 {
                     attributeType.foundAttribute.AttributeMeta.AttributeType = attributeType.attributeTypeToCheck;
                 });

                if (resultCheck.GUID == globals.LState_Error.GUID)
                {
                    result = resultCheck;
                }

                var attributes = (from attributeType in attributeTypesToCheck
                                  join foundTerm in foundAttributes on attributeType.Name.ToLower() equals foundTerm.AttributeMeta.AttributeType.Name.ToLower()
                                  select new
                                  {
                                      attributeType,
                                      foundTerm
                                  }).ToList();




                var objectsToCheck = foundObjects.GroupBy(foundObject => new { foundObject.Name, foundObject.GUID_Parent }).Select(foundObjGroup => new clsOntologyItem { Name = foundObjGroup.Key.Name, GUID_Parent = foundObjGroup.Key.GUID_Parent, Type = globals.Type_Object }).ToList();

                var resultCheckObjects = await CheckObjects(objectsToCheck);

                (from obj in foundTerms.Where(foundTerm => foundTerm.ObjectItem != null).Select(foundTerm => foundTerm.ObjectItem)
                 join objectChecked in objectsToCheck on new { Name = obj.Name.ToLower(), GUID_Parent = obj.GUID_Parent } equals new { Name = objectChecked.Name.ToLower(), GUID_Parent = objectChecked.GUID_Parent }
                 select new { obj, objectChecked }).ToList().ForEach(obj =>
                   {
                       obj.obj.GUID = obj.objectChecked.GUID;
                       obj.obj.New_Item = obj.objectChecked.New_Item;
                   });

                var objectItemsToSave = foundTerms.Where(foundTerm => foundTerm.ObjectItem != null && foundTerm.ObjectItem.New_Item != null && foundTerm.ObjectItem.New_Item.Value).Select(foundTerm => foundTerm.ObjectItem).ToList();



                // var attributesToSave = attributes.GroupBy(attribute => new { attribute.foundTerm.AttributeMeta.TermWithAttribute.ObjectItem, attribute.attributeType, attribute.foundTerm.AttributeMeta.AttributeValue }).Select(attribute =>
                // {
                //     return relationConfig.Rel_ObjectAttribute(attribute.Key.ObjectItem, attribute.Key.attributeType, attribute.Key.AttributeValue);
                // }
                //).ToList();

                //var classAttributes = (from attributeType in attributeTypesToCheck
                //                       from classItm in foundTerms.Select(foundTerm => foundTerm.TemplateTerm.ClassItem)
                //                       select new { attributeType, classItm }).Select(attClass =>
                //                       {
                //                           var rel = new clsClassAtt
                //                           {
                //                               ID_Class = attClass.classItm.GUID,
                //                               ID_AttributeType = attClass.attributeType.GUID,
                //                               ID_DataType = attClass.attributeType.GUID_Parent,
                //                               Min = 1,
                //                               Max = 1
                //                           };
                //                           return rel;
                //                       }).ToList();

                if (resultCheckObjects.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return resultCheckObjects.ResultState;
                }

                var triplePartToObjectItem = tripleParts.SelectMany(triplePart =>
                {
                    var triplePartItem = triplePart.TriplePart;
                    return (from foundTerm in triplePart.FoundTerms.Where(foundTerm => foundTerm.ObjectItem != null)
                            join attribute in attributes.Where(attributeItm => attributeItm.attributeType.GUID_Parent == globals.DType_Int.GUID) on foundTerm.ObjectItem.GUID equals attribute.foundTerm.AttributeMeta.TermWithAttribute.ObjectItem.GUID into attributeItems
                            from attribute in attributeItems.DefaultIfEmpty()
                            select new { foundTerm, attribute }).Select(term =>
                              {
                                  var rel = relationConfig.Rel_ObjectRelation(triplePartItem, term.foundTerm.ObjectItem, RelationTypeContains, orderId: term.attribute != null ? (long)term.attribute.foundTerm.AttributeMeta.AttributeValue : 1);
                                  return rel;
                              });

                }).ToList();



                var synonyms = foundTerms.Where(foundTerm => foundTerm.ObjectItemSynonym != null).Select(foundTerm => relationConfig.Rel_ObjectRelation(foundTerm.ObjectItem, foundTerm.ObjectItemSynonym, RelationTypeIsSynonymFor));


                var attributeTypesToSave = attributeTypesToCheck.Where(attributeType => attributeType.New_Item != null && attributeType.New_Item.Value).ToList();
                if (attributeTypesToSave.Any())
                {
                    result = dbWriter.SaveAttributeTypes(attributeTypesToSave);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                if (triplePartsToSave.Any())
                {
                    result = dbWriter.SaveObjects(triplePartsToSave.Select(triplePart => triplePart.TriplePart).ToList());
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                if (projectsToTripleParts.Any())
                {
                    result = dbWriter.SaveObjRel(projectsToTripleParts);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                if (templateToTriplePart.Any())
                {
                    result = dbWriter.SaveObjRel(templateToTriplePart);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                if (triplePartsToText.Any())
                {
                    result = dbWriter.SaveObjAtt(triplePartsToText);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                if (triplePartType.Any())
                {
                    result = dbWriter.SaveObjRel(triplePartType);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                if (objectItemsToSave.Any())
                {
                    result = dbWriter.SaveObjects(objectItemsToSave);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                if (triplePartToObjectItem.Any())
                {
                    result = dbWriter.SaveObjRel(triplePartToObjectItem);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                //if (attributesToSave.Any())
                //{
                //    result = dbWriter.SaveObjAtt(attributesToSave);
                //    if (result.GUID == globals.LState_Error.GUID)
                //    {
                //        return result;
                //    }
                //}

                //if (classAttributes.Any())
                //{
                //    result = dbWriter.SaveClassAtt(classAttributes);
                //    if (result.GUID == globals.LState_Error.GUID)
                //    {
                //        return result;
                //    }
                //}

                return result;
            });

            return taskResult;

        }

        /// <summary>
        /// Check existance of Objects and mark Objects as new if the are not existing
        /// </summary>
        /// <param name="foundObjects">Objects to check</param>
        /// <param name="parentItem">Class of Objects</param>
        /// <returns>List of checked Objects</returns>
        public async Task<ResultItem<List<clsOntologyItem>>> CheckObjects(List<clsOntologyItem> foundObjects, clsOntologyItem parentItem = null)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var dbReader = new OntologyModDBConnector(globals);

                var searchObjects = foundObjects.Select(obj => new clsOntologyItem { Name = obj.Name, GUID_Parent = obj.GUID_Parent }).ToList();
                if (parentItem != null)
                {
                    var searchObjRel = new clsObjectRel { ID_Object = parentItem.GUID, ID_Parent_Other = ClassTriplePart.GUID };
                    result.ResultState = dbReader.GetDataObjectRel(new List<clsObjectRel> { searchObjRel });
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    (from foundObject in foundObjects
                     join existingObject in dbReader.ObjectRels on new { Name = foundObject.Name, GUID_Parent = foundObject.GUID_Parent } equals new { Name = existingObject.Name_Other, GUID_Parent = existingObject.ID_Parent_Other } into existingObjects
                     from existingObject in existingObjects.DefaultIfEmpty()
                     select new { foundObject, existingObject }).ToList().ForEach(mergeObj =>
                     {
                         if (mergeObj.existingObject != null)
                         {
                             mergeObj.foundObject.GUID = mergeObj.existingObject.ID_Other;
                         }
                         else
                         {
                             mergeObj.foundObject.GUID = globals.NewGUID;
                             mergeObj.foundObject.New_Item = true;
                         }

                     });

                    result.Result = foundObjects.Where(obj => !string.IsNullOrEmpty(obj.GUID)).ToList();
                }
                else
                {
                    if (foundObjects.Any())
                    {
                        result.ResultState = dbReader.GetDataObjects(searchObjects);
                        if (result.ResultState.GUID == globals.LState_Success.GUID)
                        {
                            (from foundObject in foundObjects
                             join existingObject in dbReader.Objects1 on new { Name = foundObject.Name, GUID_Parent = foundObject.GUID_Parent } equals new { Name = existingObject.Name, GUID_Parent = existingObject.GUID_Parent } into existingObjects
                             from existingObject in existingObjects.DefaultIfEmpty()
                             select new { foundObject, existingObject }).ToList().ForEach(mergeObj =>
                             {
                                 if (mergeObj.existingObject != null)
                                 {
                                     mergeObj.foundObject.GUID = mergeObj.existingObject.GUID;
                                 }
                                 else
                                 {
                                     mergeObj.foundObject.GUID = globals.NewGUID;
                                     mergeObj.foundObject.New_Item = true;
                                 }

                             });

                            result.Result = foundObjects.Where(obj => !string.IsNullOrEmpty(obj.GUID)).ToList();
                        }
                    }
                }




                return result;
            });

            return taskResult;

        }


        public async Task<clsOntologyItem> CheckClasses(List<clsOntologyItem> foundClasses)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var foundClassesWithOutGiuds = foundClasses.Where(cls => string.IsNullOrEmpty(cls.GUID)).ToList();

                var classesWithoutGuids = foundClassesWithOutGiuds.GroupBy(cls => cls.Name).Select(cls => new clsOntologyItem
                {
                    Name = cls.Key
                }).ToList();

                var dbReader = new OntologyModDBConnector(globals);

                var result = dbReader.GetDataClasses(classesWithoutGuids);

                if (result.GUID == globals.LState_Success.GUID)
                {
                    (from classWithOutGuid in classesWithoutGuids
                     join classInDb in dbReader.Classes1 on classWithOutGuid.Name.ToLower() equals classInDb.Name.ToLower() into classesInDb
                     from classInDb in classesInDb.DefaultIfEmpty()
                     where classInDb == null
                     select new { classWithOutGuid, classInDb }).ToList().ForEach(cls =>
                      {
                          cls.classWithOutGuid.GUID = globals.NewGUID;
                          cls.classWithOutGuid.New_Item = true;
                      });

                    

                    (from foundClass in foundClasses
                     join withOutGuid in classesWithoutGuids on foundClass.Name.ToLower() equals withOutGuid.Name.ToLower()
                     select new { foundClass, withOutGuid }).ToList().ForEach(cls =>
                     {
                         cls.foundClass.GUID = cls.withOutGuid.GUID;
                         cls.foundClass.New_Item = true;
                     });

                    (from foundClass in foundClasses
                     join classInDb in dbReader.Classes1 on foundClass.Name.ToLower() equals classInDb.Name.ToLower()
                     select new { foundClass, classInDb }).ToList().ForEach(cls =>
                     {
                         cls.foundClass.GUID = cls.classInDb.GUID;
                         cls.foundClass.New_Item = false;
                     });


                    var dbWriter = new OntologyModDBConnector(globals);
                    var classesToSave = foundClasses.Where(foundClass => foundClass.New_Item != null && foundClass.New_Item.Value).ToList();
                    if (classesToSave.Any())
                    {
                        var resultWriter = dbWriter.SaveClass(classesToSave);
                        result = resultWriter;
                    }

                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> CheckAttributeTypes(List<clsOntologyItem> foundAttributeTypes)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var foundAttributeTypesWithOutGiuds = foundAttributeTypes.Where(cls => string.IsNullOrEmpty(cls.GUID)).ToList();

                var attributeTypesWithoutGuids = foundAttributeTypesWithOutGiuds.Select(cls => new clsOntologyItem
                {
                    Name = cls.Name
                }).ToList();

                var dbReader = new OntologyModDBConnector(globals);

                var result = dbReader.GetDataAttributeType(attributeTypesWithoutGuids);

                if (result.GUID == globals.LState_Success.GUID)
                {

                    (from foundAttributeType in foundAttributeTypesWithOutGiuds
                     join attributeTypeInDb in dbReader.AttributeTypes on foundAttributeType.Name.ToLower() equals attributeTypeInDb.Name.ToLower()
                     select new { foundAttributeType, attributeTypeInDb }).ToList().ForEach(cls =>
                     {
                         cls.foundAttributeType.GUID = cls.attributeTypeInDb.GUID;
                         cls.foundAttributeType.Name = cls.attributeTypeInDb.Name;
                         cls.foundAttributeType.GUID_Parent = cls.attributeTypeInDb.GUID_Parent;
                         cls.foundAttributeType.Type = cls.attributeTypeInDb.Type;

                     });

                    var newGuids = foundAttributeTypesWithOutGiuds.GroupBy(attributeType => attributeType.Name.ToLower()).Select(attributeTypeGroup => attributeTypeGroup.Key).ToList().Select(attributeT => new clsOntologyItem
                    {
                        GUID = globals.NewGUID,
                        Name = attributeT,
                        New_Item = true
                    }).ToList();

                    (from foundAttributeType in foundAttributeTypesWithOutGiuds
                     join newGuid in newGuids on foundAttributeType.Name.ToLower() equals newGuid.Name.ToLower()
                     select new { foundAttributeType, newGuid }).ToList().ForEach(attributeType =>
                    {
                        attributeType.foundAttributeType.GUID = attributeType.newGuid.GUID;
                        attributeType.foundAttributeType.New_Item = true;
                    });

                    var dbWriter = new OntologyModDBConnector(globals);
                    var attributeTypesToSave = foundAttributeTypes.Where(attributeType => attributeType.New_Item != null && attributeType.New_Item.Value).ToList();
                    if (attributeTypesWithoutGuids.Any())
                    {
                        var resultWriter = dbWriter.SaveClass(attributeTypesToSave);
                        result = resultWriter;
                    }

                    var foundAttributeTypesWithGiuds = foundAttributeTypes.Where(cls => !string.IsNullOrEmpty(cls.GUID)).ToList();
                    var attributeTypesWithGuids = foundAttributeTypesWithGiuds.Select(cls => new clsOntologyItem
                    {
                        GUID = cls.GUID
                    }).ToList();

                    result = dbReader.GetDataAttributeType(attributeTypesWithGuids);

                    if (result.GUID == globals.LState_Success.GUID)
                    {
                        (from foundAttributeType in foundAttributeTypesWithGiuds
                         join attributeTypeInDb in dbReader.AttributeTypes on foundAttributeType.GUID equals attributeTypeInDb.GUID
                         select new { foundAttributeType, attributeTypeInDb }).ToList().ForEach(cls =>
                         {
                             cls.foundAttributeType.Name = cls.attributeTypeInDb.Name;
                         });

                        //if (foundClassesWithGiuds.Any())
                        //{
                        //    var resultWriter = dbWriter.SaveClass(foundClassesWithGiuds);
                        //    result.Result = resultWriter;
                        //}
                    }
                }

                return result;
            });

            return taskResult;
        }

        #region BaseStrcutre

        [BaseOStructure(Order = 1)]
        public clsOntologyItem AttributeTypeTripleText => new clsOntologyItem
        {
            GUID = "e580c17544ed4e18aeddf778ac7bb3b3",
            Name = "Triple-Text",
            GUID_Parent = globals.DType_String.GUID,
            Type = globals.Type_AttributeType
        };

        [BaseOStructure(Order = 1)]
        public clsOntologyItem AttributeTypePrio => new clsOntologyItem
        {
            GUID = "e9df605e71b442c89a0124d987e34eb4",
            Name = "Prio",
            GUID_Parent = globals.DType_Int.GUID,
            Type = globals.Type_AttributeType
        };

        [BaseOStructure(Order = 1)]
        public clsOntologyItem AttributeTypeRegEx => new clsOntologyItem
        {
            GUID = "22e93da2894a45d497d15e1711aa5657",
            Name = "RegEx",
            GUID_Parent = globals.DType_String.GUID,
            Type = globals.Type_AttributeType
        };

        [BaseOStructure(Order = 1)]
        public clsOntologyItem ClassProject => new clsOntologyItem
        {
            GUID = "dca1d3d97f194f788d6934dd0de87461",
            Name = "Project (Triple-Parser)",
            GUID_Parent = globals.Root.GUID,
            Type = globals.Type_Class
        };

        [BaseOStructure(Order = 2)]
        public clsOntologyItem ClassTriplePart => new clsOntologyItem
        {
            GUID = "930a7878fe3e4d86b3f85fbb9d74e4b5",
            Name = "Triple-Part",
            GUID_Parent = ClassProject.GUID,
            Type = globals.Type_Class
        };

        [BaseOStructure(Order = 2)]
        public clsOntologyItem ClassTicket => new clsOntologyItem
        {
            GUID = "3e5b6f4c823f4aa48012f6b6c563d936",
            Name = "Ticket",
            GUID_Parent = ClassProject.GUID,
            Type = globals.Type_Class
        };

        [BaseOStructure(Order = 2)]
        public clsOntologyItem ClassRegularExpressionToClass => new clsOntologyItem
        {
            GUID = "1db525d631df4d86a4098709e3e200ec",
            Name = "Regular Expression To Class",
            GUID_Parent = ClassProject.GUID,
            Type = globals.Type_Class
        };

        [BaseOStructure(Order = 2)]
        public clsOntologyItem RelationTypeContains => new clsOntologyItem
        {
            GUID = "e971160347db44d8a476fe88290639a4",
            Name = "contains",
            Type = globals.Type_RelationType
        };

        [BaseOStructure(Order = 2)]
        public clsOntologyItem RelationTypeBefore => new clsOntologyItem
        {
            GUID = "235c7535b95d45439da9b75dc60ae324",
            Name = "before",
            Type = globals.Type_RelationType
        };

        [BaseOStructure(Order = 2)]
        public clsOntologyItem RelationTypeAfter => new clsOntologyItem
        {
            GUID = "dbc7fc72cca247c481d9111c012d9704",
            Name = "after",
            Type = globals.Type_RelationType
        };

        [BaseOStructure(Order = 2)]
        public clsOntologyItem RelationTypeIs => new clsOntologyItem
        {
            GUID = "3e104b75e01c48a0b04112908fd446a0",
            Name = "is",
            Type = globals.Type_RelationType
        };

        [BaseOStructure(Order = 2)]
        public clsOntologyItem RelationTypeBelongingClass => new clsOntologyItem
        {
            GUID = "f2b54f82ada5460eafe5551d55629f14",
            Name = "belonging Class",
            Type = globals.Type_RelationType
        };

        [BaseOStructure(Order = 3)]
        public clsClassRel ClassRelationProjectToPart => new clsClassRel
        {
            ID_Class_Left = ClassProject.GUID,
            ID_Class_Right = ClassTriplePart.GUID,
            ID_RelationType = RelationTypeContains.GUID,
            Max_Forw = -1,
            Max_Backw = -1,
            Min_Forw = 1,
            Ontology = "Class"
        };

        [BaseOStructure(Order = 4)]
        public clsClassRel ClassRelationPartToPart => new clsClassRel
        {
            ID_Class_Left = ClassTriplePart.GUID,
            ID_Class_Right = ClassTriplePart.GUID,
            ID_RelationType = RelationTypeContains.GUID,
            Max_Forw = -1,
            Max_Backw = -1,
            Min_Forw = 0,
            Ontology = "Class"
        };

        [BaseOStructure(Order = 4)]
        public clsClassRel ClassRelationTicketContainsTriplePart => new clsClassRel
        {
            ID_Class_Left = ClassTicket.GUID,
            ID_Class_Right = ClassTriplePart.GUID,
            ID_RelationType = RelationTypeContains.GUID,
            Max_Forw = -1,
            Max_Backw = -1,
            Min_Forw = 0,
            Ontology = "Class"
        };

        [BaseOStructure(Order = 4)]
        public clsClassRel ClassRelationTriplePartContains => new clsClassRel
        {
            ID_Class_Left = ClassTriplePart.GUID,
            ID_RelationType = RelationTypeContains.GUID,
            Max_Forw = -1,
            Max_Backw = -1,
            Min_Forw = 0,
            Ontology = "Other"
        };

        [BaseOStructure(Order = 4)]
        public clsClassRel ClassRelationTriplePartToBelongingClass => new clsClassRel
        {
            ID_Class_Left = ClassTriplePart.GUID,
            ID_RelationType = RelationTypeBelongingClass.GUID,
            Max_Forw = -1,
            Max_Backw = -1,
            Min_Forw = 0,
            Ontology = "Other"
        };

        [BaseOStructure(Order = 4)]
        public clsOntologyItem ClassPartType => new clsOntologyItem
        {
            GUID = "a826f24716b44b82b54b81cb1fa0755d",
            Name = "Part-Type (Triple-Parser",
            GUID_Parent = ClassTriplePart.GUID,
            Type = globals.Type_Class
        };

        [BaseOStructure(Order = 3)]
        public clsOntologyItem ClassTemplateParser => new clsOntologyItem
        {
            GUID = "414f8b09d2774c179bf447e2a9801fa7",
            Name = "Template-Parser",
            GUID_Parent = ClassProject.GUID,
            Type = globals.Type_Class
        };

        [BaseOStructure(Order = 4)]
        public clsOntologyItem ClassWord => new clsOntologyItem
        {
            GUID = "1b23f612709542998a8255f5c6cdca5d",
            Name = "Word",
            GUID_Parent = ClassProject.GUID,
            Type = globals.Type_Class
        };

        [BaseOStructure(Order = 4)]
        public clsOntologyItem ClassTemplateTerm => new clsOntologyItem
        {
            GUID = "6232a9e94735400c9c9629c0e176fef5",
            Name = "Template-Term",
            GUID_Parent = ClassTemplateParser.GUID,
            Type = globals.Type_Class
        };

        [BaseOStructure(Order = 4)]
        public clsOntologyItem ClassDataTypes => new clsOntologyItem
        {
            GUID = "69e1c4ac593e4ebb907c9b2bcec595de",
            Name = "DataTypes",
            GUID_Parent = ClassTemplateParser.GUID,
            Type = globals.Type_Class
        };

        [BaseOStructure(Order = 4)]
        public clsOntologyItem ClassTemplateAttributeTerm => new clsOntologyItem
        {
            GUID = "b28e4a328c5a4766bbea3e8519e2a4ab",
            Name = "Template-AttributeTerm",
            GUID_Parent = ClassTemplateParser.GUID,
            Type = globals.Type_Class
        };

        [BaseOStructure(Order = 5)]
        public clsOntologyItem RelationTypeIsOfType => new clsOntologyItem
        {
            GUID = "9996494aef6a4357a6ef71a92b5ff596",
            Name = "is of Type",
            Type = globals.Type_RelationType
        };

        [BaseOStructure(Order = 5)]
        public clsOntologyItem RelationTypeIsDefinedBy => new clsOntologyItem
        {
            GUID = "cd32af17bab349da857b60c5ac809e4e",
            Name = "is defined by",
            Type = globals.Type_RelationType
        };

        [BaseOStructure(Order = 5)]
        public clsOntologyItem RelationTypeIsSynonymFor => new clsOntologyItem
        {
            GUID = "b9f39d8c31be4c3e8da4c49277a5c80a",
            Name = "Is Synonym for",
            Type = globals.Type_RelationType
        };

        [BaseOStructure(Order = 6)]
        public clsClassRel ClassRelationPartToType => new clsClassRel
        {
            ID_Class_Left = ClassTriplePart.GUID,
            ID_Class_Right = ClassPartType.GUID,
            ID_RelationType = RelationTypeIsOfType.GUID,
            Max_Forw = 1,
            Max_Backw = -1,
            Min_Forw = 1,
            Ontology = "Class"
        };

        [BaseOStructure(Order = 6)]
        public clsClassRel ClassRelationTermSynonym => new clsClassRel
        {
            ID_Class_Left = ClassTemplateTerm.GUID,
            ID_Class_Right = ClassTemplateTerm.GUID,
            ID_RelationType = RelationTypeIsSynonymFor.GUID,
            Max_Forw = 0,
            Max_Backw = -1,
            Min_Forw = 1,
            Ontology = "Class"
        };


        [BaseOStructure(Order = 6)]
        public clsClassRel ClassRelationAttributeTermToTeplateTerm => new clsClassRel
        {
            ID_Class_Left = ClassTemplateAttributeTerm.GUID,
            ID_Class_Right = ClassTemplateTerm.GUID,
            ID_RelationType = RelationTypeIs.GUID,
            Max_Forw = 1,
            Max_Backw = -1,
            Min_Forw = 1,
            Ontology = "Class"
        };

        [BaseOStructure(Order = 6)]
        public clsClassRel ClassRelationAttributeTermIsOfType => new clsClassRel
        {
            ID_Class_Left = ClassTemplateAttributeTerm.GUID,
            ID_Class_Right = ClassDataTypes.GUID,
            ID_RelationType = RelationTypeIsOfType.GUID,
            Max_Forw = 1,
            Max_Backw = -1,
            Min_Forw = 1,
            Ontology = "Class"
        };

        [BaseOStructure(Order = 6)]
        public clsClassRel ClassRelationTemplateToTriplePart => new clsClassRel
        {
            ID_Class_Left = ClassTemplateTerm.GUID,
            ID_Class_Right = ClassTriplePart.GUID,
            ID_RelationType = globals.RelationType_belongsTo.GUID,
            Max_Forw = 1,
            Max_Backw = 1,
            Min_Forw = 1,
            Ontology = "Class"
        };

        [BaseOStructure(Order = 6)]
        public clsClassRel ClassRelationTemplateToWordBefore => new clsClassRel
        {
            ID_Class_Left = ClassTemplateTerm.GUID,
            ID_Class_Right = ClassWord.GUID,
            ID_RelationType = RelationTypeBefore.GUID,
            Max_Forw = -1,
            Max_Backw = 1,
            Min_Forw = 0,
            Ontology = "Class"
        };

        [BaseOStructure(Order = 6)]
        public clsClassRel ClassRelationTemplateToWordAfter => new clsClassRel
        {
            ID_Class_Left = ClassTemplateTerm.GUID,
            ID_Class_Right = ClassWord.GUID,
            ID_RelationType = RelationTypeAfter.GUID,
            Max_Forw = -1,
            Max_Backw = 1,
            Min_Forw = 0,
            Ontology = "Class"
        };

        [BaseOStructure(Order = 6)]
        public clsClassAtt ClassAttributeRegularExpRegEx => new clsClassAtt
        {
            ID_AttributeType = AttributeTypeRegEx.GUID,
            ID_Class = ClassRegularExpressionToClass.GUID,
            ID_DataType = AttributeTypeRegEx.GUID_Parent,
            Min = 1,
            Max = 1
        };

        [BaseOStructure(Order = 6)]
        public clsClassAtt ClassAttributeTripleText => new clsClassAtt
        {
            ID_AttributeType = AttributeTypeTripleText.GUID,
            ID_Class = ClassTriplePart.GUID,
            ID_DataType = AttributeTypeTripleText.GUID_Parent,
            Min = 1,
            Max = 1
        };

        [BaseOStructure(Order = 7)]
        public clsOntologyItem ObjectAim => new clsOntologyItem
        {
            GUID = "97bbc951f52b415eabd8ee534277dc4e",
            Name = "Aim",
            GUID_Parent = ClassPartType.GUID,
            Type = globals.Type_Object
        };

        [BaseOStructure(Order = 7)]
        public clsOntologyItem ObjectBase => new clsOntologyItem
        {
            GUID = "c3c4374faeea40319e512a0b133833d0",
            Name = "User Story",
            GUID_Parent = ClassPartType.GUID,
            Type = globals.Type_Object
        };

        [BaseOStructure(Order = 7)]
        public clsOntologyItem ObjectNone => new clsOntologyItem
        {
            GUID = "01c87d5a726e4f489c0700a68618e48f",
            Name = "None",
            GUID_Parent = ClassPartType.GUID,
            Type = globals.Type_Object
        };

        [BaseOStructure(Order = 7)]
        public clsOntologyItem ObjectReason => new clsOntologyItem
        {
            GUID = "5ff7f5ca7e4a4f37a57210b04c7a6a41",
            Name = "Reason",
            GUID_Parent = ClassPartType.GUID,
            Type = globals.Type_Object
        };

        [BaseOStructure(Order = 7)]
        public clsOntologyItem ObjectAction => new clsOntologyItem
        {
            GUID = "e9d72ec128e543249efbb7c6dc3d706f",
            Name = "Action",
            GUID_Parent = ClassPartType.GUID,
            Type = globals.Type_Object
        };

        [BaseOStructure(Order = 7)]
        public clsOntologyItem ObjectTemplate => new clsOntologyItem
        {
            GUID = "71386a06cbfb4be1914a9e432648c767",
            Name = "Template",
            GUID_Parent = ClassPartType.GUID,
            Type = globals.Type_Object
        };

        [BaseOStructure(Order = 7)]
        public clsOntologyItem TicketTemplate => new clsOntologyItem
        {
            GUID = "20b67f72908b42309d04ad08b198af86",
            Name = "TicketTemplate",
            GUID_Parent = ClassPartType.GUID,
            Type = globals.Type_Object
        };

        public async Task<clsOntologyItem> CreateObjects(List<clsOntologyItem> objectsToCreate)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbWriter = new OntologyModDBConnector(globals);
                if (objectsToCreate.Any())
                {
                    result = dbWriter.SaveObjects(objectsToCreate);
                    if (result.GUID == globals.LState_Error.GUID) return result;
                }


                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> CreateObjects(List<Models.ParsedTriplePart> parsedTripleParts)
        {
            var result = globals.LState_Success.Clone();
            var tripleItem = parsedTripleParts.OrderByDescending(tmpl => tmpl.ParseSuccessPercent).FirstOrDefault();

            if (tripleItem == null)
            {
                return result;
            }
            var objectItemsPre = tripleItem.FoundTerms.Select(parseItm => parseItm.ObjectItem).Where(obj => obj != null).ToList();

            var objectItems = objectItemsPre.GroupBy(obj => new { Name = obj.Name, GUID_Parent = obj.GUID_Parent }).Select(obj => new clsOntologyItem
            {
                Name = obj.Key.Name,
                GUID_Parent = obj.Key.GUID_Parent,
                Type = globals.Type_Object
            }).ToList();


            if (objectItems.Any())
            {
                var resultCheckObjects = await CheckObjects(objectItems);
                if (resultCheckObjects.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return resultCheckObjects.ResultState;
                }

                result = await CreateObjects(objectItems.Where(obj => obj.New_Item == null ? false : obj.New_Item.Value).ToList());

                if (result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                (from parseItem in tripleItem.FoundTerms.Where(tmpl => tmpl.ObjectItem != null)
                 join objectItem in objectItems on new { Name = parseItem.ObjectItem.Name, GUID_Parent = parseItem.ObjectItem.GUID_Parent } equals new { Name = objectItem.Name, GUID_Parent = objectItem.GUID_Parent }
                 select new { parseItem, objectItem }).ToList().ForEach(mergeItm =>
                 {
                     mergeItm.parseItem.ObjectItem.GUID = mergeItm.objectItem.GUID;
                 });
            }


            return result;
        }

        public async Task<clsOntologyItem> CreateObjectRelations(List<Models.ParsedTriplePart> parsedTripleParts)
        {
            var result = globals.LState_Success.Clone();
            var tripleItem = parsedTripleParts.OrderByDescending(tmpl => tmpl.ParseSuccessPercent).FirstOrDefault();

            if (tripleItem == null)
            {
                return result;
            }
            var oTripleItem = new clsOntologyItem
            {
                GUID = tripleItem.TriplePart.GUID,
                Name = tripleItem.TriplePart.Name,
                GUID_Parent = ClassTriplePart.GUID,
                Type = globals.Type_Object
            };

            var objectItemsPre = tripleItem.FoundTerms.Select(parseItm => parseItm.ObjectItem).Where(obj => obj != null).ToList();

            var objectItems = objectItemsPre.GroupBy(obj => new { Name = obj.Name, GUID_Parent = obj.GUID_Parent }).Select(obj => new clsOntologyItem
            {
                Name = obj.Key.Name,
                GUID_Parent = obj.Key.GUID_Parent,
                Type = globals.Type_Object
            }).ToList();


            if (objectItems.Any())
            {
                var resultCheckObjects = await CheckObjects(objectItems);
                if (resultCheckObjects.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return resultCheckObjects.ResultState;
                }

                var resultTaskObjectRelations = await CreateObjectRelations(objectItems, oTripleItem);
                result = resultTaskObjectRelations.ResultState;
            }


            return result;
        }

        public async Task<clsOntologyItem> CreateObjectRelations(List<clsObjectRel> relations)
        {

            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var dbWriter = new OntologyModDBConnector(globals);

                var result1 = dbWriter.SaveObjRel(relations);

                return result1;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveTicketRelations(string idTriplePart, List<clsOntologyItem> objects)
        {

            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var dbWriter = new OntologyModDBConnector(globals);

                var newObjects = objects.Where(obj => obj.New_Item != null && obj.New_Item.Value);
                var result1 = await CreateObjects(newObjects.ToList());

                if (result1.GUID == globals.LState_Error.GUID)
                {
                    return result1;
                }

                var resultGetOItem = await GetOItem(idTriplePart, globals.Type_Object);

                if (resultGetOItem.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return resultGetOItem.ResultState;
                }

                long i = 1;
                var rels = objects.Select(obj => relationConfig.Rel_ObjectRelation(resultGetOItem.Result, obj, RelationTypeContains, orderId: i++));

                if (rels.Any())
                {
                    result1 = dbWriter.SaveObjRel(rels.ToList());
                }

                return result1;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> CreateClasses(List<clsOntologyItem> classesToCreate, clsOntologyItem classToCreate)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                List<clsOntologyItem> classes = new List<clsOntologyItem>();
                var result = globals.LState_Success.Clone();
                if (classToCreate == null)
                {
                    classes = (from classItm in classesToCreate
                               join parentClass in classesToCreate on classItm.GUID_Parent equals parentClass.GUID into parentClasses
                               from parentClass in parentClasses.DefaultIfEmpty()
                               where parentClass == null
                               select classItm).ToList();


                }
                else
                {
                    var dbWriter = new OntologyModDBConnector(globals);
                    result = dbWriter.SaveClass(new List<clsOntologyItem> { classToCreate });
                    if (result.GUID == globals.LState_Error.GUID) return result;
                    classes = classesToCreate.Where(cls => cls.GUID_Parent == classToCreate.GUID).ToList();
                }

                foreach (var classItm in classes)
                {
                    result = await CreateClasses(classesToCreate, classItm);
                    if (result.GUID == globals.LState_Error.GUID) return result;
                }

                return result;
            });

            return taskResult;

        }
        private async Task<clsOntologyItem> CheckBaseStructure()
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var result = globals.LState_Success.Clone();

                var propAttVal = this.GetType().GetProperties().Select(prop =>
                {
                    var attrib = prop.GetCustomAttributes(false).FirstOrDefault(att => att is BaseOStructureAttribute);

                    if (attrib == null) return null;

                    var value = prop.GetValue(this);

                    var typeName = "";
                    if (value is clsOntologyItem)
                    {
                        var typeProp = value.GetType().GetProperty("Type");
                        typeName = typeProp.GetValue(value).ToString();
                    }
                    else if (value is clsClassAtt)
                    {
                        typeName = globals.Type_ClassAtt;
                    }
                    else if (value is clsClassRel)
                    {
                        typeName = globals.Type_ClassRel;
                    }
                    else if (value is clsObjectAtt)
                    {
                        typeName = globals.Type_ObjectAtt;
                    }
                    else if (value is clsObjectRel)
                    {
                        typeName = globals.Type_ObjectRel;
                    }


                    return new { Prop = prop, Attribute = (BaseOStructureAttribute)attrib, Value = value, TypeName = typeName };
                }).Where(propAtt => propAtt != null);

                var attTypePropAttVals = propAttVal.Where(propAttV => propAttV.TypeName == globals.Type_AttributeType).ToList();
                var attributeTypes = attTypePropAttVals.OrderBy(propAtt => propAtt.Attribute.Order).Select(simple => (clsOntologyItem)simple.Value).ToList();
                var relationTypes = propAttVal.Where(propAttV => propAttV.TypeName == globals.Type_RelationType).OrderBy(propAtt => propAtt.Attribute.Order).Select(simple => (clsOntologyItem)simple.Value).ToList();
                var classes = propAttVal.Where(propAttV => propAttV.TypeName == globals.Type_Class).OrderBy(propAtt => propAtt.Attribute.Order).Select(simple => (clsOntologyItem)simple.Value).ToList();
                var objects = propAttVal.Where(propAttV => propAttV.TypeName == globals.Type_Object).OrderBy(propAtt => propAtt.Attribute.Order).Select(simple => (clsOntologyItem)simple.Value).ToList();

                var classesAttributes = propAttVal.Where(propAttV => propAttV.TypeName == globals.Type_ClassAtt).OrderBy(propAtt => propAtt.Attribute.Order).Select(triple => (clsClassAtt)triple.Value).ToList();
                var classesRelations = propAttVal.Where(propAttV => propAttV.TypeName == globals.Type_ClassRel).OrderBy(propAtt => propAtt.Attribute.Order).Select(triple => (clsClassRel)triple.Value).ToList();

                var objectAttributes = propAttVal.Where(propAttV => propAttV.TypeName == globals.Type_ObjectAtt).OrderBy(propAtt => propAtt.Attribute.Order).Select(triple => (clsObjectAtt)triple.Value).ToList();
                var objectRelations = propAttVal.Where(propAttV => propAttV.TypeName == globals.Type_ObjectRel).OrderBy(propAtt => propAtt.Attribute.Order).Select(triple => (clsObjectRel)triple.Value).ToList();

                var dbConn1 = new OntologyModDBConnector(globals);

                if (attributeTypes.Any())
                {
                    result = dbConn1.GetDataAttributeType(attributeTypes.Select(smpl => new clsOntologyItem { GUID = smpl.GUID }).ToList());

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                    var simplesToCreate = (from simple in attributeTypes
                                           join simpleExist in dbConn1.AttributeTypes on simple.GUID equals simpleExist.GUID into simplesExist
                                           from simpleExist in simplesExist.DefaultIfEmpty()
                                           where simpleExist == null
                                           select simple).ToList();

                    if (simplesToCreate.Any())
                    {
                        result = dbConn1.SaveAttributeTypes(simplesToCreate);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }
                    }
                }


                if (relationTypes.Any())
                {
                    result = dbConn1.GetDataRelationTypes(relationTypes.Select(smpl => new clsOntologyItem { GUID = smpl.GUID }).ToList());

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }


                    var simplesToCreate = (from simple in relationTypes
                                           join simpleExist in dbConn1.RelationTypes on simple.GUID equals simpleExist.GUID into simplesExist
                                           from simpleExist in simplesExist.DefaultIfEmpty()
                                           where simpleExist == null
                                           select simple).ToList();
                    if (simplesToCreate.Any())
                    {
                        result = dbConn1.SaveRelationTypes(simplesToCreate);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }
                    }

                }

                if (classes.Any())
                {
                    result = dbConn1.GetDataClasses(classes.Select(smpl => new clsOntologyItem { GUID = smpl.GUID }).ToList());

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var simplesToCreate = (from simple in classes
                                           join simpleExist in dbConn1.Classes1 on simple.GUID equals simpleExist.GUID into simplesExist
                                           from simpleExist in simplesExist.DefaultIfEmpty()
                                           where simpleExist == null
                                           select simple).ToList();

                    if (simplesToCreate.Any())
                    {
                        result = await CreateClasses(simplesToCreate, null);
                    }


                }


                if (objects.Any())
                {
                    result = dbConn1.GetDataObjects(objects.Select(smpl => new clsOntologyItem { GUID = smpl.GUID }).ToList());

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var simplesToCreate = (from simple in objects
                                           join simpleExist in dbConn1.Objects1 on simple.GUID equals simpleExist.GUID into simplesExist
                                           from simpleExist in simplesExist.DefaultIfEmpty()
                                           where simpleExist == null
                                           select simple).ToList();

                    if (simplesToCreate.Any())
                    {
                        result = dbConn1.SaveObjects(simplesToCreate);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }
                    }
                }

                if (classesAttributes.Any())
                {
                    result = dbConn1.GetDataClassAtts(classesAttributes.Select(triple => new clsClassAtt { ID_AttributeType = triple.ID_AttributeType, ID_Class = triple.ID_Class }).ToList());

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var classAttsToCreate = (from triple in classesAttributes
                                             join tripleExist in dbConn1.ClassAtts on new { ID_Attribute = triple.ID_AttributeType, ID_Class = triple.ID_Class } equals new { ID_Attribute = tripleExist.ID_AttributeType, ID_Class = tripleExist.ID_Class } into triplesExist
                                             from tripleExist in triplesExist.DefaultIfEmpty()
                                             where tripleExist == null
                                             select triple).ToList();

                    if (classAttsToCreate.Any())
                    {
                        result = dbConn1.SaveClassAtt(classAttsToCreate);

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }
                    }

                }


                if (classesRelations.Any())
                {
                    result = dbConn1.GetDataClassRel(classesRelations.Select(triple => new clsClassRel { ID_Class_Left = triple.ID_Class_Left, ID_Class_Right = triple.ID_Class_Right, ID_RelationType = triple.ID_RelationType }).ToList());

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var classRelsToCreate = (from triple in classesRelations
                                             join tripleExist in dbConn1.ClassRels on new { ID_ClassLeft = triple.ID_Class_Left, ID_ClassRight = triple.ID_Class_Right, ID_RelationType = triple.ID_RelationType }
                                                  equals new { ID_ClassLeft = tripleExist.ID_Class_Left, ID_ClassRight = tripleExist.ID_Class_Right, ID_RelationType = tripleExist.ID_RelationType } into triplesExist
                                             from tripleExist in triplesExist.DefaultIfEmpty()
                                             where tripleExist == null
                                             select triple).ToList();

                    if (classRelsToCreate.Any())
                    {
                        result = dbConn1.SaveClassRel(classRelsToCreate);

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }
                    }

                }

                if (objectAttributes.Any())
                {
                    result = dbConn1.GetDataObjectAtt(objectAttributes.Select(triple => new clsObjectAtt { ID_AttributeType = triple.ID_AttributeType, ID_Object = triple.ID_Object }).ToList());

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var objAttsToCreate = (from triple in objectAttributes
                                           join tripleExist in dbConn1.ObjAtts on new { ID_Attribute = triple.ID_AttributeType, ID_Object = triple.ID_Object } equals new { ID_Attribute = tripleExist.ID_AttributeType, ID_Object = tripleExist.ID_Object } into triplesExist
                                           from tripleExist in triplesExist.DefaultIfEmpty()
                                           where tripleExist == null
                                           select triple).ToList();

                    if (objAttsToCreate.Any())
                    {
                        result = dbConn1.SaveObjAtt(objAttsToCreate);

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }
                    }

                }

                if (objectRelations.Any())
                {
                    result = dbConn1.GetDataObjectRel(objectRelations.Select(triple => new clsObjectRel { ID_Object = triple.ID_Object, ID_Other = triple.ID_Other, ID_RelationType = triple.ID_RelationType }).ToList());

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var objRelsToCreate = (from triple in objectRelations
                                           join tripleExist in dbConn1.ObjectRels on new { ID_Object = triple.ID_Object, ID_Other = triple.ID_Other, ID_RelationType = triple.ID_RelationType }
                                                  equals new { ID_Object = tripleExist.ID_Object, ID_Other = tripleExist.ID_Other, ID_RelationType = tripleExist.ID_RelationType } into triplesExist
                                           from tripleExist in triplesExist.DefaultIfEmpty()
                                           where tripleExist == null
                                           select triple).ToList();

                    if (objRelsToCreate.Any())
                    {
                        result = dbConn1.SaveObjRel(objRelsToCreate);

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }
                    }

                }


                return result;
            });

            return taskResult;
        }

        #endregion

        public async Task<ResultItem<List<clsObjectRel>>> CreateObjectRelations(List<clsOntologyItem> objects, clsOntologyItem triplePart)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                long orderId = 1;
                var relations = objects.Select(cls => relationConfig.Rel_ObjectRelation(triplePart, cls, RelationTypeContains, orderId: orderId++)).ToList();
                var dbWriter = new OntologyModDBConnector(globals);
                result.ResultState = dbWriter.SaveObjRel(relations);
                result.Result = relations;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> CreateClassRelations(List<clsOntologyItem> classes, clsOntologyItem triplePart)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                long orderId = 1;
                var relations = classes.Select(cls => relationConfig.Rel_ObjectRelation(triplePart, cls, RelationTypeContains, orderId: orderId++)).ToList();
                var dbWriter = new OntologyModDBConnector(globals);
                result.ResultState = dbWriter.SaveObjRel(relations);
                result.Result = relations;

                return result;
            });

            return taskResult;
        }

        /// <summary>
        /// Create the Triple Store Items of Triple Part and Project
        /// </summary>
        /// <param name="parentItem">Parent (e.g. Project)</param>
        /// <param name="triplePart">The Triple Part</param>
        /// <param name="partType">Type fo the Triple Part</param>
        /// <param name="orderId">Order of the Triple Part, related to the parent</param>
        /// <returns>Triple Store Items</returns>
        public async Task<ResultItem<CreateTriplePart>> CheckTriplePart(clsOntologyItem parentItem, string triplePart, clsOntologyItem partType, long orderId)
        {
            var taskResult = await Task.Run<ResultItem<CreateTriplePart>>(() =>
            {
                var isGuid = globals.is_GUID(triplePart);
                var result = new ResultItem<CreateTriplePart>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new Services.CreateTriplePart()
                };

                var nameTriplePart = "";
                if (!isGuid)
                {
                    nameTriplePart = triplePart.Length > 255 ? triplePart.Substring(0, 254) : triplePart;
                }
                


                clsOntologyItem triplePartItem = null;
                clsObjectAtt triplePartAttribute = null;

                var dbConn = new OntologyModDBConnector(globals);

                var searchChildren = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = parentItem.GUID,
                            ID_RelationType = RelationTypeContains.GUID,
                            ID_Other = isGuid ? triplePart : null,
                            ID_Parent_Other = !isGuid ? ClassTriplePart.GUID : null
                        }
                    };

                result.ResultState = dbConn.GetDataObjectRel(searchChildren);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                if (!isGuid)
                {
                    var relationS = dbConn.ObjectRels.Where(objRel => objRel.Name_Other == nameTriplePart);
                    if (relationS.Any())
                    {
                        var first = relationS.First();
                        triplePartItem = new clsOntologyItem
                        {
                            GUID = first.ID_Other,
                            Name = first.Name_Other,
                            GUID_Parent = first.ID_Parent_Other,
                            Type = globals.Type_Object,
                            Additional1 = triplePart
                        };

                        triplePartAttribute = relationConfig.Rel_ObjectAttribute(triplePartItem, AttributeTypeTripleText, triplePart);
                        result.Result.TriplePart = triplePartItem;
                        result.Result.TriplePartText = triplePartAttribute;
                    }
                }
                else
                {
                    var first = dbConn.ObjectRels.First();
                    triplePartItem = new clsOntologyItem
                    {
                        GUID = first.ID_Other,
                        Name = first.Name_Other,
                        GUID_Parent = first.ID_Parent_Other,
                        Type = globals.Type_Object,
                        Additional1 = triplePart
                    };

                    triplePartAttribute = relationConfig.Rel_ObjectAttribute(triplePartItem, AttributeTypeTripleText, triplePart);
                    result.Result.TriplePart = triplePartItem;
                    result.Result.TriplePartText = triplePartAttribute;
                }
                

                if (triplePartItem == null)
                {
                    triplePartItem = new clsOntologyItem
                    {
                        GUID = globals.NewGUID,
                        Name = nameTriplePart,
                        GUID_Parent = ClassTriplePart.GUID,
                        Type = globals.Type_Object,
                        New_Item = true,
                        Additional1 = triplePart
                    };
                }
                triplePartAttribute = relationConfig.Rel_ObjectAttribute(triplePartItem, AttributeTypeTripleText, triplePart);

                result.Result.TriplePart = triplePartItem;
                result.Result.TriplePartText = triplePartAttribute;


                return result;
            });

            return taskResult;
        }


        public async Task<ResultItem<CreateTriplePart>> CreateTriplePart(clsOntologyItem parentItem, string triplePart, bool useExisting, clsOntologyItem partType, long orderId)
        {
            clsTransaction transaction = new clsTransaction(globals);
            var taskResult = await Task.Run<ResultItem<CreateTriplePart>>(() =>
            {
                var nameTriplePart = triplePart.Length > 255 ? triplePart.Substring(0, 254) : triplePart;
                var result = new ResultItem<CreateTriplePart>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                clsOntologyItem triplePartItem = null;

                var dbConn = new OntologyModDBConnector(globals);

                if (useExisting)
                {
                    var searchChildren = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = parentItem.GUID,
                            ID_RelationType = RelationTypeContains.GUID,
                            ID_Parent_Other = ClassTriplePart.GUID
                        }
                    };

                    result.ResultState = dbConn.GetDataObjectRel(searchChildren);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var relationS = dbConn.ObjectRels.Where(objRel => objRel.Name_Other == nameTriplePart);
                    if (relationS.Any())
                    {
                        var first = relationS.First();
                        triplePartItem = new clsOntologyItem
                        {
                            GUID = first.ID_Other,
                            Name = first.Name_Other,
                            GUID_Parent = first.ID_Parent_Other,
                            Type = globals.Type_Object
                        };
                        result.Result.TriplePart = triplePartItem;
                    }
                }

                if (triplePartItem == null)
                {
                    triplePartItem = new clsOntologyItem
                    {
                        GUID = globals.NewGUID,
                        Name = nameTriplePart,
                        GUID_Parent = ClassTriplePart.GUID,
                        Type = globals.Type_Object
                    };

                    transaction.ClearItems();
                    result.ResultState = transaction.do_Transaction(triplePartItem);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }


                    var rel = relationConfig.Rel_ObjectRelation(parentItem, triplePartItem, RelationTypeContains, orderId: orderId);
                    result.ResultState = transaction.do_Transaction(rel);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        transaction.rollback();
                        return result;
                    }

                    result.Result.TriplePart = triplePartItem;

                    var attRel = relationConfig.Rel_ObjectAttribute(triplePartItem, AttributeTypeTripleText, triplePart);
                    result.ResultState = transaction.do_Transaction(attRel);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        transaction.rollback();
                        return result;
                    }
                }

                var relTripleType = relationConfig.Rel_ObjectRelation(triplePartItem, partType, RelationTypeIsOfType);
                result.ResultState = transaction.do_Transaction(relTripleType, true);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    transaction.rollback();
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<TicketTemplatesWithClassesAndRegex>> GetTicketTemplates(clsOntologyItem project)
        {
            var taskResult = await Task.Run<ResultItem<TicketTemplatesWithClassesAndRegex>>(() =>
            {
                var result = new ResultItem<TicketTemplatesWithClassesAndRegex>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new TicketTemplatesWithClassesAndRegex
                    {
                        Classes = new List<clsObjectRel>(),
                        RegexAttributes = new List<clsObjectAtt>(),
                        TicketTemplates = new List<clsObjectRel>(),
                        TicketTemplatesToRegex = new List<clsObjectRel>()
                    }
                };

                var searchTicketTemplatesOfProject = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = project.GUID,
                        ID_RelationType = RelationTypeContains.GUID,
                        ID_Parent_Other = ClassTriplePart.GUID
                    }
                };

                var dbReaderTicketTicketTemplateOfProject = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderTicketTicketTemplateOfProject.GetDataObjectRel(searchTicketTemplatesOfProject, doIds: true);

                var searchTicketTemplates = dbReaderTicketTicketTemplateOfProject.ObjectRelsId.Select(objRel => new clsObjectRel
                {
                    ID_Object = objRel.ID_Other,
                    ID_RelationType = RelationTypeIsOfType.GUID,
                    ID_Other = TicketTemplate.GUID
                }).ToList();

                var dbReaderTicketTicketTemplateType = new OntologyModDBConnector(globals);

                var start = 0;
                var searchCount = 1000;
                while (start < searchTicketTemplates.Count)
                {
                    if (searchTicketTemplates.Count < start + searchCount)
                    {
                        searchCount = searchTicketTemplates.Count - start;
                    }


                    var objectsToCheckPart = searchTicketTemplates.GetRange(start, searchCount);

                    result.ResultState = dbReaderTicketTicketTemplateType.GetDataObjectRel(objectsToCheckPart);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }


                    result.Result.TicketTemplates.AddRange(dbReaderTicketTicketTemplateType.ObjectRels);

                    start += searchCount;
                }

                var searchRegex = result.Result.TicketTemplates.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = RelationTypeIsDefinedBy.GUID,
                    ID_Parent_Other = ClassRegularExpressionToClass.GUID
                }).ToList();



                var dbReaderRegEx = new OntologyModDBConnector(globals);

                start = 0;
                searchCount = 1000;
                while (start < searchRegex.Count)
                {
                    if (searchRegex.Count < start + searchCount)
                    {
                        searchCount = searchRegex.Count - start;
                    }


                    var objectsToCheckPart = searchRegex.GetRange(start, searchCount);

                    result.ResultState = dbReaderRegEx.GetDataObjectRel(objectsToCheckPart);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }


                    result.Result.TicketTemplatesToRegex.AddRange(dbReaderRegEx.ObjectRels);

                    start += searchCount;
                }

                var searchRegexAtt = result.Result.TicketTemplatesToRegex.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.ID_Other,
                    ID_AttributeType = AttributeTypeRegEx.GUID
                }).ToList();

                var dbReaderRegExAtt = new OntologyModDBConnector(globals);

                start = 0;
                searchCount = 1000;
                while (start < searchRegexAtt.Count)
                {
                    if (searchRegexAtt.Count < start + searchCount)
                    {
                        searchCount = searchRegexAtt.Count - start;
                    }


                    var objectsToCheckPart = searchRegexAtt.GetRange(start, searchCount);

                    result.ResultState = dbReaderRegExAtt.GetDataObjectAtt(objectsToCheckPart);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.RegexAttributes.AddRange(dbReaderRegExAtt.ObjAtts);

                    start += searchCount;
                }

                var searchClasses = result.Result.TicketTemplates.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = RelationTypeBelongingClass.GUID
                }).ToList();

                searchClasses.AddRange(result.Result.TicketTemplatesToRegex.Select(tickToReg => new clsObjectRel
                {
                    ID_Object = tickToReg.ID_Other,
                    ID_RelationType = RelationTypeBelongingClass.GUID
                }));

                var dbReaderClasses = new OntologyModDBConnector(globals);

                start = 0;
                searchCount = 1000;
                while (start < searchClasses.Count)
                {
                    if (searchClasses.Count < start + searchCount)
                    {
                        searchCount = searchClasses.Count - start;
                    }


                    var objectsToCheckPart = searchClasses.GetRange(start, searchCount);

                    result.ResultState = dbReaderClasses.GetDataObjectRel(objectsToCheckPart);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.Classes.AddRange(dbReaderClasses.ObjectRels);

                    start += searchCount;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> GetObjectsOfTicketTemplates(TicketTemplates ticketTemplates)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var searchObjects = ticketTemplates.TicketTemplatesClasses.Select(sel => new clsOntologyItem { GUID_Parent = sel.ToClassItem.ID_Other }).ToList();
                searchObjects.AddRange(ticketTemplates.TicketTemplatesRegex.Select(sel => new clsOntologyItem { GUID_Parent = sel.ToClassItem.ID_Other }));

                var dbReaderObjects = new OntologyModDBConnector(globals);

                result = dbReaderObjects.GetDataObjects(searchObjects);

                ticketTemplates.TicketTemplatesClasses.ForEach(ticketTemplate =>
                 {
                     ticketTemplate.ObjectItems = dbReaderObjects.Objects1.Where(obj => obj.GUID_Parent == ticketTemplate.ToClassItem.ID_Other).ToList();
                     
                 });

                ticketTemplates.TicketTemplatesRegex.ForEach(ticketTemplate =>
                {
                    ticketTemplate.ObjectItems = dbReaderObjects.Objects1.Where(obj => obj.GUID_Parent == ticketTemplate.ToClassItem.ID_Other).ToList();

                });


                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ParseTripleOfProject>> GetTemplates(clsOntologyItem project)
        {
            var taskResult = await Task.Run<ResultItem<ParseTripleOfProject>>(() =>
            {
                var result = new ResultItem<ParseTripleOfProject>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ParseTripleOfProject
                    {
                        ProjectItem = project,
                        ParseTemplates = new List<ParseTripleTemplate>()
                    }
                };

                var dbReaderTripleParts = new OntologyModDBConnector(globals);
                var searchTripleParts = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = project.GUID,
                        ID_RelationType = RelationTypeContains.GUID,
                        ID_Parent_Other = ClassTriplePart.GUID
                    }
                };

                result.ResultState = dbReaderTripleParts.GetDataObjectRel(searchTripleParts);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var templatesToTypes = new List<clsObjectRel>();
                result.Result.ProjectItem = project;

                var searchTriplePartType = dbReaderTripleParts.ObjectRels.Select(objRel => new clsObjectRel
                {
                    ID_Other = ObjectTemplate.GUID,
                    ID_RelationType = RelationTypeIsOfType.GUID,
                    ID_Object = objRel.ID_Other
                }).ToList();

                if (searchTriplePartType.Any())
                {
                    var dbReaderTemplates = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderTemplates.GetDataObjectRel(searchTriplePartType);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    templatesToTypes = dbReaderTemplates.ObjectRels;
                }

                var projectToTripleParts = (from triplePart in dbReaderTripleParts.ObjectRels
                                            join triplePartType in templatesToTypes on triplePart.ID_Other equals triplePartType.ID_Object
                                            select triplePart).ToList();

                var searchText = projectToTripleParts.Select(objRel => new clsObjectAtt
                {
                    ID_Object = objRel.ID_Other,
                    ID_AttributeType = AttributeTypeTripleText.GUID
                }).ToList();

                var templatesText = new List<clsObjectAtt>();
                if (searchText.Any())
                {
                    var dbReaderText = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderText.GetDataObjectAtt(searchText);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    templatesText = dbReaderText.ObjAtts;
                }

                var searchTemplates = projectToTripleParts.Select(triple => new clsObjectRel
                {
                    ID_Other = triple.ID_Other,
                    ID_RelationType = globals.RelationType_belongsTo.GUID,
                    ID_Parent_Object = ClassTemplateTerm.GUID
                }).ToList();

                var templatesToTripleParts = new List<clsObjectRel>();
                if (searchTemplates.Any())
                {
                    var dbReaderTemplates = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderTemplates.GetDataObjectRel(searchTemplates);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                    templatesToTripleParts = dbReaderTemplates.ObjectRels;
                }

                var searchWordsBefore = templatesToTripleParts.Select(templ => new clsObjectRel
                {
                    ID_Object = templ.ID_Object,
                    ID_RelationType = RelationTypeBefore.GUID,
                    ID_Parent_Other = ClassWord.GUID
                }).ToList();

                var templatesToWordsBefore = new List<clsObjectRel>();
                if (searchWordsBefore.Any())
                {
                    var dbReaderWords = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderWords.GetDataObjectRel(searchWordsBefore);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                    templatesToWordsBefore = dbReaderWords.ObjectRels;
                }

                var searchWordsAfter = templatesToTripleParts.Select(templ => new clsObjectRel
                {
                    ID_Object = templ.ID_Object,
                    ID_RelationType = RelationTypeAfter.GUID,
                    ID_Parent_Other = ClassWord.GUID
                }).ToList();

                var templatesToWordsAfter = new List<clsObjectRel>();
                if (searchWordsAfter.Any())
                {
                    var dbReaderWords = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderWords.GetDataObjectRel(searchWordsAfter);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                    templatesToWordsAfter = dbReaderWords.ObjectRels;
                }

                var searchClasses = templatesToTripleParts.Select(templ => new clsObjectRel
                {
                    ID_Object = templ.ID_Object,
                    ID_RelationType = globals.RelationType_belongingClass.GUID
                }).ToList();

                var templatesToClasses = new List<clsObjectRel>();
                if (searchClasses.Any())
                {
                    var dbReaderClasses = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderClasses.GetDataObjectRel(searchClasses);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                    templatesToClasses = dbReaderClasses.ObjectRels;
                }

                var searchTemplatesToAttributeTerms = templatesToTripleParts.Select(templ => new clsObjectRel
                {
                    ID_Other = templ.ID_Object,
                    ID_RelationType = RelationTypeIs.GUID,
                    ID_Parent_Object = ClassTemplateAttributeTerm.GUID
                }).ToList();

                var templatesToAtributeTerms = new List<clsObjectRel>();
                if (searchTemplatesToAttributeTerms.Any())
                {
                    var dbReaderAttributeTerms = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderAttributeTerms.GetDataObjectRel(searchTemplatesToAttributeTerms);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                    templatesToAtributeTerms = dbReaderAttributeTerms.ObjectRels;
                }

                var attributeTermsToTypes = new List<clsObjectRel>();
                var searchAttTermTypes = templatesToAtributeTerms.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = RelationTypeIsOfType.GUID,
                    ID_Parent_Other = ClassDataTypes.GUID
                }).ToList();

                if (searchAttTermTypes.Any())
                {
                    var dbReaderAttributeTermTypes = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderAttributeTermTypes.GetDataObjectRel(searchAttTermTypes);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                    attributeTermsToTypes = dbReaderAttributeTermTypes.ObjectRels;
                }

                var searchSynonyms = templatesToTripleParts.Select(templ => new clsObjectRel
                {
                    ID_Object = templ.ID_Object,
                    ID_RelationType = RelationTypeIsSynonymFor.GUID,
                    ID_Parent_Other = ClassTemplateTerm.GUID
                }).ToList();

                var templateSynonyms = new List<clsObjectRel>();
                if (searchSynonyms.Any())
                {
                    var dbReaderSynonyms = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderSynonyms.GetDataObjectRel(searchSynonyms);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                    templateSynonyms = dbReaderSynonyms.ObjectRels;
                }

                result.Result.ParseTemplates = projectToTripleParts.Select(projToTriple =>
                {
                    var resultTripleTemplate = new ParseTripleTemplate
                    {
                        ProjectToTriplePart = projToTriple
                    };

                    resultTripleTemplate.TemplatesToTripleParts = templatesToTripleParts.Where(templ => templ.ID_Other == projToTriple.ID_Other).ToList();
                    resultTripleTemplate.TemplatesSynonyms = (from templ in resultTripleTemplate.TemplatesToTripleParts
                                                              join synonym in templateSynonyms on templ.ID_Object equals synonym.ID_Object
                                                              select synonym).ToList();

                    resultTripleTemplate.TemplatesText = templatesText.Where(tmplText => tmplText.ID_Object == projToTriple.ID_Other).ToList();


                    resultTripleTemplate.TemplatesToClasses = (from templ in resultTripleTemplate.TemplatesToTripleParts
                                                               join classRel in templatesToClasses on templ.ID_Object equals classRel.ID_Object
                                                               select classRel).ToList();

                    resultTripleTemplate.TemplatesToTypes = templatesToTypes.Where(typeItm => typeItm.ID_Object == projToTriple.ID_Other).ToList();

                    resultTripleTemplate.TemplatesToWordsBefore = (from templ in resultTripleTemplate.TemplatesToTripleParts
                                                                   join wordItem in templatesToWordsBefore on templ.ID_Object equals wordItem.ID_Object
                                                                   select wordItem).ToList();

                    resultTripleTemplate.TemplatesToWordsAfter = (from templ in resultTripleTemplate.TemplatesToTripleParts
                                                                  join wordItem in templatesToWordsAfter on templ.ID_Object equals wordItem.ID_Object
                                                                  select wordItem).ToList();

                    resultTripleTemplate.AttributeTermToTemplates = (from templ in resultTripleTemplate.TemplatesToTripleParts
                                                                     join attributeTerm in templatesToAtributeTerms on templ.ID_Object equals attributeTerm.ID_Other
                                                                     select attributeTerm).ToList();

                    resultTripleTemplate.AttributeTermToDataTypes = (from attribTerm in resultTripleTemplate.AttributeTermToTemplates
                                                                     join attributeTermType in attributeTermsToTypes on attribTerm.ID_Object equals attributeTermType.ID_Object
                                                                     select attributeTermType).ToList();
                    return resultTripleTemplate;
                }).ToList();

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ClassesAndObjectsWithRootItem>> GetClassesAndObjectsOfProject(string idProject)
        {
            var taskResult = await Task.Run<ResultItem<ClassesAndObjectsWithRootItem>>(async () =>
            {

                var result = new ResultItem<ClassesAndObjectsWithRootItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var oItemProject = await GetOItem(idProject, globals.Type_Object);

                if (oItemProject.Result.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = oItemProject.Result;
                    return result;
                }

                result.Result.RootItem = oItemProject.Result;

                var dbReaderTripleParts = new OntologyModDBConnector(globals);
                var searchUserStory = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = idProject,
                        ID_Parent_Other = ClassTriplePart.GUID,
                        ID_RelationType = RelationTypeContains.GUID
                    }
                };

                result.ResultState = dbReaderTripleParts.GetDataObjectRel(searchUserStory);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var dbReader = new OntologyModDBConnector(globals);

                var searchClassesAndObjects = dbReader.ObjectRels.Select(objRel => new clsObjectRel
                {
                    ID_Object = objRel.ID_Other,
                    ID_RelationType = RelationTypeContains.GUID
                }).ToList();

                if (searchClassesAndObjects.Any())
                {
                    var dbReaderClassesAndObjects = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderClassesAndObjects.GetDataObjectRel(searchClassesAndObjects);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.ClassesAndObjects = (from userStory in dbReader.ObjectRels
                                                       join classObj in dbReaderClassesAndObjects.ObjectRels.Where(objRel => objRel.ID_Parent_Other != ClassTriplePart.GUID) on userStory.ID_Other equals classObj.ID_Object
                                                       select new clsOntologyItem
                                                       {
                                                           GUID = classObj.ID_Other,
                                                           Name = classObj.Name_Other,
                                                           GUID_Parent = classObj.ID_Parent_Other,
                                                           Type = classObj.Ontology,
                                                           Val_Long = userStory.OrderID.Value
                                                       }).GroupBy(itm => new { GUID = itm.GUID, Name = itm.Name, GUID_Parent = itm.GUID_Parent, Type = itm.Type, Val_Long = itm.Val_Long }).Select(itm => new clsOntologyItem
                                                       {
                                                           GUID = itm.Key.GUID,
                                                           Name = itm.Key.Name,
                                                           GUID_Parent = itm.Key.GUID_Parent,
                                                           Type = itm.Key.Type,
                                                           Val_Long = itm.Max(row => row.Val_Long)
                                                       }).ToList().OrderByDescending(objCls => objCls.Val_Long).ToList();

                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> ChangePrio(string idObject, string idAttribute, long newPrio)
        {
            var oItemResult = await GetOItem(idObject, globals.Type_Object);

            if (oItemResult.ResultState.GUID == globals.LState_Error.GUID)
            {
                return oItemResult.ResultState;
            }

            var objectItem = oItemResult.Result;

            clsTransaction transaction = new clsTransaction(globals);

            var taskResult = await Task.Run<clsOntologyItem>(() =>
                {
                    var dbWriter = new OntologyModDBConnector(globals);
                    var result = globals.LState_Success.Clone();
                    clsObjectAtt change;
                    if (!string.IsNullOrEmpty(idAttribute))
                    {
                        change = new clsObjectAtt
                        {
                            ID_AttributeType = AttributeTypePrio.GUID,
                            ID_Object = objectItem.GUID,
                            ID_Class = objectItem.GUID_Parent,
                            ID_DataType = AttributeTypePrio.GUID_Parent,
                            Val_Lng = newPrio,
                            Val_Name = newPrio.ToString()
                        };

                        change = relationConfig.Rel_ObjectAttribute(objectItem, AttributeTypePrio, newPrio);
                        result = transaction.do_Transaction(change, boolRemoveAll: true);
                        change.ID_Attribute = transaction.OItem_Last.OItemObjectAtt.ID_Attribute;
                    }
                    else
                    {
                        change = relationConfig.Rel_ObjectAttribute(objectItem, AttributeTypePrio, newPrio);
                        result = transaction.do_Transaction(change, boolRemoveAll: true);
                        change.ID_Attribute = transaction.OItem_Last.OItemObjectAtt.ID_Attribute;
                    }


                    return result;
                });

            return taskResult;
        }

        public async Task<ResultItem<ProjectsTriplePartsAndObjectsWithPrios>> GetObjectListWithPrio(string idProject, string idClass)
        {
            var taskResult = await Task.Run<ResultItem<ProjectsTriplePartsAndObjectsWithPrios>>(() =>
            {
                var result = new ResultItem<ProjectsTriplePartsAndObjectsWithPrios>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ProjectsTriplePartsAndObjectsWithPrios
                    {
                        ProjectToTripleParts = new List<clsObjectRel>(),
                        TriplePartsToObjects = new List<clsObjectRel>()
                    }
                };

                var searchProjectToTripleParts = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = idProject,
                        ID_RelationType = RelationTypeContains.GUID,
                        ID_Parent_Other = ClassTriplePart.GUID
                    }
                };

                var dbReaderProjectToTripleParts = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderProjectToTripleParts.GetDataObjectRel(searchProjectToTripleParts);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchNutzen = dbReaderProjectToTripleParts.ObjectRels.Select(objRel => new clsObjectRel
                {
                    ID_Object = objRel.ID_Other,
                    ID_Parent_Other = idClass,
                    ID_RelationType = RelationTypeContains.GUID
                }).ToList();

                var dbReaderTriplePartsToObjects = new OntologyModDBConnector(globals);

                if (searchNutzen.Any())
                {
                    result.ResultState = dbReaderTriplePartsToObjects.GetDataObjectRel(searchNutzen);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                var searchObjAtts = dbReaderTriplePartsToObjects.ObjectRels.Select(objRel => new clsObjectAtt
                {
                    ID_Object = objRel.ID_Other,
                    ID_AttributeType = AttributeTypePrio.GUID
                }).ToList();

                var dbReaderObjectAtts = new OntologyModDBConnector(globals);

                if (searchObjAtts.Any())
                {
                    result.ResultState = dbReaderObjectAtts.GetDataObjectAtt(searchObjAtts);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.ProjectToTripleParts = dbReaderProjectToTripleParts.ObjectRels;
                result.Result.TriplePartsToObjects = dbReaderTriplePartsToObjects.ObjectRels;
                result.Result.ObjectPrios = dbReaderObjectAtts.ObjAtts;

                return result;
            });

            return taskResult;

        }

        public async Task<ResultItem<ProjectAndUserStory>> GetProjectAndUserStoryByUserStoryId(string idUserStory)
        {
            var taskResult = await Task.Run<ResultItem<ProjectAndUserStory>>(async () =>
            {
                var result = new ResultItem<ProjectAndUserStory>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ProjectAndUserStory()
                };

                var resultGetOItem = await GetOItem(idUserStory, globals.Type_Object);

                if (resultGetOItem.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultGetOItem.ResultState;
                    return result;
                }

                result.Result.TriplePart = resultGetOItem.Result;

                var searchTriplePartText = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object = idUserStory,
                        ID_AttributeType = AttributeTypeTripleText.GUID
                    }
                };

                var dbReaderAttr = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderAttr.GetDataObjectAtt(searchTriplePartText);

                if (result.ResultState.GUID == globals.LState_Error.GUID || !dbReaderAttr.ObjAtts.Any())
                {
                    return result;
                }

                result.Result.TriplePartText = dbReaderAttr.ObjAtts.First();

                var searchProject = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = idUserStory,
                        ID_RelationType = RelationTypeContains.GUID,
                        ID_Parent_Object = ClassProject.GUID
                    }
                };

                var dbReader = new OntologyModDBConnector(globals);
                result.ResultState = dbReader.GetDataObjectRel(searchProject);

                if (result.ResultState.GUID == globals.LState_Error.GUID || !dbReader.ObjectRels.Any())
                {
                    return result;
                }

                result.Result.Project = dbReader.ObjectRels.Select(objRel => new clsOntologyItem
                {
                    GUID = objRel.ID_Object,
                    Name = objRel.Name_Object,
                    GUID_Parent = objRel.ID_Parent_Object,
                    Type = globals.Type_Object
                }).First();

                return result;
            });


            return taskResult;
        }
        public async Task<ResultItem<List<clsOntologyItem>>> GetProjectClasses(string classStartsWith)
        {
            classStartsWith = classStartsWith.ToLower();
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var searchProjectClasses = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = ClassProject.GUID,
                        Name = classStartsWith
                    }
                };

                var dbReader = new OntologyModDBConnector(globals);

                result.ResultState = dbReader.GetDataClasses(searchProjectClasses);
                result.Result = dbReader.Classes1.Where(cls => cls.Name.ToLower().StartsWith(classStartsWith)).ToList();


                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetWords(string wordStartsWith)
        {
            wordStartsWith = wordStartsWith.ToLower();
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var searchWords = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = ClassWord.GUID,
                        Name = wordStartsWith
                    }
                };

                var dbReader = new OntologyModDBConnector(globals);

                result.ResultState = dbReader.GetDataObjects(searchWords);
                result.Result = dbReader.Objects1.Where(obj => obj.Name.ToLower().StartsWith(wordStartsWith)).ToList();


                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ClassesAndObjectsWithRootItem>> GetClassesAndObjectsOfTriplePart(string idTriplePart, string idProject)
        {
            var taskResult = await Task.Run<ResultItem<ClassesAndObjectsWithRootItem>>(async () =>
            {

                var result = new ResultItem<ClassesAndObjectsWithRootItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var oItem = await GetOItem(idTriplePart, globals.Type_Object);

                if (oItem.Result.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = oItem.Result;
                    return result;
                }

                result.Result.RootItem = oItem.Result;

                var dbReaderPrio = new OntologyModDBConnector(globals);
                var searchUserStory = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = idProject,
                        ID_Other = idTriplePart,
                        ID_RelationType = RelationTypeContains.GUID
                    }
                };

                result.ResultState = dbReaderPrio.GetDataObjectRel(searchUserStory);
                if (result.ResultState.GUID == globals.LState_Error.GUID || !dbReaderPrio.ObjectRels.Any())
                {
                    return result;
                }

                var prio = dbReaderPrio.ObjectRels.First().OrderID.Value;
                result.Result.RootItem.Val_Long = prio;

                var dbReader = new OntologyModDBConnector(globals);
                var searchTipleParts = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = idTriplePart,
                        ID_RelationType = RelationTypeContains.GUID,
                        ID_Parent_Other = ClassTriplePart.GUID
                    }
                };

                result.ResultState = dbReader.GetDataObjectRel(searchTipleParts);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchClassesAndObjects = dbReader.ObjectRels.Select(objRel => new clsObjectRel
                {
                    ID_Object = objRel.ID_Other,
                    ID_RelationType = RelationTypeContains.GUID
                }).ToList();

                if (searchClassesAndObjects.Any())
                {
                    var dbReaderClassesAndObjects = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderClassesAndObjects.GetDataObjectRel(searchClassesAndObjects);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.ClassesAndObjects = (from userStory in dbReader.ObjectRels
                                                       join classObj in dbReaderClassesAndObjects.ObjectRels.Where(objRel => objRel.ID_Parent_Other != ClassTriplePart.GUID) on userStory.ID_Other equals classObj.ID_Object
                                                       select new clsOntologyItem
                                                       {
                                                           GUID = classObj.ID_Other,
                                                           Name = classObj.Name_Other,
                                                           GUID_Parent = classObj.ID_Parent_Other,
                                                           Type = classObj.Ontology,
                                                           Val_Long = prio * userStory.OrderID.Value
                                                       }).GroupBy(itm => new { GUID = itm.GUID, Name = itm.Name, GUID_Parent = itm.GUID_Parent, Type = itm.Type, Val_Long = itm.Val_Long }).Select(itm => new clsOntologyItem
                                                       {
                                                           GUID = itm.Key.GUID,
                                                           Name = itm.Key.Name,
                                                           GUID_Parent = itm.Key.GUID_Parent,
                                                           Type = itm.Key.Type,
                                                           Val_Long = itm.Max(row => row.Val_Long)
                                                       }).ToList().OrderByDescending(objCls => objCls.Val_Long).ToList();

                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetProjectList()
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchProjects = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = ClassProject.GUID
                    }
                };

                var dbReader = new OntologyModDBConnector(globals);
                result.ResultState = dbReader.GetDataObjects(searchProjects);

                if (result.ResultState.GUID == globals.LState_Success.GUID)
                {
                    result.Result = dbReader.Objects1.OrderBy(obj => obj.Name).ToList();
                }

                return result;
            });

            return taskResult;

        }

        public async Task<ResultItem<clsOntologyItem>> GetOItem(string idItem, string type)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var dbReader = new OntologyModDBConnector(globals);

                var resultOItem = dbReader.GetOItem(idItem, type);
                if (resultOItem.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    return result;
                }
                result.Result = resultOItem.Clone();


                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<clsOntologyItem>> CheckProject(string project)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    Result = globals.LState_Success.Clone()
                };

                var searchProject = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        Name = project,
                        GUID_Parent = ClassProject.GUID
                    }
                };

                var dbConn = new OntologyModDBConnector(globals);

                result.Result = dbConn.GetDataObjects(searchProject);

                if (result.Result.GUID == globals.LState_Error.GUID) return result;

                var projectItem = dbConn.Objects1.FirstOrDefault(proj => proj.Name == project);

                if (projectItem == null)
                {
                    projectItem = new clsOntologyItem
                    {
                        GUID = globals.NewGUID,
                        Name = project,
                        GUID_Parent = ClassProject.GUID,
                        Type = globals.Type_Object,
                        New_Item = true
                    };
                }

                result.Result = projectItem;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<clsOntologyItem>> CreateProject(string project)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    Result = globals.LState_Success.Clone()
                };

                var searchProject = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        Name = project,
                        GUID_Parent = ClassProject.GUID
                    }
                };

                var dbConn = new OntologyModDBConnector(globals);

                result.Result = dbConn.GetDataObjects(searchProject);

                if (result.Result.GUID == globals.LState_Error.GUID) return result;

                var projectItem = dbConn.Objects1.FirstOrDefault(proj => proj.Name == project);

                if (projectItem == null)
                {
                    projectItem = new clsOntologyItem
                    {
                        GUID = globals.NewGUID,
                        Name = project,
                        GUID_Parent = ClassProject.GUID,
                        Type = globals.Type_Object
                    };

                    var dbWriter = new OntologyModDBConnector(globals);

                    result.Result = dbWriter.SaveObjects(new List<clsOntologyItem> { projectItem });
                    if (result.Result.GUID == globals.LState_Error.GUID) return result;
                }

                result.Result = projectItem;

                return result;
            });

            return taskResult;
        }


        public async Task<ResultItem<TriplePartItems>> GetTripleParts(string idProject)
        {
            var taskResult = await Task.Run<ResultItem<TriplePartItems>>(() =>
            {
                var result = new ResultItem<TriplePartItems>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var dbReader = new OntologyModDBConnector(globals);
                var searchTipleParts = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = idProject,
                        ID_RelationType = RelationTypeContains.GUID,
                        ID_Parent_Other = ClassTriplePart.GUID
                    }
                };

                result.ResultState = dbReader.GetDataObjectRel(searchTipleParts);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                result.Result.ProjectToTripleParts = dbReader.ObjectRels;
                result.Result.TriplePartsToTripleParts = new List<clsObjectRel>();

                var dbReaderSub = new OntologyModDBConnector(globals);
                var searchSub = dbReader.ObjectRels.Select(objRel => new clsObjectRel
                {
                    ID_Object = objRel.ID_Other,
                    ID_RelationType = RelationTypeContains.GUID,
                    ID_Parent_Other = ClassTriplePart.GUID
                }).ToList();

                while (searchSub.Any())
                {

                    result.ResultState = dbReaderSub.GetDataObjectRel(searchSub);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        break;
                    }

                    result.Result.TriplePartsToTripleParts.AddRange(dbReaderSub.ObjectRels);

                    searchSub = dbReaderSub.ObjectRels.Select(objRel => new clsObjectRel
                    {
                        ID_Object = objRel.ID_Other,
                        ID_RelationType = RelationTypeContains.GUID,
                        ID_Parent_Other = ClassTriplePart.GUID
                    }).ToList();

                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<TriplePartsOfProjects>> GetUserStoriesOfProjects()
        {
            var taskResult = await Task.Run<ResultItem<TriplePartsOfProjects>>(() =>
            {
                var result = new ResultItem<TriplePartsOfProjects>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new TriplePartsOfProjects
                    {
                        ObjectTemplate = ObjectTemplate,
                        ClassTriplePart = ClassTriplePart
                    }

                };

                var dbReaderProjects = new OntologyModDBConnector(globals);

                var searchProjects = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = ClassProject.GUID
                    }
                };

                var resultReadProjects = dbReaderProjects.GetDataObjects(searchProjects);

                if (resultReadProjects.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultReadProjects;
                    return result;
                }



                var dbReaderProjectsToTripleParts = new OntologyModDBConnector(globals);

                var search = dbReaderProjects.Objects1.Select(prj => new clsObjectRel
                {
                    ID_Object = prj.GUID,
                    ID_Parent_Other = ClassTriplePart.GUID,
                    ID_RelationType = RelationTypeContains.GUID
                }).ToList();

                var resultRead = dbReaderProjectsToTripleParts.GetDataObjectRel(search);

                if (resultRead.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultRead;
                    return result;
                }

                search = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Object = ClassTriplePart.GUID,
                        ID_Parent_Other = ClassPartType.GUID,
                        ID_RelationType = RelationTypeIsOfType.GUID
                    }
                };

                var dbReaderTriplePartsToType = new OntologyModDBConnector(globals);

                resultRead = dbReaderTriplePartsToType.GetDataObjectRel(search);
                if (resultRead.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultRead;
                    return result;
                }


                search = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Object = ClassTriplePart.GUID,
                        ID_RelationType = RelationTypeContains.GUID
                    }
                };

                var dbReaderObjectsOfTripleParts = new OntologyModDBConnector(globals);

                resultRead = dbReaderObjectsOfTripleParts.GetDataObjectRel(search);
                if (resultRead.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultRead;
                    return result;
                }


                result.Result.Projects = dbReaderProjects.Objects1;
                result.Result.ProjectsToTripleParts = dbReaderProjectsToTripleParts.ObjectRels;
                result.Result.TriplePartsToPartType = dbReaderTriplePartsToType.ObjectRels;
                result.Result.ObjectsOfTripleParts = dbReaderObjectsOfTripleParts.ObjectRels;
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetClassesAndObjects()
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };

                var dbReaderGetObjectsOfTripleParts = new OntologyModDBConnector(globals);

                var search = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Object = ClassTriplePart.GUID,
                        ID_RelationType = RelationTypeContains.GUID
                    }
                };

                var resultRead = dbReaderGetObjectsOfTripleParts.GetDataObjectRel(search);

                if (resultRead.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultRead;
                    return result;
                }


                result.Result = dbReaderGetObjectsOfTripleParts.ObjectRels.Where(objRel => objRel.ID_Parent_Other != ClassTriplePart.GUID).ToList();

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<TriplePartItems>> GetUserStories(string idProject)
        {
            var taskResult = await Task.Run<ResultItem<TriplePartItems>>(() =>
            {
                var result = new ResultItem<TriplePartItems>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new TriplePartItems
                    {
                        ProjectToTripleParts = new List<clsObjectRel>(),
                        TriplePartsToObjects = new List<clsObjectRel>(),
                        TriplePartsToTripleParts = new List<clsObjectRel>()
                    }

                };

                var dbReader = new OntologyModDBConnector(globals);
                var searchTipleParts = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = idProject,
                        ID_RelationType = RelationTypeContains.GUID,
                        ID_Parent_Other = ClassTriplePart.GUID
                    }
                };

                result.ResultState = dbReader.GetDataObjectRel(searchTipleParts);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchTriplePartTypes = dbReader.ObjectRels.Select(objRel => new clsObjectRel
                {
                    ID_Object = objRel.ID_Other,
                    ID_RelationType = RelationTypeIsOfType.GUID,
                    ID_Parent_Other = ClassPartType.GUID
                }).ToList();

                var triplePartTypes = new List<clsObjectRel>();
                if (searchTriplePartTypes.Any())
                {
                    var dbReaderTypes = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderTypes.GetDataObjectRel(searchTriplePartTypes);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    triplePartTypes = dbReaderTypes.ObjectRels;
                }

                var searchTriplePartText = dbReader.ObjectRels.Select(objRel => new clsObjectAtt
                {
                    ID_Object = objRel.ID_Other,
                    ID_AttributeType = AttributeTypeTripleText.GUID
                }).ToList();
                var triplePartText = new List<clsObjectAtt>();

                if (searchTriplePartText.Any())
                {
                    var dbReaderText = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderText.GetDataObjectAtt(searchTriplePartText);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    triplePartText = dbReaderText.ObjAtts;
                }

                result.Result.ProjectToTripleParts = (from projetToTriplePart in dbReader.ObjectRels
                                                      join triplePartToType in triplePartTypes on projetToTriplePart.ID_Other equals triplePartToType.ID_Object
                                                      where triplePartToType.ID_Other != ObjectTemplate.GUID
                                                      select projetToTriplePart).ToList();
                result.Result.TriplePartText = (from triplePart in result.Result.ProjectToTripleParts
                                                join tripleText in triplePartText on triplePart.ID_Other equals tripleText.ID_Object
                                                select tripleText).ToList();
                result.Result.TriplePartsToTripleParts = new List<clsObjectRel>();

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ProjectsAndUserStories>> GetProjectsAndUserStories(string idObject)
        {
            var taskResult = await Task.Run<ResultItem<ProjectsAndUserStories>>(async () =>
            {
                var result = new ResultItem<ProjectsAndUserStories>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ProjectsAndUserStories()
                };


                var getObjectResult = await GetOItem(idObject, globals.Type_Object);

                if (getObjectResult.Result.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = getObjectResult.Result;
                    return result;
                }

                result.Result.RootObject = getObjectResult.Result;

                var getClassResult = await GetOItem(result.Result.RootObject.GUID_Parent, globals.Type_Class);

                if (getClassResult.Result.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = getObjectResult.Result;
                    return result;
                }

                result.Result.RootClass = getClassResult.Result;

                var searchPrio = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object = idObject,
                        ID_AttributeType = AttributeTypePrio.GUID
                    }
                };

                var dbReaderObjectAtt = new OntologyModDBConnector(globals);
                result.ResultState = dbReaderObjectAtt.GetDataObjectAtt(searchPrio);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                if (dbReaderObjectAtt.ObjAtts.Any())
                {
                    result.Result.RootObject.Val_Long = dbReaderObjectAtt.ObjAtts.First().Val_Lng;
                }
                else
                {
                    result.Result.RootObject.Val_Long = 0;
                }

                var searchUserStoriesOfObject = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = idObject,
                        ID_RelationType = RelationTypeContains.GUID,
                        ID_Parent_Object = ClassTriplePart.GUID
                    }
                };

                var dbReaderObjectToUserStories = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderObjectToUserStories.GetDataObjectRel(searchUserStoriesOfObject);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchProjectsOfUserStories = dbReaderObjectToUserStories.ObjectRels.Select(objRel => new clsObjectRel
                {
                    ID_Other = objRel.ID_Object,
                    ID_RelationType = RelationTypeContains.GUID,
                    ID_Parent_Object = ClassProject.GUID
                }).ToList();

                var dbReaderProjectsToUserStories = new OntologyModDBConnector(globals);
                if (searchProjectsOfUserStories.Any())
                {
                    result.ResultState = dbReaderProjectsToUserStories.GetDataObjectRel(searchProjectsOfUserStories);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                }
                else
                {
                    dbReaderProjectsToUserStories.ObjectRels = new List<clsObjectRel>();
                }

                var searchObjectsOfUserStories = dbReaderObjectToUserStories.ObjectRels.Select(objRel => new clsObjectRel
                {
                    ID_Object = objRel.ID_Object,
                    ID_RelationType = RelationTypeContains.GUID
                }).ToList();

                var dbReaderObjectsOfUserStories = new OntologyModDBConnector(globals);
                if (searchObjectsOfUserStories.Any())
                {
                    result.ResultState = dbReaderObjectsOfUserStories.GetDataObjectRel(searchObjectsOfUserStories);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                }
                else
                {
                    dbReaderObjectsOfUserStories.ObjectRels = new List<clsObjectRel>();
                }

                var obejctsWithOut = dbReaderObjectsOfUserStories.ObjectRels.Where(objRel => objRel.ID_Other != result.Result.RootObject.GUID && objRel.ID_Parent_Other != ClassTriplePart.GUID);



                result.Result.ObjectToUserStories = dbReaderObjectToUserStories.ObjectRels;
                result.Result.UserStoriesToProjects = dbReaderProjectsToUserStories.ObjectRels;
                result.Result.ObjectsOfUserStories = (from userStoryToObject in dbReaderObjectToUserStories.ObjectRels
                                                      join userStoryToOtherObjects in obejctsWithOut on userStoryToObject.ID_Object equals userStoryToOtherObjects.ID_Object
                                                      select new clsObjectRel
                                                      {
                                                          ID_Object = userStoryToOtherObjects.ID_Object,
                                                          ID_Parent_Object = userStoryToOtherObjects.ID_Parent_Object,
                                                          Name_Object = userStoryToOtherObjects.Name_Object,
                                                          Name_Parent_Object = userStoryToOtherObjects.Name_Parent_Object,
                                                          ID_Other = userStoryToOtherObjects.ID_Other,
                                                          ID_Parent_Other = userStoryToOtherObjects.ID_Parent_Other,
                                                          Name_Other = userStoryToOtherObjects.Name_Other,
                                                          Name_Parent_Other = userStoryToOtherObjects.Name_Parent_Other,
                                                          OrderID = userStoryToObject.OrderID
                                                      }).ToList();



                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ObjectsOfClasses>> GetObjectsOfClasses(string idClass)
        {
            var taskResult = await Task.Run<ResultItem<ObjectsOfClasses>>(async () =>
            {
                var result = new ResultItem<ObjectsOfClasses>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ObjectsOfClasses()
                };

                var oItemGet = await GetOItem(idClass, globals.Type_Class);

                if (oItemGet.Result.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = oItemGet.Result;
                    return result;
                }

                var searchObjects = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = idClass
                    }
                };

                var dbReader = new OntologyModDBConnector(globals);

                result.ResultState = dbReader.GetDataObjects(searchObjects);
                result.Result.Objects = dbReader.Objects1;
                result.Result.RootClass = oItemGet.Result;


                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<TriplePartItems>> GetUserStoriesWithRelatedObjects(string idProject)
        {
            var taskResult = await Task.Run<ResultItem<TriplePartItems>>(() =>
            {
                var result = new ResultItem<TriplePartItems>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new TriplePartItems
                    {
                        ProjectToTripleParts = new List<clsObjectRel>(),
                        TriplePartsToObjects = new List<clsObjectRel>(),
                        TriplePartsToTripleParts = new List<clsObjectRel>()
                    }
                };

                var dbReader = new OntologyModDBConnector(globals);
                var searchTipleParts = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = idProject,
                        ID_RelationType = RelationTypeContains.GUID,
                        ID_Parent_Other = ClassTriplePart.GUID
                    }
                };

                result.ResultState = dbReader.GetDataObjectRel(searchTipleParts);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchTriplePartTypes = dbReader.ObjectRels.Select(objRel => new clsObjectRel
                {
                    ID_Object = objRel.ID_Other,
                    ID_RelationType = RelationTypeIsOfType.GUID,
                    ID_Parent_Other = ClassPartType.GUID
                }).ToList();

                var triplePartTypes = new List<clsObjectRel>();
                if (searchTriplePartTypes.Any())
                {
                    var dbReaderTypes = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderTypes.GetDataObjectRel(searchTriplePartTypes);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    triplePartTypes = dbReaderTypes.ObjectRels;
                }


                var searchTriplePartText = dbReader.ObjectRels.Select(objRel => new clsObjectAtt
                {
                    ID_Object = objRel.ID_Other,
                    ID_AttributeType = AttributeTypeTripleText.GUID
                }).ToList();
                var triplePartText = new List<clsObjectAtt>();

                if (searchTriplePartText.Any())
                {
                    var dbReaderText = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderText.GetDataObjectAtt(searchTriplePartText);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    triplePartText = dbReaderText.ObjAtts;
                }

                var searchObjects = dbReader.ObjectRels.Select(objRel => new clsObjectRel
                {
                    ID_Object = objRel.ID_Other,
                    ID_RelationType = RelationTypeContains.GUID
                }).ToList();

                var objectRels = new List<clsObjectRel>();
                if (searchObjects.Any())
                {
                    var dbReaderObjects = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderObjects.GetDataObjectRel(searchObjects);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    objectRels = dbReaderObjects.ObjectRels;
                }

                result.Result.ProjectToTripleParts = (from projetToTriplePart in dbReader.ObjectRels
                                                      join triplePartToType in triplePartTypes on projetToTriplePart.ID_Other equals triplePartToType.ID_Object
                                                      where triplePartToType.ID_Other != ObjectTemplate.GUID
                                                      select projetToTriplePart).ToList();
                result.Result.TriplePartText = (from triplePart in result.Result.ProjectToTripleParts
                                                join tripleText in triplePartText on triplePart.ID_Other equals tripleText.ID_Object
                                                select tripleText).ToList();
                result.Result.TriplePartsToObjects = (from triplePart in result.Result.ProjectToTripleParts
                                                      join objectItem in objectRels on triplePart.ID_Other equals objectItem.ID_Object into objectItems
                                                      from objectItem in objectItems.DefaultIfEmpty()
                                                      select objectItem).ToList();

                return result;
            });

            return taskResult;
        }

        #region TicketParsing

        public async Task<ResultItem<TicketClassesObjectsRels>> GetTicketClassesObjects(string idProject)
        {
            var taskResult = await Task.Run<ResultItem<TicketClassesObjectsRels>>(async () =>
            {
                var result = new ResultItem<TicketClassesObjectsRels>();

                var project = await GetOItem(idProject, globals.Type_Object);
                result.Result = new TicketClassesObjectsRels
                {
                    Project = project.Result
                };

                result.ResultState = project.ResultState;

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchProjectToTripleParts = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Project.GUID,
                        ID_RelationType = RelationTypeContains.GUID,
                        ID_Parent_Other = ClassTriplePart.GUID
                    }
                };

                var dbReaderProjectToTripleParts = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderProjectToTripleParts.GetDataObjectRel(searchProjectToTripleParts);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.ProjectToTripleParts = dbReaderProjectToTripleParts.ObjectRels;

                var searchTicketToTripleParts = result.Result.ProjectToTripleParts.Select(projectToTicket => new clsObjectRel
                {
                    ID_Other = projectToTicket.ID_Other,
                    ID_RelationType = RelationTypeContains.GUID,
                    ID_Parent_Object = ClassTicket.GUID
                }).ToList();

                result.Result.TicketsToTripleParts = new List<clsObjectRel>();

                if (searchTicketToTripleParts.Any())
                {
                    var dbReaderTicketsToTripleParts = new OntologyModDBConnector(globals);
                    var start = 0;
                    var searchCount = 1000;
                    while (start < searchTicketToTripleParts.Count)
                    {
                        if (searchTicketToTripleParts.Count < start + searchCount)
                        {
                            searchCount = searchTicketToTripleParts.Count - start;
                        }


                        var relationsToQuery = searchTicketToTripleParts.GetRange(start, searchCount);

                        result.ResultState = dbReaderTicketsToTripleParts.GetDataObjectRel(relationsToQuery);

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }


                        result.Result.TicketsToTripleParts.AddRange(dbReaderTicketsToTripleParts.ObjectRels);

                        start += searchCount;
                    }
                }

                var searchTriplePartsToObjects = result.Result.ProjectToTripleParts.Select(projectToTriplePart => new clsObjectRel
                {
                    ID_Object = projectToTriplePart.ID_Other,
                    ID_RelationType = RelationTypeContains.GUID
                }).ToList();

                result.Result.TriplePartsToObjects = new List<clsObjectRel>();

                if (searchTriplePartsToObjects.Any())
                {
                    var dbReaderTriplePartsToObjects = new OntologyModDBConnector(globals);
                    var start = 0;
                    var searchCount = 1000;
                    while (start < searchTriplePartsToObjects.Count)
                    {
                        if (searchTriplePartsToObjects.Count < start + searchCount)
                        {
                            searchCount = searchTriplePartsToObjects.Count - start;
                        }


                        var relationsToQuery = searchTriplePartsToObjects.GetRange(start, searchCount);

                        result.ResultState = dbReaderTriplePartsToObjects.GetDataObjectRel(relationsToQuery);

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }


                        result.Result.TriplePartsToObjects.AddRange(dbReaderTriplePartsToObjects.ObjectRels);

                        start += searchCount;
                    }
                }

                return result;
            });

            return taskResult;
        }

        #endregion

        public ServiceAgentElastic(Globals globals)
        {
            this.globals = globals;
            relationConfig = new clsRelationConfig(globals);
            clsTransaction transaction = new clsTransaction(globals);


            var taskResult = Task.Run<clsOntologyItem>(async () =>
            {
                var result = await CheckBaseStructure();
                if (result.GUID == globals.LState_Error.GUID)
                {
                    throw new Exception("Base-Structures cannot be created!");
                }

                return result;
            });

        }
    }

    //public class ResultCheckClasses
    //{
    //    public clsOntologyItem Result { get; set; }
    //}

    //public class ResultAttributeTypes
    //{
    //    public clsOntologyItem Result { get; set; }
    //}


    //public class ResultCheckObjects
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public List<clsOntologyItem> ExistingObjects { get; set; }
    //}

    //public class ResultCreateProject
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public clsOntologyItem Project { get; set; }
    //}


    public class CreateTriplePart
    {
        public clsOntologyItem TriplePart { get; set; }
        public clsObjectAtt TriplePartText { get; set; }
    }

    //public class ResultCreateTripleParts
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public List<TriplePart> TripleParts { get; set; }
    //}

    public class TriplePart
    {
        public clsOntologyItem TriplePartItem { get; set; }
        public clsObjectAtt TriplePartText { get; set; }
        public clsObjectRel TriplePartToProject { get; set; }
    }

    //public class ResultCreateClassRelations
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public List<clsObjectRel> TriplePartToClasses { get; set; }
    //}

    //public class ResultCreateObjectRelations
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public List<clsObjectRel> TriplePartToObjects { get; set; }
    //}

    //public class ResultGetProjectList
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public List<clsOntologyItem> ProjectList { get; set; }
    //}

    //public class ResultGetTripleParts
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public List<clsObjectRel> ProjectToTripleParts { get; set; }
    //    public List<clsObjectAtt> TriplePartText { get; set; }
    //    public List<clsObjectRel> TriplePartsToTripleParts { get; set; }
    //    public List<clsObjectRel> TriplePartsToObjects { get; set; }
    //}

    public class TriplePartItems
    {
        public List<clsObjectRel> ProjectToTripleParts { get; set; }
        public List<clsObjectAtt> TriplePartText { get; set; }
        public List<clsObjectRel> TriplePartsToTripleParts { get; set; }
        public List<clsObjectRel> TriplePartsToObjects { get; set; }
    }

    //public class ResultGetClassesAndObjects
    //{
    //    public clsOntologyItem Result { get; set; }

    //}

    public class ClassesAndObjectsWithRootItem
    {
        public clsOntologyItem RootItem { get; set; }
        public List<clsOntologyItem> ClassesAndObjects { get; set; }
    }


    //public class ResultGetClassesAndObjectList
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public List<clsObjectRel> ClassesAndObjects { get; set; }
    //}

    //public class ResultClassList
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public List<clsOntologyItem> ClassList { get; set; }
    //}

    //public class ResultObjectList
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public List<clsOntologyItem> ObjectList { get; set; }
    //}

    //public class ResultGetOItem
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public clsOntologyItem OItem { get; set; }
    //}

    //public class ResultTriplePartsOfProjects
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public List<clsObjectRel> ProjectsToTripleParts { get; set; }
    //    public List<clsObjectRel> TriplePartsToPartType { get; set; }
    //    public List<clsObjectRel> ObjectsOfTripleParts { get; set; }
    //    public clsOntologyItem ObjectTemplate { get; set; }
    //    public clsOntologyItem ClassTriplePart { get; set; }
    //}

    public class TriplePartsOfProjects
    {
        public List<clsOntologyItem> Projects { get; set; }
        public List<clsObjectRel> ProjectsToTripleParts { get; set; }
        public List<clsObjectRel> TriplePartsToPartType { get; set; }
        public List<clsObjectRel> ObjectsOfTripleParts { get; set; }
        public clsOntologyItem ObjectTemplate { get; set; }
        public clsOntologyItem ClassTriplePart { get; set; }
    }

    //public class ResultGetProjectsAndUserStories
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public clsOntologyItem RootObject { get; set; }
    //    public clsOntologyItem RootClass { get; set; }
    //    public List<clsObjectRel> ObjectToUserStories { get; set; }
    //    public List<clsObjectRel> UserStoriesToProjects { get; set; }
    //    public List<clsObjectRel> ObjectsOfUserStories { get; set; }
    //}

    public class ProjectsAndUserStories
    {
        public clsOntologyItem RootObject { get; set; }
        public clsOntologyItem RootClass { get; set; }
        public List<clsObjectRel> ObjectToUserStories { get; set; }
        public List<clsObjectRel> UserStoriesToProjects { get; set; }
        public List<clsObjectRel> ObjectsOfUserStories { get; set; }
    }

    public class TicketTemplatesWithClassesAndRegex
    {
        public List<clsObjectRel> TicketTemplates { get; set; }
        public List<clsObjectRel> TicketTemplatesToRegex { get; set; }
        public List<clsObjectAtt> RegexAttributes { get; set; }
        public List<clsObjectRel> Classes { get; set; }
    }

    //public class ResultGetObjectsOfClass
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public clsOntologyItem RootClass { get; set; }
    //    public List<clsOntologyItem> Objects { get; set; }
    //}

    public class ObjectsOfClasses
    {
        public clsOntologyItem RootClass { get; set; }
        public List<clsOntologyItem> Objects { get; set; }
    }
}
