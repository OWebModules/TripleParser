﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TripleParserConnector.Models
{
    public class TriplePart
    {
        public clsOntologyItem ProjectItem { get; set; }
        public string TriplePartText { get; set; }
        public clsObjectAtt TripleArtTextARel { get; set; }
        public clsOntologyItem TriplePartType { get; set; }
        public List<TriplePart> SubParts { get; set; }
        public List<TripleTerm> TripleTerms { get; set; }
        public clsOntologyItem TriplePartOItem { get; set; }
        public long OrderId { get; set; }

        
    }

    public class ResultTripleParts
    {
        public clsOntologyItem Result { get; set; }
        public List<TriplePart> TripleParts { get; set; }
    }
}
