﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripleParserConnector.Models
{
    public class TicketClassesObjectsRels
    {
        public clsOntologyItem Project { get; set; }
        public List<clsObjectRel> ProjectToTripleParts { get; set; }
        public List<clsObjectRel> TicketsToTripleParts { get; set; }
        public List<clsObjectRel> TriplePartsToObjects { get; set; }
    }
}
