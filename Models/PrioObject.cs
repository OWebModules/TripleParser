﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripleParserConnector.Models
{
    public class PrioObject
    {
        public string IdObject { get; set; }
        public string NameObject { get; set; }
        public string IdClass { get; set; }
        public string NameClass { get; set; }
        public long CountTripleParts { get; set; }
        public string IdAttributePrio { get; set; }
        public long? Prio { get; set; }
    }
}
