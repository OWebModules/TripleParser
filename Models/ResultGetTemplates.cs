﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripleParserConnector.Models
{
    //public class ResultGetTemplates
    //{
    //    public clsOntologyItem Result { get; set; }
    //    public ParseTripleOfProject TripleParse { get; set;}

        

    //}

    public class ParseTripleOfProject
    {
        public clsOntologyItem ProjectItem { get; set; }
        public List<ParseTripleTemplate> ParseTemplates { get; set; }
    }

    public class ParseTripleTemplate
    {
        public clsObjectRel ProjectToTriplePart { get; set; }
        public List<clsObjectRel> TemplatesToTripleParts { get; set; }
        public List<clsObjectAtt> TemplatesText { get; set; }
        public List<clsObjectRel> TemplatesToTypes { get; set; }
        public List<clsObjectRel> TemplatesToWordsBefore { get; set; }
        public List<clsObjectRel> TemplatesToWordsAfter { get; set; }
        public List<clsObjectRel> TemplatesToClasses { get; set; }
        public List<clsObjectRel> TemplatesSynonyms { get; set; }
        public List<clsObjectRel> AttributeTermToTemplates { get; set; }
        public List<clsObjectRel> AttributeTermToDataTypes { get; set; }

        
    }


}
