﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TripleParserConnector.Models
{
    public class ParsedTriplePart
    {
        public Globals Globals { get; set; }
        public TriplePart Template { get; set; }
        public clsOntologyItem TriplePart { get; set; }
        public clsObjectAtt TriplePartText { get; set; }
        public clsOntologyItem ProjectItem { get; set; }
        public clsOntologyItem TriplePartType { get; set; }
        public string HtmlText { get; set; }
        public List<TextPart> TextParts { get; set; }
        public List<FoundTerm> FoundTerms { get; set; }
        public bool BestResult { get; set; }
        public long OrderId { get; set; }

        /// <summary>
        /// Percent of succeeded Term-recognitions
        /// </summary>
        public double ParseSuccessPercent
        {
            get
            {
                if (FoundTerms.Count == 0) return 0;
                var parseItemsNoSynonym = FoundTerms.Where(parseItem => parseItem.TemplateTerm.Synonym == null).ToList();
                return 100.0 / parseItemsNoSynonym.Count * parseItemsNoSynonym.Count(parseItm => parseItm.ParseSuccessful);
            }
        }

        /// <summary>
        /// Efficiency of ratio between Terms and found Terms in the text
        /// ToDo: Optimize the calculation
        /// </summary>
        public double Efficiency
        {
            get
            {
                if (!Template.TripleTerms.Any()) return 0;
                var diff = Template.TripleTerms.Count - FoundTerms.Count;
                diff = diff + 1;
                var diffFactor = 1.0 / diff;

                var factor = Template.TripleTerms.Count == FoundTerms.Count ? 2.0 : 0.5;
                var result = diffFactor * ParseSuccessPercent * factor;
                return result;
            }
        }

        public ParsedTriplePart(clsOntologyItem projectItem,
                                TriplePart template,
                                Globals globals,
                                clsOntologyItem triplePart,
                                clsObjectAtt triplePartText,
                                clsOntologyItem triplePartType,
                                long orderId)
        {
            Template = template;
            FoundTerms = new List<FoundTerm>();
            TextParts = new List<TextPart>();
            TriplePart = triplePart;
            TriplePartText = triplePartText;
            TriplePartType = triplePartType;
            ProjectItem = projectItem;
            OrderId = orderId;
            Globals = globals;
        }

        /// <summary>
        /// Recognize the Terms
        /// </summary>
        public void CompareWords()
        {
            var parsePointer = new ParsePointer
            {
                Offset = 0,
                Text = TriplePart.Additional1
            };

            HtmlText = "";

            // Pattern for datatypes
            var regexBool = new Regex(ParseParts.BoolPattern);
            var regexDouble = new Regex(ParseParts.DoublePattern);
            var regexInt = new Regex(ParseParts.IntPattern);
            var regexDateTime = new Regex(ParseParts.DateTimePattern);

            // No Synonyms. Recognized Objects will be saved to Synonym-Class automatically
            var parseItemsNoSynonym = Template.TripleTerms.Where(parseItem => parseItem.Synonym == null).ToList();
            for (var ix = 0; ix < parseItemsNoSynonym.Count; ix++)
            {

                var parseItem = parseItemsNoSynonym[ix];
                var foundTerm = new FoundTerm
                {
                    TemplateTerm = parseItem
                };

                // Seperators between the words
                var seperator = $"{ParseParts.SeperatorsPattern}(\\s)?";

                // The regex before the Term (before-words with seperator-regex between)
                var regexBefore = $"{string.Join(seperator, parseItem.BeforeWords.Select(word => word.Name.Replace("(",@"\(").Replace(")", @"\)")))}(\\s)?";

                if (ix > 0)
                {
                    regexBefore = $"{seperator}{regexBefore}";
                }

                // The regex after the Term (after-words with seperator-regex between)
                var regexAfter = "";
                if (parseItem.AfterWords.Any())
                {
                    regexAfter = $"(\\s)?{string.Join(seperator, parseItem.AfterWords.Select(word => word.Name.Replace("(", @"\(").Replace(")", @"\)")))}";
                }

                // Matches of before-regex in the text
                var matchesBefore = Regex.Matches(parsePointer.Text, regexBefore).Cast<Match>().ToList();
                if (!string.IsNullOrEmpty(regexAfter))
                {

                    // Matches of after-regex in the text
                    var matchesAfter = Regex.Matches(parsePointer.Text, regexAfter).Cast<Match>().ToList();
                    var match = matchesBefore.Select(matchBefore =>
                    {
                        var matchAfter = matchesAfter.FirstOrDefault(matchA => matchA.Index > matchBefore.Index + matchBefore.Length);

                        return new { Before = matchBefore, After = matchAfter };
                    }).FirstOrDefault(matchItm => matchItm.Before.Success && (matchItm.After == null || matchItm.After.Success));

                    // Term is found in text
                    // ToDo: Handle empty matches
                    if (match != null)
                    {
                        // Start: After the before-word
                        parsePointer.StartIxCut = match.Before.Index + match.Before.Length;

                        TextParts.Add(new TextPart
                        {
                            Text = parsePointer.Text.Substring(0, parsePointer.StartIxCut)
                        });

                        // Length: To the after-word or to the end of the text
                        parsePointer.LengthCut = match.After != null ? match.After.Index - parsePointer.StartIxCut : parsePointer.Text.Length - parsePointer.StartIxCut;

                        var objectString = parsePointer.Text.Substring(parsePointer.StartIxCut, parsePointer.LengthCut);


                        TextParts.Add(new TextPart
                        {
                            Text = objectString,
                            GuidTerm = parseItem.TripleTermOItem.GUID
                        });

                        foundTerm.StartIx = parsePointer.Offset + parsePointer.StartIxCut;
                        foundTerm.EndIx = foundTerm.StartIx + parsePointer.LengthCut - 1;

                        // Set pointer to the end of found term
                        parsePointer.StartIxCut += parsePointer.LengthCut;

                        
                        if (parseItem.TripleTermAttribute == null)
                        {
                            foundTerm.ObjectItem = new clsOntologyItem
                            {
                                Name = objectString,
                                GUID_Parent = parseItem.ClassItem.GUID,
                                Type = Globals.Type_Object
                            };
                        }
                        // Should the Term be recognized as an attribute (int, double, datetime, bool)
                        else
                        {
                            // Term will be an attribute of the last found term
                            var lastTerm = FoundTerms.LastOrDefault();
                            if (lastTerm != null)
                            {
                                if (parseItem.TripleTermAttribute.DataType.Name.ToLower() == "bit")
                                {
                                    var matchDataType = regexBool.Match(objectString);
                                    if (matchDataType.Success)
                                    {
                                        bool test;
                                        if (bool.TryParse(matchDataType.Value, out test))
                                        {
                                            foundTerm.AttributeMeta = new AttributeTerm
                                            {
                                                AttributeType = new clsOntologyItem
                                                {
                                                    Name = parseItem.TripleTermAttribute.Name,
                                                    GUID_Parent = Globals.DType_Bool.GUID,
                                                    Type = Globals.Type_AttributeType
                                                },
                                                AttributeValue = test,
                                                TermWithAttribute = lastTerm
                                            };
                                            foundTerm.ParseSuccessful = true;
                                        }

                                    }
                                }
                                else if (parseItem.TripleTermAttribute.DataType.Name.ToLower() == "datetime")
                                {
                                    var matchDataType = regexDateTime.Match(objectString);
                                    if (matchDataType.Success)
                                    {
                                        DateTime test;
                                        if (DateTime.TryParse(matchDataType.Value, out test))
                                        {
                                            foundTerm.AttributeMeta = new AttributeTerm
                                            {
                                                AttributeType = new clsOntologyItem
                                                {
                                                    Name = parseItem.TripleTermAttribute.Name,
                                                    GUID_Parent = Globals.DType_DateTime.GUID,
                                                    Type = Globals.Type_AttributeType
                                                },
                                                AttributeValue = test,
                                                TermWithAttribute = lastTerm
                                            };
                                            foundTerm.ParseSuccessful = true;
                                        }

                                    }
                                }
                                else if (parseItem.TripleTermAttribute.DataType.Name.ToLower() == "int")
                                {
                                    var matchDataType = regexInt.Match(objectString);
                                    if (matchDataType.Success)
                                    {
                                        long test;
                                        if (long.TryParse(matchDataType.Value, out test))
                                        {
                                            foundTerm.AttributeMeta = new AttributeTerm
                                            {
                                                AttributeType = new clsOntologyItem
                                                {
                                                    Name = parseItem.TripleTermAttribute.Name,
                                                    GUID_Parent = Globals.DType_Int.GUID,
                                                    Type = Globals.Type_AttributeType
                                                },
                                                AttributeValue = test,
                                                TermWithAttribute = lastTerm
                                            };
                                            foundTerm.ParseSuccessful = true;
                                        }

                                    }
                                }
                                else if (parseItem.TripleTermAttribute.DataType.Name.ToLower() == "double")
                                {
                                    var matchDataType = regexDouble.Match(objectString);
                                    if (matchDataType.Success)
                                    {
                                        double test;
                                        if (double.TryParse(matchDataType.Value, out test))
                                        {
                                            foundTerm.AttributeMeta = new AttributeTerm
                                            {
                                                AttributeType = new clsOntologyItem
                                                {
                                                    Name = parseItem.TripleTermAttribute.Name,
                                                    GUID_Parent = Globals.DType_Real.GUID,
                                                    Type = Globals.Type_AttributeType
                                                },
                                                AttributeValue = test,
                                                TermWithAttribute = lastTerm
                                            };
                                            foundTerm.ParseSuccessful = true;
                                        }

                                    }
                                }
                                // longer than 255 characters
                                else if (parseItem.TripleTermAttribute.DataType.Name.ToLower() == "string")
                                {
                                    foundTerm.AttributeMeta = new AttributeTerm
                                    {
                                        AttributeType = new clsOntologyItem
                                        {
                                            Name = parseItem.TripleTermAttribute.Name,
                                            GUID_Parent = Globals.DType_String.GUID,
                                            Type = Globals.Type_AttributeType
                                        },
                                        AttributeValue = objectString,
                                        TermWithAttribute = lastTerm
                                    };
                                }
                            }

                        }


                        foundTerm.ParseSuccessful = true;
                        parsePointer.Offset += parsePointer.StartIxCut;
                        parsePointer.Text = parsePointer.Text.Substring(parsePointer.StartIxCut);


                        FoundTerms.Add(foundTerm);
                    }
                }
                // no after-word found
                else
                {
                    if (matchesBefore.Any())
                    {
                        var match = matchesBefore.Last();
                        if (match.Success)
                        {
                            parsePointer.StartIxCut = match.Index + match.Length;


                            TextParts.Add(new TextPart
                            {
                                Text = parsePointer.Text.Substring(0, parsePointer.StartIxCut)
                            });
                            parsePointer.LengthCut = parsePointer.Text.Length - parsePointer.StartIxCut;


                            var objectString = parsePointer.Text.Substring(parsePointer.StartIxCut, parsePointer.LengthCut);


                            TextParts.Add(new TextPart
                            {
                                Text = objectString,
                                GuidTerm = parseItem.TripleTermOItem.GUID
                            });

                            foundTerm.StartIx = parsePointer.Offset + parsePointer.StartIxCut;
                            foundTerm.EndIx = foundTerm.StartIx + parsePointer.LengthCut - 1;
                            parsePointer.StartIxCut += parsePointer.LengthCut;

                            if (parseItem.TripleTermAttribute == null)
                            {
                                foundTerm.ObjectItem = new clsOntologyItem
                                {
                                    Name = objectString,
                                    GUID_Parent = parseItem.ClassItem.GUID,
                                    Type = Globals.Type_Object
                                };
                                foundTerm.ParseSuccessful = true;
                            }
                            else
                            {
                                var lastTerm = FoundTerms.LastOrDefault();
                                if (lastTerm != null)
                                {
                                    if (parseItem.TripleTermAttribute.DataType.Name.ToLower() == "bit")
                                    {
                                        var matchDataType = regexBool.Match(objectString);
                                        if (matchDataType.Success)
                                        {
                                            bool test;
                                            if (bool.TryParse(matchDataType.Value, out test))
                                            {
                                                foundTerm.AttributeMeta = new AttributeTerm
                                                {
                                                    AttributeType = new clsOntologyItem
                                                    {
                                                        Name = parseItem.TripleTermAttribute.Name,
                                                        GUID_Parent = Globals.DType_Bool.GUID,
                                                        Type = Globals.Type_AttributeType
                                                    },
                                                    AttributeValue = test,
                                                    TermWithAttribute = lastTerm
                                                };
                                                foundTerm.ParseSuccessful = true;
                                            }

                                        }
                                    }
                                    else if (parseItem.TripleTermAttribute.DataType.Name.ToLower() == "datetime")
                                    {
                                        var matchDataType = regexDateTime.Match(objectString);
                                        if (matchDataType.Success)
                                        {
                                            DateTime test;
                                            if (DateTime.TryParse(matchDataType.Value, out test))
                                            {
                                                foundTerm.AttributeMeta = new AttributeTerm
                                                {
                                                    AttributeType = new clsOntologyItem
                                                    {
                                                        Name = parseItem.TripleTermAttribute.Name,
                                                        GUID_Parent = Globals.DType_DateTime.GUID,
                                                        Type = Globals.Type_AttributeType
                                                    },
                                                    AttributeValue = test,
                                                    TermWithAttribute = lastTerm
                                                };
                                                foundTerm.ParseSuccessful = true;
                                            }

                                        }
                                    }
                                    else if (parseItem.TripleTermAttribute.DataType.Name.ToLower() == "int")
                                    {
                                        var matchDataType = regexInt.Match(objectString);
                                        if (matchDataType.Success)
                                        {
                                            long test;
                                            if (long.TryParse(matchDataType.Value, out test))
                                            {
                                                foundTerm.AttributeMeta = new AttributeTerm
                                                {
                                                    AttributeType = new clsOntologyItem
                                                    {
                                                        Name = parseItem.TripleTermAttribute.Name,
                                                        GUID_Parent = Globals.DType_Int.GUID,
                                                        Type = Globals.Type_AttributeType
                                                    },
                                                    AttributeValue = test,
                                                    TermWithAttribute = lastTerm
                                                };
                                                foundTerm.ParseSuccessful = true;
                                            }

                                        }
                                    }
                                    else if (parseItem.TripleTermAttribute.DataType.Name.ToLower() == "double")
                                    {
                                        var matchDataType = regexDouble.Match(objectString);
                                        if (matchDataType.Success)
                                        {
                                            double test;
                                            if (double.TryParse(matchDataType.Value, out test))
                                            {
                                                foundTerm.AttributeMeta = new AttributeTerm
                                                {
                                                    AttributeType = new clsOntologyItem
                                                    {
                                                        Name = parseItem.TripleTermAttribute.Name,
                                                        GUID_Parent = Globals.DType_Real.GUID,
                                                        Type = Globals.Type_AttributeType
                                                    },
                                                    AttributeValue = test,
                                                    TermWithAttribute = lastTerm
                                                };
                                                foundTerm.ParseSuccessful = true;
                                            }

                                        }
                                    }
                                    else if (parseItem.TripleTermAttribute.DataType.Name.ToLower() == "string")
                                    {
                                        foundTerm.AttributeMeta = new AttributeTerm
                                        {
                                            AttributeType = new clsOntologyItem
                                            {
                                                Name = parseItem.TripleTermAttribute.Name,
                                                GUID_Parent = Globals.DType_String.GUID,
                                                Type = Globals.Type_AttributeType
                                            },
                                            AttributeValue = objectString,
                                            TermWithAttribute = lastTerm
                                        };
                                    }
                                }
                            }


                            parsePointer.Offset += parsePointer.StartIxCut;
                            parsePointer.Text = parsePointer.Text.Substring(parsePointer.StartIxCut);


                            FoundTerms.Add(foundTerm);
                        }

                        if (foundTerm.EndIx < parsePointer.Text.Length - 1)
                        {
                            TextParts.Add(new TextPart
                            {
                                Text = parsePointer.Text.Substring(0, parsePointer.Text.Length - foundTerm.EndIx)
                            });
                        }
                    }
                }


            }

            // Synonym: found Term will be also recognized as Instance of an Synonym of the Term
            var parseItemsSynonyms = Template.TripleTerms.Where(parseItem => parseItem.Synonym != null).ToList();

            foreach (var parseItem in parseItemsSynonyms)
            {
                var synonym = Template.TripleTerms.FirstOrDefault(parseItm => parseItm == parseItem.Synonym);
                if (synonym == null) continue;
                var synonymFound = FoundTerms.FirstOrDefault(foundTerm => foundTerm.TemplateTerm == synonym);

                if (synonymFound == null || synonymFound.ObjectItem == null) continue;
                var foundTermToInsert = new FoundTerm
                {
                    TemplateTerm = parseItem,
                    ObjectItem = new clsOntologyItem
                    {
                        Name = synonymFound.ObjectItem.Name,
                        GUID_Parent = parseItem.ClassItem.GUID,
                        Type = Globals.Type_Object
                    },
                    ObjectItemSynonym = synonymFound.ObjectItemSynonym,
                    ParseSuccessful = true
                };

                FoundTerms.Add(foundTermToInsert);
            }

            // Create HTML-Text of found Terms
            // ToDo: Extract to HTML-Factory
            foreach (var textPart in TextParts)
            {
                if (!string.IsNullOrEmpty(textPart.GuidTerm))
                {
                    HtmlText += $"<span class='borderd' data-guidTerm='{textPart.GuidTerm}'>{System.Web.HttpUtility.HtmlEncode(textPart.Text)}</span>";
                }
                else
                {
                    HtmlText += $"<span>{System.Web.HttpUtility.HtmlEncode(textPart.Text)}</span>";
                }
            }
            HtmlText = $"<p>{HtmlText}</p>";
        }
    }
}
