﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TripleParserConnector.TripleParserWithTemplates;

namespace TripleParserConnector.Models
{
    public class TemplateParse
    {
        public string IdProject { get; set; }
        public string NameProject { get; set; }
        public string IdTripleTemplate { get; set; }
        public string NameTripleTemplate { get; set; }
        public string IdTriplePart { get; set; }
        public string NameTriplePart { get; set; }
        public string Text { get; set; }
        public List<TextPart> TextParts { get; set; }
        public string HtmlText { get; set; }

        public int LengthText
        {
            get
            {
                return Text.Length;
            }
        }
        public ParsePartItem ParsePartItem { get; set; }
        public List<TemplatePraseItem> ParseItems { get; set; }
        public double ParseSuccessPercent
        {
            get
            {
                if (ParseItems.Count == 0) return 0;
                var parseItemsNoSynonym = ParseItems.Where(parseItem => parseItem.TemplateTerm.Synonym == null).ToList();
                return 100.0 / parseItemsNoSynonym.Count * parseItemsNoSynonym.Count(parseItm => parseItm.ParseSuccessful);
            }
        }


        public TemplateParse()
        {
            ParseItems = new List<TemplatePraseItem>();
            TextParts = new List<TextPart>();
        }
        public void CompareWords(string text, Globals globals)
        {
            Text = text;
            HtmlText = "";
            var startIx = 0;
            var parseItemsNoSynonym = ParseItems.Where(parseItem => parseItem.TemplateTerm.Synonym == null).ToList();
            for (var ix = 0; ix < parseItemsNoSynonym.Count; ix++)
            {
                startIx = Text.Length - text.Length;
                var parseItem = parseItemsNoSynonym[ix];
                
                var seperator = $"{ParseParts.SeperatorsPattern}(\\s)?";
                var regexBefore = $"{string.Join(seperator, parseItem.TemplateTerm.WordsBefore.Select(word => word.Word))}(\\s)?";

                if (ix > 0)
                {
                    regexBefore = $"{seperator}{regexBefore}";
                }
                var regexAfter = "";
                if (parseItem.TemplateTerm.WordsAfter.Any())
                {
                    regexAfter = $"(\\s)?{string.Join(seperator, parseItem.TemplateTerm.WordsAfter.Select(word => word.Word))}";
                }

                var matchesBefore = Regex.Matches(text, regexBefore).Cast<Match>().ToList();
                if (!string.IsNullOrEmpty(regexAfter))
                {
                    var matchesAfter = Regex.Matches(text, regexAfter).Cast<Match>().ToList();
                    var match = matchesBefore.Select(matchBefore =>
                    {
                        var matchAfter = matchesAfter.FirstOrDefault(matchA => matchA.Index > matchBefore.Index);

                        return new { Before = matchBefore, After = matchAfter };
                    }).FirstOrDefault(matchItm => matchItm.Before.Success && (matchItm.After == null || matchItm.After.Success));

                    if (match != null)
                    {

                        parseItem.StartIx = match.Before.Index + match.Before.Length;
                        parseItem.EndIx = match.After != null ? match.After.Index : text.Length - 1;

                        TextParts.Add(new TextPart
                        {
                            Text = text.Substring(0, parseItem.StartIx)
                        });
                        var objectString = text.Substring(parseItem.StartIx, parseItem.EndIx - parseItem.StartIx);
                        TextParts.Add(new TextPart
                        {
                            Text = objectString,
                            GuidTerm = parseItem.TemplateTerm.GuidTerm
                        });

                        parseItem.StartIx += startIx;
                        parseItem.EndIx = parseItem.StartIx + objectString.Length;

                        parseItem.ObjectItem = new clsOntologyItem
                        {
                            Name = objectString,
                            GUID_Parent = parseItem.TemplateTerm.Class.GUID,
                            Type = globals.Type_Object
                        };

                        parseItem.ParseSuccessful = true;
                        startIx += parseItem.StartIx + objectString.Length;
                        if (match.After != null && match.After.Success)
                        {
                            startIx = text.Length;
                            text = text.Substring(match.After.Index);
                            startIx -= text.Length;
                        }

                    }
                }
                else
                {
                    if (matchesBefore.Any())
                    {
                        var match = matchesBefore.Last();
                        if (match.Success)
                        {

                            parseItem.StartIx = match.Index + match.Length;
                            parseItem.EndIx = text.Length - parseItem.StartIx;
                            TextParts.Add(new TextPart
                            {
                                Text = text.Substring(0, parseItem.StartIx)
                            });
                            var objectString = text.Substring(parseItem.StartIx);
                            parseItem.ObjectItem = new clsOntologyItem
                            {
                                Name = objectString,
                                GUID_Parent = parseItem.TemplateTerm.Class.GUID,
                                Type = globals.Type_Object
                            };
                            TextParts.Add(new TextPart
                            {
                                Text = objectString,
                                GuidTerm = parseItem.TemplateTerm.GuidTerm
                            });
                            parseItem.ParseSuccessful = true;
                            parseItem.StartIx += startIx;
                            parseItem.EndIx = parseItem.StartIx + objectString.Length;
                        }

                        if (parseItem.EndIx < text.Length - 1)
                        {
                            TextParts.Add(new TextPart
                            {
                                Text = text.Substring(0, text.Length - parseItem.EndIx)
                            });
                        }
                    }
                }
                
                
            }

            var parseItemsSynonyms = ParseItems.Where(parseItem => parseItem.TemplateTerm.Synonym != null).ToList();

            foreach (var parseItem in parseItemsSynonyms)
            {
                var synonym = ParseItems.FirstOrDefault(parseItm => parseItm.TemplateTerm == parseItem.TemplateTerm.Synonym);

                if (synonym != null && synonym.ObjectItem != null)
                {
                    parseItem.ObjectItem = new clsOntologyItem
                    {
                        Name = synonym.ObjectItem.Name,
                        GUID_Parent = parseItem.TemplateTerm.Class.GUID,
                        Type = globals.Type_Object
                    };
                    parseItem.ParseSuccessful = synonym.ParseSuccessful;
                    parseItem.ObjectItemSynonym = synonym.ObjectItem;
                }
            }

            foreach (var textPart in TextParts)
            {
                if (!string.IsNullOrEmpty(textPart.GuidTerm))
                {
                    HtmlText += $"<span class='borderd' data-guidTerm='{textPart.GuidTerm}'>{System.Web.HttpUtility.HtmlEncode(textPart.Text)}</span>";
                }
                else
                {
                    HtmlText += $"<span>{System.Web.HttpUtility.HtmlEncode(textPart.Text)}</span>";
                }
            }
            HtmlText = $"<p>{HtmlText}</p>";
        }
    }

    public class TemplatePraseItem
    {
        //public string IdTemplate { get; set; }
        //public string NameTemplate { get; set; }
        //public List<clsObjectRel> Words { get; set; }
        //public string IdSynonymTemplate { get; set; }
        //public string NameSynonymTemplate { get; set; }
        //public string IdClass { get; set; }
        //public string NameClass { get; set; }
        //public long OrderIdTemplate { get; set; }
        //public clsOntologyItem ObjectItem { get; set; }
        //public clsOntologyItem ObjectItemSynonym { get; set; }
        public TemplateTerm TemplateTerm { get; set; }
        public bool ParseSuccessful { get; set; }
        public clsOntologyItem ObjectItem { get; set; }
        public clsOntologyItem ObjectItemSynonym { get; set; }
        public clsObjectAtt ObjectAttribute { get; set; }
        public int StartIx { get; set; }
        public int EndIx { get; set; }
    }

    

    public class TextPart
    {
        public string Text { get; set; }
        public string GuidTerm { get; set; }
    }
}
