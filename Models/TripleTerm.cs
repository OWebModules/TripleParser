﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripleParserConnector.Models
{
    public class TripleTerm
    {
        public TripleTerm Synonym { get; set; }
        public clsOntologyItem ClassItem { get; set; }
        public clsOntologyItem TriplePartOItem{ get; set; }
        public clsOntologyItem TripleTermOItem { get; set; }
        public clsOntologyItem SynonymOItem { get; set; }
        public List<clsOntologyItem> BeforeWords { get; set; }
        public List<clsOntologyItem> AfterWords { get; set; }
       
        public TripleTermAttribute TripleTermAttribute { get; set; }
        public long OrderId { get; set; }

       

    }
}
