﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripleParserConnector.Models
{
    public class ResultGetProjectById
    {
        public clsOntologyItem Result { get; set;  }
        public clsOntologyItem ProjectItem { get; set; }
    }
}
