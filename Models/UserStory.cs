﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripleParserConnector.Models
{
    public class UserStory
    {
        public string IdUserStory { get; set; }
        public string Name { get; set; }
        public long Priority { get; set; }
        public string PriorityString => Priority.ToString();
        public long Order { get; set; }
        public string OrderString => Order.ToString();
        public string Object { get; set; }
        public string Class { get; set; }
    }
}
