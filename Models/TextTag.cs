﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripleParserConnector.Models
{
    public class TextTag
    {
        public string Id { get; set; }
        public clsOntologyItem Reference { get; set; }
        public clsObjectRel ReferenceParentItem { get; set; }
        public int Start { get; set; }
        public int End { get; set; }
    }

    
}
