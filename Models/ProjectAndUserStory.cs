﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripleParserConnector.Models
{
    public class ProjectAndUserStory
    {
        public clsOntologyItem TriplePart { get; set; }
        public clsObjectAtt TriplePartText { get; set; }
        public clsOntologyItem Project { get; set; }
    }
}
