﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripleParserConnector.Models
{
    public class ParsePointer
    {
        public string Text { get; set; }
        public int Offset { get; set; }
        public int StartIxCut { get; set; }
        public int LengthCut { get; set; }

    }
}
