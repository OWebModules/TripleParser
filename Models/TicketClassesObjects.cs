﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TripleParserConnector.Models
{
    public class TicketClassesObjects
    {
        public string IdProject { get; set; }
        public string NameProject { get; set; }
        public string IdTicket { get; set; }
        public string NameTicket { get; set; }
        public string IdClass { get; set; }
        public string NameClass { get; set; }
        public string IdObject { get; set; }
        public string NameObject { get; set; }
    }
}