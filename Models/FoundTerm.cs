﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripleParserConnector.Models
{
    public class FoundTerm
    {
        public TripleTerm TemplateTerm { get; set; }
        public int StartIx { get; set; }
        public int EndIx { get; set; }

        public clsOntologyItem ObjectItem { get; set; }
        public clsOntologyItem ObjectItemSynonym { get; set; }
        public AttributeTerm AttributeMeta { get; set; }
        public bool ParseSuccessful { get; set; }
    }

    public class AttributeTerm
    {
        public clsOntologyItem AttributeType { get; set; }
        public FoundTerm TermWithAttribute { get; set; }
        public object AttributeValue{ get; set; }
    }
}
