﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripleParserConnector.Models
{
    public class ProjectsTriplePartsAndObjectsWithPrios
    {
        public List<clsObjectRel> ProjectToTripleParts { get; set; }
        public List<clsObjectRel> TriplePartsToObjects { get; set; }
        public List<clsObjectAtt> ObjectPrios { get; set; }
    }
}
