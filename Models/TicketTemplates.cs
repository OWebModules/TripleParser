﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TripleParserConnector.Models
{
    public class TicketTemplates
    {
        public List<TicketTemplateRegex> TicketTemplatesRegex { get; set; }
        public List<TicketTemplateClass> TicketTemplatesClasses { get; set; }
        public string HtmlText(string text)
        {
            List<TextTag> textTags = new List<TextTag>();

            TicketTemplatesClasses.SelectMany(ticketTemplateClass => ticketTemplateClass.TextTags).ToList().ForEach(textTag =>
            {
                //if (!textTags.Any(textTag1 => textTag1.Reference.GUID == textTag.Reference.GUID))
                //{
                    textTags.Add(textTag);
                //}
            });
            TicketTemplatesRegex.SelectMany(clss => clss.TextTags).ToList().ForEach(textTag =>
            {
                //if (!textTags.Any(textTag1 => textTag1.Reference.GUID == textTag.Reference.GUID))
                //{
                    textTags.Add(textTag);
                //}
            });
            

            textTags = textTags.OrderBy(textTag => textTag.Start).ToList();

            StringBuilder sbResult = new StringBuilder();

            GetTextTags(textTags, text, sbResult);

            return sbResult.ToString();
        }

        private void GetTextTags(List<TextTag> textTags, string text, StringBuilder sbResult)
        {
            List<TextTag> nextTags = new List<TextTag>();
            sbResult.Append("<div>");
            var lastStart = 0;
            var lastEnd = 0;
            for (int i = 0; i < textTags.Count; i++)
            {
                TextTag lastTextTag = null;
                if (i > 0)
                {
                    lastTextTag = textTags[i - 1];
                }
                var textTag = textTags[i];

                if (lastTextTag != null && (lastStart >= textTag.Start || lastEnd >= textTag.Start))
                {
                    nextTags.Add(textTag);
                }
                else
                {
                    sbResult.Append(text.Substring(lastEnd, textTag.Start - lastEnd));
                    sbResult.Append($"<button id='{textTag.Id}' class='btn btn-success btn-xs buttonRel' title='{textTag.ReferenceParentItem.Name_Other}'>{textTag.Reference.Name}</button>");


                    lastStart = textTag.Start;
                    lastEnd = textTag.End;
                    if (i == textTags.Count - 1 && text.Length > lastEnd)
                    {
                        sbResult.Append(text.Substring(lastEnd));
                    }
                }
                
            }

            sbResult.Append("</div>");

            if (nextTags.Any())
            {
                GetTextTags(nextTags, text, sbResult);
            }
            
        }
    }

    
    public class TicketTemplateClass
    {
        public clsObjectRel TicketTemplateItem { get; private set; }
        public clsObjectRel ToClassItem { get; private set; }
        public List<clsOntologyItem> ObjectItems { get; set; }
        public string TextToCheck { get; set; }
        public List<TextTag> TextTags { get; set; }

        public bool IsContained(string text)
        {
            TextToCheck = text;
            text = text.ToLower();
            TextTags = new List<TextTag>();

            foreach (var item in ObjectItems)
            {
                var lastIndex = 0;
                var existingTags = TextTags.Where(textTag => textTag.Reference.Name.ToLower() == item.Name.ToLower());

                if (existingTags.Any())
                {
                    lastIndex = existingTags.Max(textTag => textTag.End);
                }
                var ix = text.IndexOf(item.Name.ToLower(), lastIndex);
                while (ix != -1)
                {
                    
                    TextTags.Add(new TextTag
                    {
                        Id = Guid.NewGuid().ToString(),
                        Reference = item,
                        ReferenceParentItem = ToClassItem,
                        Start = ix,
                        End = ix + item.Name.Length
                    });
                    lastIndex = ix + 1;
                    ix = text.IndexOf(item.Name.ToLower(), lastIndex);
                }
                
            }

            return TextTags.Any();
        }

        public TicketTemplateClass(clsObjectRel ticketTemplateItem, clsObjectRel toClassItem)
        {
            ObjectItems = new List<clsOntologyItem>();
            TicketTemplateItem = ticketTemplateItem;
            ToClassItem = toClassItem;
            TextTags = new List<TextTag>();
        }
    }

    public class TicketTemplateRegex
    {
        public clsObjectRel TicketTemplate { get; private set; }
        public clsObjectRel TemplateToRegex { get; private set; }
        public clsObjectAtt RegexObject { get; private set; }
        public clsObjectRel ToClassItem { get; private set; }
        public List<clsOntologyItem> ObjectItems { get; set; }
        public Regex Regex { get; private set; }

        public string TextToCheck { get; set; }
        public List<TextTag> TextTags { get; set; }

        public bool IsContained(string text, Globals globals)
        {
            TextToCheck = text;
            text = text.ToLower();
            TextTags = new List<TextTag>();

            foreach (var item in ObjectItems)
            {
                var lastIndex = 0;
                var existingTags = TextTags.Where(textTag => textTag.Reference.Name.ToLower() == item.Name.ToLower());
                if (existingTags.Any())
                {
                    lastIndex = existingTags.Max(textTag => textTag.End);
                }

                var ix = text.IndexOf(item.Name.ToLower(), lastIndex);
                while (ix != -1)
                {
                    TextTags.Add(new TextTag
                    {
                        Id = Guid.NewGuid().ToString(),
                        Reference = item,
                        ReferenceParentItem = ToClassItem,
                        Start = ix,
                        End = ix + item.Name.Length
                    });
                    lastIndex = ix + 1;
                    ix = text.IndexOf(item.Name.ToLower(), lastIndex);
                }
            }

            if (!TextTags.Any())
            {
                var matches = Regex.Matches(TextToCheck);
                foreach (Match match in matches)
                {
                    var referenceItem = TextTags.FirstOrDefault(textTag => textTag.Reference.Name.ToLower() == match.Value.ToLower() && textTag.ReferenceParentItem.ID_Other == ToClassItem.ID_Other)?.Reference;
                    TextTags.Add(new TextTag
                    {
                        Id = Guid.NewGuid().ToString(),
                        Reference = referenceItem ?? new clsOntologyItem
                        {
                            GUID = globals.NewGUID,
                            Name = match.Value,
                            GUID_Parent = ToClassItem.ID_Other,
                            New_Item = true
                        },
                        ReferenceParentItem = ToClassItem,
                        Start = match.Index,
                        End = match.Index + match.Length
                    });
                }
            }

            return TextTags.Any();
        }

        public TicketTemplateRegex(clsObjectRel ticketTemplate, clsObjectRel templateToRegex, clsObjectAtt regexObject, clsObjectRel toClassItem)
        {
            TicketTemplate = ticketTemplate;
            RegexObject = regexObject;
            ToClassItem = toClassItem;
            Regex = new Regex(regexObject.Val_String);
            ObjectItems = new List<clsOntologyItem>();
            TextTags = new List<TextTag>();
        }
    }
}
