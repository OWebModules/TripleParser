﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using TripleParserConnector.Models;
using TripleParserConnector.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TripleParserConnector
{
    public abstract class ParseBase
    {
        protected Globals globals;
        protected clsLogStates logStates = new clsLogStates();
        protected ServiceAgentElastic serviceAgent;
        protected clsRelationConfig relationConfig;

        protected void CheckNomen(FoundWord foundWord, string sentense)
        {
            var wordToTest = foundWord.Word.Trim();
            if (foundWord.StartIx == 0) return;

            if (Regex.IsMatch(wordToTest, ParseParts.UpperCaseStartPattern))
            {
                var character = "";
                var startIx = foundWord.StartIx - 1;
                for (int ix = startIx; ix >= 0; ix--)
                {
                    character = sentense.Substring(ix, 1);
                    if (Regex.IsMatch(character, ParseParts.NotString)) break;
                }

                if (Regex.IsMatch(character, ParseParts.PunctuationMarkPattern)) return;

                foundWord.Nomen = true;
            }

        }

        /// <summary>
        /// Find Words in quotation marks
        /// </summary>
        /// <param name="sentense">The sentense to be parsed</param>
        /// <param name="words">The list of found parts</param>
        protected void ParseQuotationWords(string sentense, List<FoundWord> words)
        {
            var regexQuotationMarks = new Regex("\"");
            var matchesQuotationMarks = regexQuotationMarks.Matches(sentense);
            for (int ix = 0; ix < matchesQuotationMarks.Count; ix += 2)
            {
                if (matchesQuotationMarks.Count > ix)
                {
                    var start = matchesQuotationMarks[ix].Index + 1;
                    var end = matchesQuotationMarks[ix + 1].Index;
                    if (end > start + 1)
                    {
                        var wordText = sentense.Substring(start, end - start);
                        var word = new FoundWord
                        {
                            Word = wordText,
                            StartIx = start - 1,
                            EndIx = end,
                            Quoted = true,
                            OItemWord = new clsOntologyItem
                            {
                                Name = wordText,
                                GUID_Parent = serviceAgent.ClassWord.GUID,
                                Type = globals.Type_Object,
                                Val_Long = start - 1
                            }
                        };
                        CheckNomen(word, sentense);
                        words.Add(word);
                    }
                }
            }

        }

        /// <summary>
        /// Split text by seperators like ",", "." or ";"... 
        /// </summary>
        /// <param name="sentense">The sentense to be parsed</param>
        /// <param name="words">The list of found parts</param>
        protected void ParseSeperators(string sentense, List<FoundWord> words)
        {
            var regexSeperators = new Regex(ParseParts.SeperatorsPattern);
            var matchesSeperators = regexSeperators.Matches(sentense);

            for (int ix = 0; ix < matchesSeperators.Count; ix++)
            {
                // Beginning of the sentense
                if (ix == 0)
                {
                    // The found part is not in a part inside of the list
                    if (!words.Any(word => matchesSeperators[ix].Index >= word.StartIx && matchesSeperators[ix].Index <= word.EndIx))
                    {
                        var word = sentense.Substring(0, matchesSeperators[ix].Index);

                        if (!string.IsNullOrEmpty(word))
                        {
                            var wordToAdd = new FoundWord
                            {
                                Word = word,
                                StartIx = 0,
                                EndIx = matchesSeperators[ix].Index,
                                OItemWord = new clsOntologyItem
                                {
                                    Name = word,
                                    GUID_Parent = serviceAgent.ClassWord.GUID,
                                    Type = globals.Type_Object,
                                    Val_Long = 0
                                }
                            };

                            // Check for Nomen (later processing)
                            CheckNomen(wordToAdd, sentense);
                            words.Add(wordToAdd);
                        }
                    }
                }
                // at the end of the sentense
                else if (ix == matchesSeperators.Count - 1)
                {

                    var matchBefore = matchesSeperators[ix - 1];
                    var match = matchesSeperators[ix];

                    // The found part is in a part inside of the list
                    if (!words.Any(word => matchBefore.Index + matchBefore.Length + 1 >= word.StartIx - 1
                         && matchBefore.Index + matchBefore.Length + 1 <= word.EndIx))
                    {

                        var word = sentense.Substring(matchBefore.Index + matchBefore.Length, match.Index - (matchBefore.Index + matchBefore.Length));

                        if (!string.IsNullOrEmpty(word))
                        {
                            var wordToAdd = new FoundWord
                            {
                                Word = word,
                                StartIx = matchBefore.Index + matchBefore.Length + 1,
                                EndIx = matchBefore.Index + matchBefore.Length + 1 + word.Length,
                                OItemWord = new clsOntologyItem
                                {
                                    Name = word,
                                    GUID_Parent = serviceAgent.ClassWord.GUID,
                                    Type = globals.Type_Object,
                                    Val_Long = matchBefore.Index + matchBefore.Length + 1
                                }
                            };
                            CheckNomen(wordToAdd, sentense);
                            words.Add(wordToAdd);
                        }

                    }
                    // The found part is in a part inside of the list
                    if (!words.Any(word => matchesSeperators[ix].Index + matchesSeperators[ix].Length + 1 >= word.StartIx - 1
                         && matchesSeperators[ix].Index + matchesSeperators[ix].Length + 1 <= word.EndIx))
                    {
                        var word = sentense.Substring(matchesSeperators[ix].Index + matchesSeperators[ix].Length);

                        if (!string.IsNullOrEmpty(word))
                        {
                            var wordToAdd = new FoundWord
                            {
                                Word = word,
                                StartIx = matchesSeperators[ix].Index + matchesSeperators[ix].Length,
                                EndIx = matchesSeperators[ix].Index + matchesSeperators[ix].Length + word.Length,
                                OItemWord = new clsOntologyItem
                                {
                                    Name = word,
                                    GUID_Parent = serviceAgent.ClassWord.GUID,
                                    Type = globals.Type_Object,
                                    Val_Long = matchesSeperators[ix].Index + matchesSeperators[ix].Length
                                }
                            };
                            CheckNomen(wordToAdd, sentense);
                            words.Add(wordToAdd);
                        }
                    }
                }
                // Not at the beginning and not at the end of the sentense
                else
                {
                    var matchBefore = matchesSeperators[ix - 1];
                    var match = matchesSeperators[ix];
                    // The found part is in a part inside of the list
                    if (!words.Any(word => matchBefore.Index + matchBefore.Length + 1 >= word.StartIx - 1
                         && matchBefore.Index + matchBefore.Length + 1 <= word.EndIx))
                    {
                        var word = sentense.Substring(matchBefore.Index + matchBefore.Length, match.Index - (matchBefore.Index + matchBefore.Length));

                        if (!string.IsNullOrEmpty(word))
                        {
                            var wordToAdd = new FoundWord
                            {
                                Word = word,
                                StartIx = matchBefore.Index + matchBefore.Length,
                                EndIx = matchBefore.Index + matchBefore.Length + word.Length - 1,
                                OItemWord = new clsOntologyItem
                                {
                                    Name = word,
                                    GUID_Parent = serviceAgent.ClassWord.GUID,
                                    Type = globals.Type_Object,
                                    Val_Long = matchBefore.Index + matchBefore.Length
                                }
                            };
                            CheckNomen(wordToAdd, sentense);
                            words.Add(wordToAdd);
                        }

                    }
                }
            }
        }

        public async Task<ResultFindWords> FindWords(string text)
        {
            var taskResult = await Task.Run<ResultFindWords>(() =>
            {
                var result = new ResultFindWords
                {
                    Result = globals.LState_Success.Clone()
                };

                var words = new List<FoundWord>();

                // Get words in quotation-marks
                ParseQuotationWords(text, words);

                // Get the other words in the text
                ParseSeperators(text, words);

                words = words.OrderBy(match => match.StartIx).ToList();

                // Mark the Terms
                words.Where(word => Regex.IsMatch(word.Word, ParseParts.UpperCase)).ToList().ForEach(word =>
                {
                    // Brakets contain Synonyms (Definition by Irene)
                    word.Brackets = Regex.IsMatch(word.Word, ParseParts.Brackets);
                    word.Word = Regex.Replace(word.Word, @"(\(|\))", "");
                    word.Word = word.Word.Substring(0, 1) + word.Word.Substring(1).ToLower();
                    word.UpperCase = true;
                    word.OItemWord = new clsOntologyItem
                    {
                        Name = word.Word,
                        GUID_Parent = serviceAgent.ClassWord.GUID,
                        Val_Long = word.StartIx,
                        Type = globals.Type_Object
                    };
                });



                result.FoundWords = words;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<clsOntologyItem>> CreateProject(string project)
        {
            serviceAgent = new ServiceAgentElastic(globals);
            var projectResult = await serviceAgent.CreateProject(project);

            return projectResult;
        }


        public ParseBase(Globals globals)
        {
            this.globals = globals;
            this.serviceAgent = new ServiceAgentElastic(globals);
            this.relationConfig = new clsRelationConfig(globals);
        }
    }

    public class ResultParse
    {
        public clsOntologyItem Result { get; set; }
        public clsObjectRel ObjectRel { get; set; }
        public clsClassAtt ClassAtt { get; set; }
        public clsClassRel ClassRel { get; set; }
        public clsObjectAtt ObjectAtt { get; set; }
    }

    public class FoundWord
    {
        public string Word { get; set; }
        public int StartIx { get; set; }
        public int EndIx { get; set; }
        public bool Brackets { get; set; }
        public bool Quoted { get; set; }
        public bool Nomen { get; set; }
        public bool UpperCase { get; set; }
        public clsOntologyItem OItemWord { get; set; }
    }

    public class ResultFindWords
    {
        public clsOntologyItem Result { get; set; }
        public List<FoundWord> FoundWords { get; set; }

    }



    public class FoundClass
    {
        public FoundWord FoundWord { get; set; }
        public clsOntologyItem ClassItem { get; set; }
        public Match GuidMatch { get; set; }
        public bool Pre { get; set; }
    }

    public class FoundClasses
    {
        public List<FoundClass> ClassesFound { get; set; }

    }

    public class ResultFoundClasses
    {
        public clsOntologyItem Result { get; set; }
        public FoundClasses FoundClasses { get; set; }
    }

    public class ParsePartItem
    {
        public ParsePartItem BasePart { get; set; }
        public clsOntologyItem PartOItem { get; set; }
        public string Part { get; set; }
        public int StartIx { get; set; }
        public int EndIx { get; set; }
        public int NextOrderId { get; set; }
        public List<FoundWord> Words { get; set; }

    }
}
