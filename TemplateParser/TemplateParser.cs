﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripleParserConnector.Models;
using TripleParserConnector.TripleParserWithTemplates;

namespace TripleParserConnector.TemplateParser
{
    public class TemplateParser : ParseBase
    {
        /// <summary>
        /// Get Terms, before- and after-words of template
        /// </summary>
        /// <param name="template">The text of the template</param>
        /// <returns></returns>
        public Task<ResultItem<List<TemplateTerm>>> ParseTemplate(string template)
        {
            var taskResult = Task.Run<ResultItem<List<TemplateTerm>>>(async () =>
            {
                var result = new ResultItem<List<TemplateTerm>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<TemplateTerm>()
                };

                var wordsResult = await FindWords(template);


                if (wordsResult.Result.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = wordsResult.Result;
                    return result;
                }

                // Terms
                var upperCases = wordsResult.FoundWords.Where(word => word.UpperCase).ToList();

                var triplePart = new clsOntologyItem
                {
                    GUID = globals.NewGUID,
                    Name = template.Length > 255 ? template.Substring(0, 254) : template,
                    GUID_Parent = serviceAgent.ClassTriplePart.GUID,
                    Type = globals.Type_Object,
                    Additional1 = template,
                    New_Item = true
                };

                foreach (var upperCase in upperCases)
                {
                    var templateWord = new TemplateTerm
                    {
                        TriplePart = triplePart,
                        WordsBefore = new List<FoundWord>(),
                        WordsAfter = new List<FoundWord>()
                    };

                    // Terms before this term
                    var upperCaseBefore = upperCases.Where(upperC => upperC.StartIx < upperCase.StartIx).OrderByDescending(upperC => upperC.StartIx).FirstOrDefault();

                    // Terms after this term
                    var upperCasesAfter = upperCases.Where(upperC => !upperC.Brackets && upperC.StartIx > upperCase.StartIx).OrderBy(upperC => upperC.StartIx).FirstOrDefault();

                    // First Term in text
                    if (upperCaseBefore == null)
                    {
                        // find the word before the template
                        var wordsBefore = wordsResult.FoundWords.Where(word => word.StartIx < upperCase.StartIx).OrderBy(word => word.StartIx);
                        templateWord.WordsBefore.AddRange(wordsBefore);
                    }
                    else
                    {
                        
                        if (upperCase.Brackets)
                        {
                            // Synonym for the Term before
                            var tempalteTermBefore = result.Result.LastOrDefault();
                            templateWord.Synonym = tempalteTermBefore;
                        }
                        else
                        {
                            // find the words before the Term but after the Term before
                            var wordsBefore = wordsResult.FoundWords.Where(word => word.StartIx < upperCase.StartIx && word.StartIx > upperCaseBefore.StartIx).OrderBy(word => word.StartIx).ThenBy(word => word.StartIx > upperCaseBefore.StartIx);
                            templateWord.WordsBefore.AddRange(wordsBefore);
                        }
                        
                    }

                    if (upperCasesAfter == null)
                    {
                        var wordsAfter = wordsResult.FoundWords.Where(word => !word.Brackets && word.StartIx > upperCase.StartIx).OrderBy(word => word.StartIx).FirstOrDefault();
                        if (wordsAfter != null)
                        {
                            templateWord.WordsAfter.Add(wordsAfter);
                        }
                        
                    }
                    else
                    {
                        // find the words after before the next Term
                        var wordsAfter = wordsResult.FoundWords.Where(word => !word.Brackets && word.StartIx > upperCase.StartIx && word.StartIx < upperCasesAfter.StartIx).OrderBy(word => word.StartIx).ThenBy(word => word.StartIx < upperCasesAfter.StartIx).FirstOrDefault();
                        if (wordsAfter != null)
                        {
                            templateWord.WordsAfter.Add(wordsAfter);
                        }
                    }


                    // Class for Term, subclass of Project-Class
                    var upperCaseClass = new clsOntologyItem
                    {
                        Name = upperCase.Word,
                        GUID_Parent = serviceAgent.ClassProject.GUID,
                        Type = globals.Type_Class
                    };

                    templateWord.Class = upperCaseClass;

                    result.Result.Add(templateWord);
                }


                return result;
            });

            return taskResult;
        }

        private async Task<clsOntologyItem> CreateTermsToClasses(List<TemplateTerm> templateTerms)
        {
            var result = globals.LState_Success.Clone();
            var serviceAgent = new Services.ServiceAgentElastic(globals);

            var taskResult = await Task.Run<List<clsObjectRel>>(() =>
            {
                var relationConfig = new clsRelationConfig(globals);
                var rel = templateTerms.Select(templateTerm =>
                {
                    var rel1 = relationConfig.Rel_ObjectRelation(templateTerm.OItemTerm, templateTerm.Class, globals.RelationType_belongingClass, orderId: 1);

                    
                    return rel1;
                }).ToList();


                return rel;
            });


            if (taskResult.Any())
            {
                var resultCreateTermsToClasses = await serviceAgent.CreateObjectRelations(taskResult);

                result = resultCreateTermsToClasses;

            }

            return result;
        }

        private async Task<clsOntologyItem> CreateTermsToWords(List<TemplateTerm> templateTerms)
        {
            var result = globals.LState_Success.Clone();
            var serviceAgent = new Services.ServiceAgentElastic(globals);

            var taskResult = await Task.Run<List<clsObjectRel>>(() =>
            {
                var relationConfig = new clsRelationConfig(globals);
                var ix1 = 1;
                var ix2 = 1;
                var rel = templateTerms.SelectMany(templateTerm =>
                {
                    
                    var rel1 = templateTerm.WordsBefore.Select(word => relationConfig.Rel_ObjectRelation(templateTerm.OItemTerm, word.OItemWord, serviceAgent.RelationTypeBefore, orderId: ix1++)).ToList();

                    rel1.AddRange(templateTerm.WordsAfter.Select(word => relationConfig.Rel_ObjectRelation(templateTerm.OItemTerm, word.OItemWord, serviceAgent.RelationTypeAfter, orderId: ix2++)));
                    return rel1;
                }).ToList();
                

                return rel;
            });


            if (taskResult.Any())
            {
                var resultCreateTerms = await serviceAgent.CreateObjectRelations(taskResult);

                result = resultCreateTerms;

            }

            return result;
        }

        /// <summary>
        /// Create Triple Store items with represent the Template, the Terms and the before- and after-words
        /// </summary>
        /// <param name="projectItem">The Triple Store Project-Item</param>
        /// <param name="templateTerms">The Template-Terms in the Template</param>
        /// <returns></returns>
        public async Task<ResultTripleParts> CreateTemplateTerms(clsOntologyItem projectItem, List<TemplateTerm> templateTerms)
        {

            var result = new ResultTripleParts
            {
                Result = globals.LState_Success.Clone()
            };

            // Create a Triplepart Object (Template) with related Project
            result.TripleParts = templateTerms.GroupBy(templateTerm => templateTerm.TriplePart.GUID).ToList().Select(triplePart =>
            {
                var triplePartItem = templateTerms.First(templateTerm => templateTerm.TriplePart.GUID == triplePart.Key);
                var tripleP = new TriplePart
                {
                    TriplePartOItem = triplePartItem.TriplePart,
                    // Text can have more then 255 characters
                    TriplePartText = triplePartItem.TriplePart.Additional1,
                    ProjectItem = projectItem,
                    TriplePartType = serviceAgent.ObjectTemplate,
                    OrderId = 1
                };

                return tripleP;
            }).ToList();

            // Create the Triple Store items of terms
            result.Result = await CreatePreTerms(templateTerms);
            if (result.Result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            //result.Result = await CreatePreWords(templateTerms);
            //if (result.Result.GUID == globals.LState_Error.GUID)
            //{
            //    return result;
            //}

            var orderId = 1;

            // Create the TripleTerm-List which contains an OrderId
            var templateTermList = templateTerms.Select(templateTerm => new TripleTerm
            {
                TriplePartOItem = templateTerm.TriplePart,
                TripleTermOItem = new clsOntologyItem
                {
                    GUID = templateTerm.OItemTerm.GUID,
                    Name = templateTerm.OItemTerm.Name,
                    GUID_Parent = templateTerm.OItemTerm.GUID_Parent,
                    Type = templateTerm.OItemTerm.Type,
                    New_Item = templateTerm.OItemTerm.New_Item
                },
                ClassItem = templateTerm.Class,
                BeforeWords = templateTerm.WordsBefore.Select(word => word.OItemWord).ToList(),
                AfterWords = templateTerm.WordsAfter.Select(word => word.OItemWord).ToList(),
                OrderId = orderId++,
                SynonymOItem = templateTerm.Synonym?.OItemTerm
            });

            // Map the TripleTerms to the TriplePart
            result.TripleParts.ForEach(triplePart =>
            {
                triplePart.TripleTerms = templateTermList.Where(term => term.TriplePartOItem == triplePart.TriplePartOItem).ToList();
            });

            // Relate the Synonyms (Terms can be synonyms for other Terms)
            (from templateTerm in result.TripleParts.SelectMany(triplePart => triplePart.TripleTerms).Where(term => term.SynonymOItem != null)
             join templateTermSynonym in result.TripleParts.SelectMany(triplePart => triplePart.TripleTerms) on templateTerm.SynonymOItem.GUID equals templateTermSynonym.TripleTermOItem.GUID
             select new { templateTerm, templateTermSynonym }).ToList().ForEach(templ =>
             {
                 templ.templateTerm.Synonym = templ.templateTermSynonym;
             });

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="project"></param>
        /// <param name="templateTerms"></param>
        /// <returns></returns>
        public async Task<ResultTripleParts> CreateTemplateTerms(string project, List<TemplateTerm> templateTerms)
        {
            
            
            var projectItem = new clsOntologyItem
            {
                Name = project,
                GUID_Parent = serviceAgent.ClassProject.GUID,
                Type = globals.Type_Object
            };

            var result = await CreateTemplateTerms(projectItem, templateTerms);

            
            return result;
        }

        private async Task<clsOntologyItem> CreateTermsToTerms(List<TemplateTerm> templateTerms)
        {
            var result = globals.LState_Success.Clone();
            var serviceAgent = new Services.ServiceAgentElastic(globals);

            var taskResult = await Task.Run<List<clsObjectRel>>(() =>
            {
                var relationConfig = new clsRelationConfig(globals);
                var rel = templateTerms.Where(templateTerm => templateTerm.Synonym != null).Select(templateTerm =>
                {
                    var rel1 = relationConfig.Rel_ObjectRelation(templateTerm.Synonym.OItemTerm, templateTerm.OItemTerm, serviceAgent.RelationTypeIsSynonymFor, orderId: 1);
                    return rel1;
                }).ToList();


                return rel;
            });


            if (taskResult.Any())
            {
                var resultCreateTermsToTerms = await serviceAgent.CreateObjectRelations(taskResult);

                result = resultCreateTermsToTerms;

            }

            return result;
        }

        
        /// <summary>
        /// Create the Triple Store items which represent the Terms
        /// </summary>
        /// <param name="templateTerms">The Terms of a Template</param>
        /// <returns>Successsstate of the creation</returns>
        private async Task<clsOntologyItem> CreatePreTerms(List<TemplateTerm> templateTerms)
        {
            var result = globals.LState_Success.Clone();
            var serviceAgent = new Services.ServiceAgentElastic(globals);

            var taskResult = await Task.Run<List<clsOntologyItem>>(() =>
            {

                templateTerms.ForEach(templTerm =>
                {
                    templTerm.OItemTerm = new clsOntologyItem
                    {
                        GUID = globals.NewGUID,
                        Name = templTerm.Class.Name,
                        GUID_Parent = serviceAgent.ClassTemplateTerm.GUID,
                        Type = globals.Type_Object,
                        New_Item = true
                    };
                });

                return templateTerms.Select(tmplTerm => tmplTerm.OItemTerm).ToList();
            });

            
            return result;
        }

        private async Task<clsOntologyItem> CreateClasses(List<TemplateTerm> templateTerms)
        {

            var result = globals.LState_Success.Clone();
            var taskResult = await Task.Run<List<clsOntologyItem>>(() =>
            {
                var classes = templateTerms.GroupBy(templateTerm => new { GUID = templateTerm.Class.GUID, Name = templateTerm.Class.Name, GUID_Parent = templateTerm.Class.GUID_Parent }).Select(classGroup => new clsOntologyItem
                {
                    GUID = classGroup.Key.GUID,
                    Name = classGroup.Key.Name,
                    GUID_Parent = classGroup.Key.GUID_Parent,
                    Type = globals.Type_Class
                }).ToList();

                return classes;
            });


            (from templateTerm in templateTerms
             join classItm in taskResult on templateTerm.Class.Name equals classItm.Name
             select new { templateTerm, classItm }).ToList().ForEach(templ =>
               {
                   templ.templateTerm.Class = templ.classItm;
               });            
            
            //var serviceAgent = new Services.ServiceAgentElastic(globals);

            //var classesToCreate = taskResult.Where(classItem => classItem.New_Item != null && classItem.New_Item.Value).ToList();

            //if (classesToCreate.Any())
            //{
            //    result = await serviceAgent.CreateClasses(classesToCreate, null);

                
                
            //}
            

            return result;
        }

        private async Task<clsOntologyItem> CreatePreWords(List<TemplateTerm> templateTerms)
        {
            var result = globals.LState_Success.Clone();
            var serviceAgent = new Services.ServiceAgentElastic(globals);

            var taskResult = await Task.Run<List<clsOntologyItem>>(async () =>
            {

                var wordListPre = templateTerms.SelectMany(templateTerm => templateTerm.WordsBefore).ToList ();
                wordListPre.AddRange(templateTerms.SelectMany(templateTerm => templateTerm.WordsAfter));
                var wordList = wordListPre.GroupBy(word => new { Name = word.Word }).Select(word => new clsOntologyItem
                {
                    Name = word.Key.Name,
                    GUID_Parent = serviceAgent.ClassWord.GUID,
                    Type = globals.Type_Object
                }).ToList();

                //var checkResult = await serviceAgent.CheckObjects(wordList);

                return wordList;
            });

            (from templateTerm in templateTerms.SelectMany(templTerm => templTerm.WordsBefore)
                join word in taskResult on templateTerm.Word equals word.Name into words
                from word in words.DefaultIfEmpty()
                select new { templateTerm, word }).ToList().ForEach(templateTermWord =>
                {
                    templateTermWord.templateTerm.OItemWord = templateTermWord.word;
                });

           

            return result;
        }

        //public async Task<bool> CreateTermsToWords(List<TemplateTerm> templateTerms)
        //{
        //    var result = globals.LState_Success.Clone();
        //    var serviceAgent = new Services.ServiceAgentElastic(globals);
        //    var relationConfig = new clsRelationConfig(globals);

        //    var taskResult = await Task.Run<List<clsObjectRel>>(() =>
        //    {
        //        templateTerms.Select(templateTerm =>
        //        {
        //            templateTerm.
        //        }).ToList();
        //        var wordsBeforeList = templateTerms.SelectMany(templateTerm => templateTerm.WordsBefore).Select(word => word.OItemWord)

        //        return wordList;
        //    });
        //}


        public TemplateParser(Globals globals) : base(globals)
        {
        }

    }

   
    
}
